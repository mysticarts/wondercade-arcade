﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class LoadingScene : MonoBehaviour
{
    [SerializeField]
    private Image _progressBar;
    SceneManage sceneManager;
    AsyncOperation gameLevel;
    public float delayTimer = 0f;
    private float currentDelayTimer = 0f;
    private bool sceneLoading = false;


    // Start is called before the first frame update
    void Start()
    {
        //Invoke("ToolTipDelay", 5f);
        //Start async operation 
        // StartCoroutine(LoadAsyncOperation());
        

    }

    public void LoadScene()
    {
        sceneManager = FindObjectOfType<SceneManage>();
        gameLevel = SceneManager.LoadSceneAsync(sceneManager.selectedlevel);
        Destroy(sceneManager);
        sceneLoading = true;


    }




    private void Update()
    {
        currentDelayTimer += Time.deltaTime * 2;
        _progressBar.fillAmount += 1 / ( delayTimer - currentDelayTimer);


        if (currentDelayTimer >= delayTimer)
        {
            if (!sceneLoading)
            {
                LoadScene();
            }
        }


    }


    //IEnumerator LoadAsyncOperation()
    //{
    //    //creat an async operation
        
    //    while (gameLevel.progress < 1)
    //    {
    //        //take the progress bar fill - async operation progress
    //        yield return new WaitForEndOfFrame();
    //    }
    //}
}

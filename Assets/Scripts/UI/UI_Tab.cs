﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public enum ButtonType {
    ALL,
    ARCADE,
    VENDING,
    DECORATION
}

public class UI_Tab : MonoBehaviour {

    public ButtonType type = ButtonType.ALL;

    public PopulateGrid content;
    public void PopUpTab() {
        content.Populate(type);
    }

}

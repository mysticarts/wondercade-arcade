﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIobject_3D : MonoBehaviour {

    public Vector3 poppedDownSize;
    public Vector3 poppedUpSize;
    public bool rotating = false;
    public float rotateSpeed = 1;

    void Update() {
        if(rotating) {
            this.gameObject.transform.Rotate(new Vector3(0, rotateSpeed, 0));
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Pixelplacement;

public class PopulateGrid : MonoBehaviour {

    public GameObject defaultItem;
    public List<Item> items = new List<Item>();

    public ButtonType currentDisplayed;

    public MachineManager machineManager;

    public void Populate(ButtonType button) {

        List<Item> itemsToShow = new List<Item>();

        currentDisplayed = button;

        if(button == ButtonType.ALL)
            itemsToShow = items; //show all items
            
        else {
            for(int count = 0; count < items.Count; count++) {
                if(button == ButtonType.ARCADE && items[count].itemType == ItemType.ARCADE)
                    itemsToShow.Add(items[count]); //show only arcades
                if(button == ButtonType.VENDING && items[count].itemType == ItemType.VENDOR)
                    itemsToShow.Add(items[count]); //show only vendors
                if(button == ButtonType.DECORATION && items[count].itemType == ItemType.DECORATION)
                    itemsToShow.Add(items[count]); //show only decorations
            }   
        }
        
        //remove old items
        foreach(Transform child in transform) {
            Tween.LocalScale(child, new Vector3(child.localScale.x, 0, child.localScale.y), .2f, 0, Tween.EaseInOutStrong, Tween.LoopType.None, null, null, false);
            Destroy(child.gameObject, 0.2f);
        }


        //display items
        for(int count = 0; count < itemsToShow.Count; count++) {
            GameObject placeholder = Instantiate(defaultItem, transform);
            ShopItem shopItem = placeholder.GetComponent<ShopItem>();

            //set data:
            shopItem.item = itemsToShow[count];
            shopItem.costPlaceholder.text = "" + shopItem.item.cost;
            shopItem.namePlaceholder.text = "" + shopItem.item.iname;
            shopItem.amountPlaceholder.text = MachineManager.GetNumberOfMachines(shopItem.item.iname) + "/3";
            shopItem.placeHolderImage.sprite = shopItem.item.shopImage;
            shopItem.button.onClick.AddListener(delegate { machineManager.EnterPurchaseMode(shopItem.gameObject); } );

            Tween.LocalScale(placeholder.transform, new Vector3(1, 1, 1), .25f, 0.2f, Tween.EaseInOutStrong, Tween.LoopType.None, null, null, false);
        }

    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CabinetRandomizer : MonoBehaviour
{
    public List<GameObject> screens = new List<GameObject>();



    // Start is called before the first frame update
    void Start()
    {
        int ran = Random.Range(0, screens.Count);

        screens[ran].SetActive(true);

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using UnityEngine.EventSystems;

public class MainMenuNav : MonoBehaviour
{
    public GameObject mainMenu;
    public List<GameObject> menus = new List<GameObject>();
    private GameObject currentSelectedButton;
    private GameObject currentMenu;
    private Player input;
    public EventSystem eventSystem;


    // Start is called before the first frame update
    void Start()
    {
        currentMenu = mainMenu;
        input = ReInput.players.GetPlayer(0);
    }

    // Update is called once per frame
    void Update()
    {

        if (mainMenu.activeInHierarchy)
        {
            currentSelectedButton = eventSystem.currentSelectedGameObject;
        }

        foreach (var menu in menus)
        {
            if (menu.activeInHierarchy)
            {
                currentMenu = menu;
            }
        }

        if (input.GetButtonDown("UICancel"))
        {
            currentMenu.SetActive(false);
            mainMenu.SetActive(true);
            eventSystem.SetSelectedGameObject(currentSelectedButton);
        }
    }

    public void SetCurrentSelectedButton(GameObject button)
    {
        eventSystem.SetSelectedGameObject(button);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Pixelplacement;
using UnityEngine.EventSystems;
using TMPro;

public class MenuUI : MonoBehaviour {

    public List<UIobject> ui_bars;
    public List<UIobject_3D> ui_3D;
    public List<UIobject_2D> ui_2D;
    public List<UIobject_Text> ui_text;

    //panel sizes:
    public Vector3 poppedDownSize = new Vector3(0, 0.65f, 1);
    public Vector3 poppedUpSize = new Vector3(0.65f, 0.65f, 1);

    //for machines only:
    public TextMeshProUGUI level;
    public UIobject levelBar;

    public Selectable initialSelected;

    public bool analyticsOn = false;

    public void Start() {
        Tween.LocalScale(this.GetComponent<RectTransform>(), Vector3.zero, 0.25f, 0.5f, Tween.EaseOutStrong);
    }

    public bool isOpening = false;
    public virtual void PopUp(bool panel) {
        if(panel) {
            if(PlayerManager.menuUsing != null) 
                PlayerManager.menuUsing.PopDown(true);
            Tween.LocalScale(this.GetComponent<RectTransform>(), Vector3.zero, new Vector3(poppedUpSize.x, 0.005f, poppedUpSize.z), .5f, 0, Tween.EaseInOutStrong, Tween.LoopType.None, null, null, false);
            Tween.LocalScale(this.GetComponent<RectTransform>(), poppedUpSize, .75f, 0.4f, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
            if(initialSelected != null) EventSystem.current.SetSelectedGameObject(initialSelected.gameObject);
        }
        
        PlayerManager.menuUsing = this;

        //UI Bars
        foreach(UIobject gutter in ui_bars) {

            //gutter:
            Tween.Size(gutter.gameObject.GetComponent<RectTransform>(), gutter.poppedDownSize, gutter.poppedUpSize, .5f, .8f, Tween.EaseInOutStrong, Tween.LoopType.None, null, null, false);
        
            //bar:
            UIobject bar = gutter.gameObject.transform.Find("Bar").GetComponent<UIobject>();
            bar.gameObject.GetComponent<RawImage>().color = Color.white;
            Tween.Size(bar.gameObject.GetComponent<RectTransform>(), bar.poppedDownSize, bar.currentSize, 1, 1.3f, Tween.EaseInOutStrong);
            
            isOpening = true;
            StartCoroutine(isNoLongerOpen()); //2.3 seconds, set isOpening false
        }

        //UI 3D objects
        foreach(UIobject_3D object3D in ui_3D) {
            //arcade prop:
            Tween.LocalScale(object3D.gameObject.transform, object3D.poppedDownSize, object3D.poppedUpSize, .7f, .7f, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
        }
        
        //UI 2d
        foreach(UIobject_2D object2D in ui_2D) {
            Tween.LocalScale(object2D.gameObject.transform, object2D.poppedDownSize, object2D.poppedUpSize, .5f, 1f, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
            if(object2D.GetComponent<Selectable>() != null) (object2D.GetComponent<Selectable>()).enabled = true;
        }

        //UI Text
        foreach(UIobject_Text text in ui_text) {
            Tween.LocalScale(text.gameObject.transform, text.poppedDownSize, text.poppedUpSize, .2f, .8f, Tween.EaseInStrong, Tween.LoopType.None, null, null, false);
        }
    }

    public virtual void PopDown(bool panel) {
        if(panel) {
            PlayerManager.menuUsing = null;
            Tween.LocalScale(this.GetComponent<RectTransform>(), poppedUpSize, new Vector3(poppedDownSize.x, 0.005f, poppedDownSize.z), .25f, 0.25f, Tween.EaseInOutStrong, Tween.LoopType.None, null, null, false);
            Tween.LocalScale(this.GetComponent<RectTransform>(), Vector3.zero, 0.25f, 0.5f, Tween.EaseOutStrong, Tween.LoopType.None, null, null, false);
        }
        
        if(!analyticsOn) {
            //UI Bars
            foreach(UIobject gutter in ui_bars) {

                //gutter:
                Tween.Size(gutter.gameObject.GetComponent<RectTransform>(), gutter.poppedUpSize, gutter.poppedDownSize, .15f, 0, Tween.EaseInOutStrong, Tween.LoopType.None, null, null, false);
        
                //bar:
                UIobject bar = gutter.gameObject.transform.Find("Bar").GetComponent<UIobject>();
                Tween.Color(bar.gameObject.GetComponent<RawImage>(), Color.clear, .1f, 0);

            }

            //UI 3D objects
            foreach(UIobject_3D object3D in ui_3D) {
                //arcade prop:
              
                Tween.LocalScale(object3D.gameObject.transform, object3D.poppedUpSize, object3D.poppedDownSize, .15f, 0, Tween.EaseOutStrong, Tween.LoopType.None, null, null, false);
            }

            //UI 2d
            foreach(UIobject_2D object2D in ui_2D) {
                Tween.LocalScale(object2D.gameObject.transform, object2D.poppedUpSize, object2D.poppedDownSize, .1f, 0, Tween.EaseOutStrong, Tween.LoopType.None, null, null, false);
                if(object2D.GetComponent<Selectable>() != null) (object2D.GetComponent<Selectable>()).enabled = false;
            }

            //UI Text
            foreach(UIobject_Text text in ui_text) {
                Tween.LocalScale(text.gameObject.transform, text.poppedUpSize, text.poppedDownSize, .1f, 0, Tween.EaseOutStrong, Tween.LoopType.None, null, null, false);
            }
        } else {
            PopDownAnalytics(false);
        }
    }

    public Button backButtonAnalytics;
    public virtual void PopUpAnalytics() {
        PopDown(false);
        analyticsOn = true;

        if(backButtonAnalytics != null) EventSystem.current.SetSelectedGameObject(backButtonAnalytics.gameObject);
    }

    public virtual void PopDownAnalytics(bool backToMain) {
        if(backToMain) PopUp(false);
        analyticsOn = false;

        if(initialSelected != null) EventSystem.current.SetSelectedGameObject(initialSelected.gameObject);
    }

    private IEnumerator isNoLongerOpen() {
        yield return new WaitForSecondsRealtime(2f);
        isOpening = false;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Rewired;

public class ControlsDisplay : MonoBehaviour
{
    Player input;
    private SpriteRenderer controlDisplay;
    private Image icontrolDisplay;
    private RawImage rcontrolDisplay;
    public Sprite pcControl;
    public Sprite ps4Control;
    public Sprite XboxControl;

    private string ps4ID = "Wireless Controller";
    private string xboxID = "XInput Gamepad";

    // Start is called before the first frame update
    void Start()
    {
        input = ReInput.players.GetPlayer(0);
        controlDisplay = GetComponent<SpriteRenderer>();
        icontrolDisplay = GetComponent<Image>();
        rcontrolDisplay = GetComponent<RawImage>();
    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_STANDALONE_WIN
        if (input.controllers.joystickCount == 0)
        {
            if (controlDisplay != null)
            {
                controlDisplay.sprite = pcControl;
            }
            if (icontrolDisplay != null)
            {
                icontrolDisplay.sprite = pcControl;
            }
            if (rcontrolDisplay != null)
            {
                rcontrolDisplay.texture = pcControl.texture;
            }
        }
        else
        {
            if (input.controllers.Joysticks[0].hardwareName.Contains(ps4ID))
            {
                if (controlDisplay != null)
                {
                    controlDisplay.sprite = ps4Control;
                }
                if (icontrolDisplay != null)
                {
                    icontrolDisplay.sprite = ps4Control;
                }
                if (rcontrolDisplay != null)
                {
                    rcontrolDisplay.texture = ps4Control.texture;
                }
            }
            if (input.controllers.Joysticks[0].hardwareName.Contains(xboxID))
            {
                if (controlDisplay != null)
                {
                    controlDisplay.sprite = XboxControl;
                }
                if (icontrolDisplay != null)
                {
                    icontrolDisplay.sprite = XboxControl;
                }
                if (rcontrolDisplay != null)
                {
                    rcontrolDisplay.texture = XboxControl.texture;
                }
            }


        }
#endif
#if UNITY_PS4
         if (controlDisplay != null)
                {
                    controlDisplay.sprite = ps4Control;
                }
                if (icontrolDisplay != null)
                {
                    icontrolDisplay.sprite = ps4Control;
                }
#endif

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using Pixelplacement;

public class CollideWithCustomer : MonoBehaviour {

    public MenuUI_Customer menu;
    public GameObject manageButton;
    public Vector3 poppedUpSize = new Vector3(1, 1, 0.5f);

    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player") && this.CompareTag("AI")) {
            ShowDiegeticUI(true);
            playerIsInRadius = true;
        }
    }

    private void OnTriggerExit(Collider other) {
        if(other.CompareTag("Player") && this.CompareTag("AI")) {
            ShowDiegeticUI(false);
            playerIsInRadius = false;
        }
    }

    public void ShowDiegeticUI(bool show) {
        if(show) {
            PlayerManager.customerInteractingWith = this.GetComponent<AIStats>();

            //pop up manage button:
            Tween.LocalScale(manageButton.transform, poppedUpSize, 0.75f, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);

        } else {
            PlayerManager.customerInteractingWith = null;

            //pop down all buttons:
            Tween.LocalScale(manageButton.transform, Vector3.zero, 0.2f, 0, Tween.EaseOutStrong, Tween.LoopType.None, null, null, false);
            
        }
    }

    protected Player input;
    protected bool playerIsInRadius = false;

    public void Start() {
        input = ReInput.players.GetPlayer(0);
    }

    public void FixedUpdate() {
        if(input.GetButtonDown("Interact")) {
            if(playerIsInRadius && menu != null && PlayerManager.menuUsing == null) {
                menu.PopUp(true);
            } else
            if(menu != null && PlayerManager.menuUsing != null && GameObject.ReferenceEquals(PlayerManager.menuUsing, menu)
                && menu.transform.localScale != Vector3.zero) {
                menu.PopDown(true);
            }
        }
    }

}

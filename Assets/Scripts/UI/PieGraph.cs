﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PieGraph : MonoBehaviour {

    public float[] values;
    public Color[] wedgeColors;
    public Image wedgePrefab;

    public void Update() {
        //Display();
    }

    virtual public void Display() {
        float total = 0;
        float zRotation = 0;
        for (int i = 0; i < values.Length; i++)
            total += values[i];

        //remove old items
        foreach(Transform child in transform) {
            Destroy(child.gameObject);
        }

        if(ValuesEqualZero()) {
            for(int count = 0; count < values.Length; count++) {
                values[count] = (1.0f/values.Length);
            }
        }

        for (int i = 0; i < values.Length; i++) {
            Image newWedge = Instantiate(wedgePrefab) as Image;
            newWedge.transform.SetParent(transform, false);
            newWedge.color = wedgeColors[i];
            newWedge.fillAmount = values[i] / total;
            newWedge.transform.Rotate(new Vector3(0, 0, zRotation));
            zRotation -= newWedge.fillAmount * 360f;
        }


    }

    public bool ValuesEqualZero() {
        for(int count = 0; count < values.Length; count++) {
            if(values[count] != 0) return false;
        }
        return true;
    }

}

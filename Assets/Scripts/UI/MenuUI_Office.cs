﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;
using UnityEngine.UI;
using TMPro;

public class MenuUI_Office : MenuUI {

    public CRTUI crtUI;

    public ScreenUI screenUI;

    public GameObject activeTab;

    public void PopUpTab(GameObject tab) {
        activeTab.SetActive(false);
        activeTab = tab;
        activeTab.SetActive(true);
    }

    public GameObject playerStandingPos;
    public GameObject viewingPos;
    override public void PopUp(bool panel) {
        base.PopUp(true);
        PlayerManager.menuUsing = this;
        crtUI.PopUp();
        screenUI.PopDown();

        // zoom into screen:
        ViewManager viewManager = GameObject.Find("Managers").GetComponent<MachineManager>().viewManager;
        viewManager.gameViewPos.transform.SetPositionAndRotation(viewingPos.transform.position, viewingPos.transform.rotation);
        viewManager.Switch(View.GAME);
    }

    override public void PopDown(bool panel) {
        base.PopDown(true);
        crtUI.PopDown();
        screenUI.PopUp();

        ViewManager viewManager = GameObject.Find("Managers").GetComponent<MachineManager>().viewManager;
        viewManager.Switch(viewManager.previousView);
    }

}
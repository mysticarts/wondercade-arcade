﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Linq;

public class BarChart : MonoBehaviour {

    public Bar barPrefab;
    public int[] inputValue;
    public string[] labels;
    public Color[] colors;


    List<Bar> bars = new List<Bar>();

    float chartHeight;

    void Start()
    {
        chartHeight = Screen.height + GetComponent<RectTransform>().sizeDelta.y;
        //display the graph
        
        DisplayGraph(inputValue);
    }

    void DisplayGraph(int[] vals)
    {
        int maxValue = vals.Max();
        for (int i = 0; i <vals.Length; i++)
        {
            Bar newBar = Instantiate(barPrefab) as Bar;
            newBar.transform.SetParent(transform);
            //size bar
            RectTransform rt = newBar.bar.GetComponent<RectTransform>();
            float normalizedValue = ((float)vals[i]/(float)maxValue) * 0.55f;
            rt.sizeDelta = new Vector2(rt.sizeDelta.x, chartHeight * normalizedValue);
            newBar.bar.color = colors[i % colors.Length];

            //set label
            if (labels.Length <= i)
            {
                newBar.label.text = "UNDEFINED";
            }
            else
            {
                newBar.label.text = labels[i];
            }

            //set value label
            newBar.barValue.text = vals[i].ToString();
            // if height too small, move label to top of bar
            if (rt.sizeDelta.y < 30f)
            {
                newBar.barValue.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0f);
                newBar.barValue.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
            }
           
        }
    }

}

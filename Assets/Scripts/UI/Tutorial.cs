﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
    int currentTip;
    public string[] tips;
   // public GameObject[] TutorialText;
    public Text tiptext;
    public Text FinishTextButton;
    public string[] ButtonText;
    int CurrentTextButton;

    public GameObject firstSelected;
    public void Start()
    {
        GameObject.Find("EventSystem").GetComponent<EventSystem>().SetSelectedGameObject(firstSelected);
        CurrentTextButton = 0;
        currentTip = 0;
        //on commension of game set the first element of the string active
        UpdateTipText();
        ReverseTipText();
        ButtonTipText();
        
    }

    public void NextTip()
    {
        currentTip++;
        if (currentTip >= tips.Length)
            gameObject.SetActive(false);
        else
            UpdateTipText();
    }

    public void PreviousTip()
    {
        currentTip--;
        if (currentTip >= tips.Length)
            gameObject.SetActive(false);
        else
            ReverseTipText();
    }

    public void ChangeButtonText()
    {
        if (currentTip == 4)
            CurrentTextButton++;
        else ButtonTipText();

    }

    void UpdateTipText()
    {
        tiptext.text = tips[currentTip];
    }

    void ReverseTipText()
    {
       tiptext.text = tips[currentTip];
    }

    void ButtonTipText()
    {
        FinishTextButton.text = ButtonText[CurrentTextButton];

    }
}


    // Start is called before the first frame update
    /*  public IEnumerator ShowText()
      {
          TutorialText[0].SetActive(true);

          yield return new WaitForSeconds(5);
          {

              TutorialText[0].SetActive(false);
              TutorialText[1].SetActive(true);
              yield return new WaitForSeconds(5);
          }
          {
              TutorialText[1].SetActive(false);
              TutorialText[2].SetActive(true);
              yield return new WaitForSeconds(5);
          }
          {
              TutorialText[1].SetActive(false);
              TutorialText[2].SetActive(true);
              yield return new WaitForSeconds(5);

          }
          {
              TutorialText[2].SetActive(false);
              TutorialText[3].SetActive(true);
              yield return new WaitForSeconds(5);

          }
          {
              TutorialText[4].SetActive(false);
              TutorialText[5].SetActive(true);
              yield return new WaitForSeconds(5);

          }
          {
              TutorialText[5].SetActive(false);
              TutorialText[6].SetActive(true);
              yield return new WaitForSeconds(5);

          }

      }

  }
  */
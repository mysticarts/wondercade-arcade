﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;
using UnityEngine.UI;

public class CRTUI : MonoBehaviour {

    public Vector3 poppedDownSize = new Vector3(0.65f, 0, 1);
    public Vector3 poppedUpSize = new Vector3(0.65f, 0.65f, 1);

    public void Start() {
        Tween.LocalScale(this.GetComponent<RectTransform>(), Vector3.zero, 0.25f, 0.5f, Tween.EaseOutStrong);    
    }

    public virtual void PopUp() {
        Tween.LocalScale(this.GetComponent<RectTransform>(), Vector3.zero, new Vector3(poppedUpSize.x, 0.005f, poppedUpSize.z), .5f, 0, Tween.EaseInOutStrong, Tween.LoopType.None, null, null, false);
        Tween.LocalScale(this.GetComponent<RectTransform>(), poppedUpSize, .75f, 0.4f, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
    }

    public virtual void PopDown() {
        Tween.LocalScale(this.GetComponent<RectTransform>(), poppedUpSize, new Vector3(poppedDownSize.x, 0.005f, poppedDownSize.z), .25f, 0.25f, Tween.EaseInOutStrong, Tween.LoopType.None, null, null, false);
        Tween.LocalScale(this.GetComponent<RectTransform>(), Vector3.zero, 0.25f, 0.5f, Tween.EaseOutStrong, Tween.LoopType.None, null, null, false);
    }

    public ScreenUI screenUI;
    public Text coinsText;
    public Economy economy;
    public DayNightCycle cycle;
    public Text timeText;
    public Text meridiesText;
    public void Update() {
        int hour = cycle.time / 60;
        int remainder = cycle.time - (hour * 60);

        if(meridiesText != null) {
            if (hour >= 12) meridiesText.text = "PM";
            else meridiesText.text = "AM";
        }

        if(hour > 12) hour -= 12;

        string hourString = "" + hour;
        if(hour == 0) hourString = "12";
        string remainderString = "" + remainder;
        if(remainder < 10) remainderString = "0" + remainder;
        if(timeText != null) {
            timeText.text = hourString + ":" + remainderString;
        }

        coinsText.text = screenUI.totalMoney.text;
    }

}

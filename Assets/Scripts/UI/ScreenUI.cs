﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;
using UnityEngine.UI;

public class ScreenUI : MonoBehaviour {

    public RectTransform saving;
    public Vector2 saving_poppedUpPos;
    public Vector2 saving_poppedDownPos;

    public Text totalMoney;
    public RectTransform coins;
    public Vector2 coins_poppedUpPos;
    public Vector2 coins_poppedDownPos;

    public RectTransform clock;
    public Vector2 clock_poppedUpPos;
    public Vector2 clock_poppedDownPos;

    public void PopUp() {
        Tween.AnchoredPosition(saving, saving_poppedUpPos, 1, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
        Tween.AnchoredPosition(coins, coins_poppedUpPos, 1, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
        Tween.AnchoredPosition(clock, clock_poppedUpPos, 1, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
    }

    public void PopDown() {
        Tween.AnchoredPosition(saving, saving_poppedDownPos, 1, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
        Tween.AnchoredPosition(coins, coins_poppedDownPos, 1, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
        Tween.AnchoredPosition(clock, clock_poppedDownPos, 1, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
    }

}

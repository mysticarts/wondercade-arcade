﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause_Resume : MonoBehaviour
{
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            //Registers when the escape key is pressed 
        {
            Time.timeScale = 0;
            //restricts the time scale to 0, meaning anything dynamic is idle
        }
    }
    

    public void ResumeGame()
    {
        Time.timeScale = 1;
        //Nomralises the time scale
      
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
public enum View {
    FIRST,
    THIRD,
    EDIT,
    GAME
};

public class ViewManager : MonoBehaviour {

    public GameObject player;
    public GameObject cameraFollow;

    //start in third
    public View previousView = View.THIRD;
    public View currentView = View.THIRD;

    public Camera UICamera;

    //view positions
    public GameObject firstViewPos;
    public GameObject thirdViewPos;
    public GameObject editViewPos;
    public GameObject gameViewPos;

    public Dictionary<View, GameObject> viewPositions = new Dictionary<View, GameObject>();

    public bool viewChangeComplete = false;

    public PlayableDirector playableDirector = null;

    public void Start() {
        viewPositions[View.FIRST] = firstViewPos;
        viewPositions[View.THIRD] = thirdViewPos;
        viewPositions[View.EDIT] = editViewPos;
        viewPositions[View.GAME] = gameViewPos;

        Switch(View.THIRD);

        if(playableDirector != null)
            playableDirector.Play();
    }

    //lerp values
    public float time = 1; //time taken to lerp between points in seconds
    public bool isUpdating = false;
    public float t = 0;

    public EditModeUI editModeUI;
    public void Switch(View view) {
        if(currentView != view) {
            cameraFollow.transform.rotation = player.transform.rotation; //update the CameraFollow object on view change

            isUpdating = true;
            if(currentView != View.EDIT) previousView = currentView;
            currentView = view;

            if(view == View.EDIT) {
                if (editModeUI != null)
                {
                    editModeUI.PopUp();
                }
            }
            if(currentView == View.EDIT) {
                if (editModeUI != null)
                {
                    editModeUI.PopDown();
                }
            }

            startPos = Camera.main.transform.position;
            startRot = Camera.main.transform.rotation;

            viewChangeComplete = false; //reset
        }
    }

    private Vector3 startPos;
    private Quaternion startRot;
    public void Update() {

        UICamera.transform.position = transform.position;
        UICamera.transform.rotation = transform.rotation;

        if(isUpdating) {

            GameObject destination = viewPositions[currentView];

            float distance = Vector3.Distance(startPos, destination.transform.position);
            t += Time.unscaledDeltaTime / time;

            transform.rotation = Quaternion.Slerp(startRot, destination.transform.rotation, t);
            transform.position = Vector3.Lerp(startPos, destination.transform.position, t);

            if (t >= 1) {
                isUpdating = false; t = 0; //reset
                viewChangeComplete = true;
            }

        }

    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using UnityEngine.UI;

public enum ExpenseType {
    Tax,
    Rent,
    Electricity,
    Marketing
}

public class ExpensesManager : MonoBehaviour {

    public Economy economy;
    public BuildingManager buildingManager;
    public AdvertisingManager advertisingManager;

    public List<Text> amounts;
    public List<Button> buttons;
    public List<Text> dueText;

    private Dictionary<ExpenseType, int> expenses = new Dictionary<ExpenseType, int>(); //type, value
    private Dictionary<ExpenseType, int> daysSincePaying = new Dictionary<ExpenseType, int>(); //type, value

    //if insufficient funds, take everything to pay for it

    public void Start() {
        //for each expense:
        for(int count = 0; count <= (int)Enum.GetValues(typeof(ExpenseType)).Cast<ExpenseType>().Last(); count++) {
            expenses.Add((ExpenseType)count, 0);
            daysSincePaying.Add((ExpenseType)count, 0);
            if(buttons[count] != null) {
                buttons[count].transform.Find("Text").GetComponent<Text>().text = "Paid!";
                amounts[count].text = "$0";
            }
           
        }
    }

    //per day values
    [Range(0,100)] public float taxPercent = 0.2f;
    public int rentCostPerRoom = 50;
    public int electricityCostBaseRate = 20;
    public int electricityCostPerMachine = 5;

    public int daysUntilOverdue = 7;
    [Range(0, 100)] public int overdueInterestPercent = 5; //charge this percent per overdue day. Eg. 7 overdue days = 35 percent

    public void AddExpensesForDay(DayStats day) {

        //check if overdue, add 1 day if so:
        for(int count = 0; count < daysSincePaying.Count; count++) {
            ExpenseType expense = (ExpenseType)count;

            if(expenses[expense] > 0) {
                daysSincePaying[expense] += 1;
            } else daysSincePaying[expense] = 0;

            if(daysSincePaying[expense] > daysUntilOverdue) {
                //overdue
                int overdueDays = daysSincePaying[expense] - daysUntilOverdue;
                dueText[(int)expense].GetComponent<Text>().text = "OVERDUE! By " + overdueDays + " days";
            } else {
                int daysUntilDue = daysUntilOverdue - daysSincePaying[expense];
                dueText[(int)expense].GetComponent<Text>().text = "Due in " + daysUntilDue + " days";
            }
        }

        //tax:
        int tax = Mathf.RoundToInt(day.GetTotalSales() * (taxPercent/100));
        AddExpense(ExpenseType.Tax, Mathf.RoundToInt(tax * GetOverdueMultiplier(ExpenseType.Tax)));

        int unlockedRooms = 0;
        if(buildingManager != null) unlockedRooms = buildingManager.unlockedGrids.Count;
        int rent = rentCostPerRoom * unlockedRooms;
        AddExpense(ExpenseType.Rent, Mathf.RoundToInt(rent * GetOverdueMultiplier(ExpenseType.Rent)));
        day.billsExpenses += rent; //update analytics

        int numberOfMachines = MachineManager.arcades.Count + MachineManager.vendors.Count;
        int electricity = electricityCostBaseRate + (electricityCostPerMachine * numberOfMachines);
        AddExpense(ExpenseType.Electricity, Mathf.RoundToInt(electricity * GetOverdueMultiplier(ExpenseType.Electricity)));
        day.billsExpenses += electricity; //update analytics

        int marketing = 0;
        for(int count = 0; count < advertisingManager.moneySpent.Count; count++) {
            marketing += Mathf.RoundToInt(advertisingManager.moneySpent[count].value);
        }
        AddExpense(ExpenseType.Marketing, Mathf.RoundToInt(marketing * GetOverdueMultiplier(ExpenseType.Marketing)));
        day.marketingExpenses += marketing; //update analytics

    }

    public float GetOverdueMultiplier(ExpenseType expense) {
        if(daysSincePaying[expense] > daysUntilOverdue) {
            int overdueDays = daysSincePaying[expense] - daysUntilOverdue;
            return 1 + ((overdueDays * overdueInterestPercent) / 100); //1-2
        }
        return 1;
    }

    //modify with +/- amount
    public void AddExpense(ExpenseType expense, int amount) {
        expenses[expense] += amount;
        if(expenses[expense] > 0) {
            buttons[(int)expense].transform.Find("Text").GetComponent<Text>().text = "Pay Now";
            amounts[(int)expense].text = "$" + expenses[expense];
        }
    }

    public void Pay(int expenseID) {
        ExpenseType expense = (ExpenseType) expenseID;

        int cost = expenses[expense];
        if(cost > 0) {
            if(economy.money >= cost) {
                economy.Deduct(cost);
                expenses[expense] = 0;
                buttons[expenseID].transform.Find("Text").GetComponent<Text>().text = "Paid!";
            } else {
                //can still pay for bills if not enough funds to clear
                expenses[expense] -= economy.money;
                economy.Deduct(economy.money);
                buttons[expenseID].transform.Find("Text").GetComponent<Text>().text = "Not enough funds!";
            }
        }

        amounts[expenseID].text = "$" + expenses[expense];
    }

}

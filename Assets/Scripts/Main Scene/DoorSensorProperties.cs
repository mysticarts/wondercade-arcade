﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SensorType
{
    INTERIOR = 0,
    EXTERIOR = 1
}

public class DoorSensorProperties : MonoBehaviour
{
    public SensorType sensorType;
    public bool AIDetected = false;
}

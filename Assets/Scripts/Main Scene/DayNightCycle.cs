﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct CycleData
{
    public string weekText;
    public string timeText;
    public float timeOfDay;
    public Vector3 sunPos;
    public Vector3 sunRotation;
    public int dayCount;
    public bool isDay;
}

public class DayNightCycle : MonoBehaviour {

    [HideInInspector]
    public CycleData cData = new CycleData();

    public List<Light> streetLights = new List<Light>();
    //reference to AIspawning manager
    public AISpawning AIspawning;

    [HideInInspector]
    public bool storeClosed = false;
    [HideInInspector]
    public bool storeOpen = false;

    public List<AIStats> customers = new List<AIStats>();
    //reference to advertising manager
    public AdvertisingManager advertisingManager;
    //reference to expenses manager
    public ExpensesManager expensesManager;
    //reference to analytics ui
    public AnalyticsUI analytics;
    //reference to the current day stats
    public DayStats currentDayStats = null;
    //default day stats prefab that gets instantiated on new day
    public GameObject dayStatsPrefab;

    public Text computerTimeText;
    public Text timeText;
    public Text meridiesText;
    public RawImage cycleImage;
    public List<Texture2D> textures;

    public GameObject pivotPoint;
    public float timePerDay = 10; //in seconds

    public bool isDay = false;

    private float speed = 0;
    [HideInInspector]
    public float t = 0; //lerp value

    public int time; //in minutes //is non-editable

    private NotificationSystem notifications;
    public bool debug = false;
    void Start() {
        speed = 180 / (timePerDay/2); //180 degrees revolution / time
        if(!gameSaveLoaded) SetMidnight();

        notifications = GameObject.Find("Managers").GetComponent<NotificationSystem>();
    }

    public float minPoint = 0;
    public float maxPoint = 0;
    public Vector3 midnightPosition;
    public Quaternion midnightRotation;
    public void SetMidnight() {
        this.gameObject.transform.position = midnightPosition;
        this.gameObject.transform.rotation = midnightRotation;
    }

    [Tooltip("The number of customers to come in before sending morale notification")]
    public int howOftenToSendMoraleNotification = 8;

    public void StartDay() {
        isDay = true;
        addedDayForToday = false;
        storeClosed = false;
        storeOpen = true;
        advertisingManager.EnableSliders(false);

        if(dayStatsPrefab != null) {
            GameObject newDayStats = Instantiate(dayStatsPrefab, GameObject.Find("WeekStats").gameObject.transform);
            currentDayStats = newDayStats.GetComponent<DayStats>();
        }

        AIspawning.DetermineMaxAIforDay();

        SetStreetLights(false);

        notifications.Add(new Notification("Start of Day", "The store is open for business!"));
    }

    public void EndDay() {

        expensesManager.AddExpensesForDay(currentDayStats);

        advertisingManager.EnableSliders(true);
        storeClosed = true;
        storeOpen = false;
        isDay = false;
        if (!WeekStats.dayStats.Contains(currentDayStats))
        {
            WeekStats.AddDay(currentDayStats);
        }
        GatherCustomerData();
        currentDayStats.menu.PopUp(true);
        analytics.UpdateData();
        customers.Clear();

        SetStreetLights(true);

        notifications.Add(new Notification("End of Day", "The store is closed!"));

        //chance of changing advertising effectiveness
        if(UnityEngine.Random.Range(0, 100) <= advertisingManager.chanceOfChangingEffectivenessEachDay) {
            advertisingManager.CalculateNewEffectiveness();
        }

    }

    public Text dayText;
    public Text weekText;
    public Text daysPlayedText;

    public enum WeekDay {
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday,
        Sunday
    }
    public int daysPlayed = 1;
    public int GetWeekNumber() {
        return (Mathf.CeilToInt(daysPlayed / 7))+1;
    }
    public WeekDay GetWeekDay() {
        return (WeekDay)(daysPlayed % 7);
    }

    public void SetStreetLights(bool status)
    {
        foreach (var light in streetLights)
        {
            light.enabled = status;
        }
    }
    public void GatherCustomerData() {
        foreach(AIStats customer in customers) {
            if (!float.IsNaN(customer.moral))
            {
                currentDayStats.totalCustomerSatisfaction += customer.moral;
            }
        }
        currentDayStats.numberOfCustomers = customers.Count;
    }

    
    public float GetCurrentAverageMorale() {
        float totalMorale = 0;
        foreach(AIStats customer in customers) {
            totalMorale += customer.moral;
        }
        return totalMorale;
    }

    void Update() {

        //move light (sun):
        if(this.gameObject.transform.position.y < 0) {
            if(isDay) //just turned to night
                EndDay();
        } else {
            if(!isDay) //just turned to day
                StartDay();
        }
        t += Time.deltaTime;
        
        transform.RotateAround(pivotPoint.transform.position, Vector3.forward, speed * Time.deltaTime);

        //debugging stuff:
        if(transform.position.y < minPoint) {
            if(debug) minPoint = transform.position.y;
        }
        if (transform.position.y < -50.2f) {
            //if starting transform changes:
            if(debug) midnightPosition = transform.position;
            if(debug) midnightRotation = transform.rotation;

            t = 0; //reset day at minimum point
        }

        //update time:
        time = (int) (1440 * (t/timePerDay));

        UpdateTimeDisplay();
        
    }

    public bool addedDayForToday = true;
    public bool gameSaveLoaded = false;
    public void UpdateTimeDisplay() {

        uint hour = (uint)time / 60;
        uint remainder = (uint)time - (hour * 60);

        if(textures.Count > 0) {
            if(cycleImage != null && hour >= 0) {
                cycleImage.texture = textures[(int)hour];
            }
        }

        if(meridiesText != null) {
            if (hour >= 12) meridiesText.text = "PM";
            else meridiesText.text = "AM";
        }

        if(hour > 12) hour -= 12;

        string hourString = "" + hour;
        if(hour == 0) hourString = "12";
        string remainderString = "" + remainder;
        if(remainder < 10) remainderString = "0" + remainder;
        if(timeText != null) {
            timeText.text = hourString + ":" + remainderString;
            computerTimeText.text = hourString + ":" + remainderString;
        }

        if((hour == 0 && remainder == 0 && !addedDayForToday) || gameSaveLoaded) {
            if (!gameSaveLoaded)
            {
                daysPlayed++;
            }          
            dayText.text = GetWeekDay().ToString();
            weekText.text = "Week " + GetWeekNumber();
            daysPlayedText.text = "Day " + (daysPlayed + 1);
            addedDayForToday = true;    
            gameSaveLoaded = false;
        }

    }


    public void SaveData(ref GameData data)
    {
        cData.timeOfDay = t;
        cData.sunPos = transform.position;
        cData.sunRotation = transform.rotation.eulerAngles;
        cData.dayCount = daysPlayed;
        cData.isDay = isDay;
        cData.timeText = timeText.text + meridiesText.text;
        cData.weekText = GetWeekNumber().ToString();
        data.cycleData = cData;

    }

    public void LoadData(GameData data)
    {
        cData = data.cycleData;
        t = cData.timeOfDay;
        isDay = cData.isDay;
        transform.position = cData.sunPos;
        transform.rotation = Quaternion.Euler(cData.sunRotation);
        daysPlayed = cData.dayCount;
        gameSaveLoaded = true;
    }



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

public class DoorBehaviour : MonoBehaviour
{
    public float direction = 0;
    public float movementSpeed = 0;
    private Vector3 openPosition;
    private Vector3 closedPosition;
    public bool stationary = false;
    private Vector3 currentPosition;
    private Vector3 previousPosition;


    private void Start()
    {
        closedPosition = transform.position;
        openPosition = transform.position + (new Vector3(0, 0, transform.localScale.z) * 2) * direction;
    }

    public void Update()
    {
        currentPosition = transform.position;
        Vector3 velocity = (currentPosition - previousPosition) / Time.deltaTime;
        if (Mathf.Abs(velocity.magnitude) == 0)
        {
            stationary = true;
        }
        else
        {
            stationary = false;
        }
        previousPosition = currentPosition;
    }

    private void OnDestroy()
    {
        Tween.CancelAll();
    }

    public void Open()
    {
        if (stationary && gameObject != null)
        {
            Tween.Position(transform, openPosition, movementSpeed, 0, Tween.EaseBounce);           
        }
    }
    public void Close()
    {
        if (stationary && gameObject != null)
        {
            Tween.Position(transform, closedPosition, movementSpeed, 0, Tween.EaseOutStrong);
        }
    }
}

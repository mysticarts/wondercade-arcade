﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyAndDuplicate : MonoBehaviour
{
    public static DontDestroyAndDuplicate worldInstance;
    void Awake() {
        DontDestroyOnLoad(this.gameObject);
        if(worldInstance == null)
            worldInstance = this;
        else Destroy(gameObject);
    }
}

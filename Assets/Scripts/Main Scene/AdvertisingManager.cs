﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using UnityEngine.UI;

public enum AdvertisingType {
    NEWSPAPERS,
    BILLBOARDS,
    WEBSITES,
    TELEVISION
}

public class AdvertisingManager : MonoBehaviour {

    [Range(0, 100)]
    public float chanceOfChangingEffectivenessEachDay = 15;

    //for inspector only:
    public List<string> greatEffectivenessHints_Newspapers;
    public List<string> greatEffectivenessHints_Billboards;
    public List<string> greatEffectivenessHints_Websites;
    public List<string> greatEffectivenessHints_Television;

    public List<string> poorEffectivenessHints_Newspapers;
    public List<string> poorEffectivenessHints_Billboards;
    public List<string> poorEffectivenessHints_Websites;
    public List<string> poorEffectivenessHints_Television;
    //EXAMPLES:
    //  GREAT:
    //eg. There is a front page opportunity in the Herald Sun!
    //or The Super Bowl is on this weekend! There is great opportunity in websites.

    //  POOR:
    //eg. Television commercials aren't going to reach locals until a local blackout is fixed!

    //for code use:
    private Dictionary<AdvertisingType, List<string>> greatEffectivenessHints = new Dictionary<AdvertisingType, List<string>>();
    private Dictionary<AdvertisingType, List<string>> poorEffectivenessHints = new Dictionary<AdvertisingType, List<string>>();

    //effectivness values for slider: 0 -> 100
    private Dictionary<AdvertisingType, int> currentEffectiveness = new Dictionary<AdvertisingType, int>();

    public List<string> currentEffectivenessHints = new List<string>();

    private NotificationSystem notifications;

    public void Start() {
        notifications = GameObject.Find("Managers").GetComponent<NotificationSystem>();

        greatEffectivenessHints.Add(AdvertisingType.NEWSPAPERS, greatEffectivenessHints_Newspapers);
        greatEffectivenessHints.Add(AdvertisingType.BILLBOARDS, greatEffectivenessHints_Billboards);
        greatEffectivenessHints.Add(AdvertisingType.WEBSITES, greatEffectivenessHints_Websites);
        greatEffectivenessHints.Add(AdvertisingType.TELEVISION, greatEffectivenessHints_Television);

        poorEffectivenessHints.Add(AdvertisingType.NEWSPAPERS, poorEffectivenessHints_Newspapers);
        poorEffectivenessHints.Add(AdvertisingType.BILLBOARDS, poorEffectivenessHints_Billboards);
        poorEffectivenessHints.Add(AdvertisingType.WEBSITES, poorEffectivenessHints_Websites);
        poorEffectivenessHints.Add(AdvertisingType.TELEVISION, poorEffectivenessHints_Television);

        CalculateNewEffectiveness();
    }

    public Text hintsText;
    public void CalculateNewEffectiveness() {

        currentEffectivenessHints.Clear(); //reset hints

        //loop advertising types:
        for(int count = 0; count <= (int)Enum.GetValues(typeof(AdvertisingType)).Cast<AdvertisingType>().Last(); count++) {
            AdvertisingType type = (AdvertisingType)count;
            
            //remove old value
            if(currentEffectiveness.ContainsKey(type)) currentEffectiveness.Remove(type);

            int effectiveness = UnityEngine.Random.Range(0, 100);

            //add effectiveness hint if applicable
            if(effectiveness > 75) {

                //effective hints:
                List<string> hintsToChooseFrom = greatEffectivenessHints[type];
                string hintToUse = hintsToChooseFrom[UnityEngine.Random.Range(0, hintsToChooseFrom.Count)];
                currentEffectivenessHints.Add(hintToUse);

            } else if(effectiveness < 25) {

                //ineffective hints:
                List<string> hintsToChooseFrom = poorEffectivenessHints[type];
                string hintToUse = hintsToChooseFrom[UnityEngine.Random.Range(0, hintsToChooseFrom.Count)];
                currentEffectivenessHints.Add(hintToUse);

            }

            //add new value
            currentEffectiveness.Add((AdvertisingType)count, effectiveness);
        }

        if (hintsText != null)
        {
            string hints = "";
            foreach (string hint in currentEffectivenessHints)
            {
                hints += " - " + hint + "\n\n";
                notifications.Add(new Notification("Advertising Agency", hint));
            }
            if (hints == "") hints = " - There's nothing to report on today.";
            hintsText.text = hints;
        }

    }

    public Text statusText;
    public void EnableSliders(bool state) {
        for(int count = 0; count < moneySpent.Count; count++) {
            moneySpent[count].enabled = state;
        }
        if (statusText != null)
        {
            if (state == false) statusText.text = "NOT ACCEPTING CHANGES";
            else statusText.text = "ACCEPTING CHANGES";
        }

    }

    public void UpdateData() {
        for(int count = 0; count < moneySpent.Count; count++) {
            moneySpentText[count].text = "$" + moneySpent[count].value;
        }
    }

    public List<Text> moneySpentText;
    public List<Slider> moneySpent;
    [Tooltip("This value represents how much it costs per customer attracted with advertising. Eg 5$ to attract 1 customer. The higher the value, the less customers you'll get for the money spent")]
    public float dollarValuePerCustomer = 5;
    //[Tooltip("How much range should the number of customers be randomised? Eg. 20 = +/- 20% (randomised)")]
    //public int plusMinusRandomPercentage = 20;

    //for spawning:
    public int GetAdvertisingAddition() {
        float total = 0;
        //loop advertising types:
        for(int count = 0; count < moneySpent.Count; count++) {
            float cost = moneySpent[count].value;
            float effectiveness = currentEffectiveness[(AdvertisingType)count] / 100.0f;
            total += (cost*effectiveness);
        }
        total /= dollarValuePerCustomer;

        //int random = UnityEngine.Random.Range(-plusMinusRandomPercentage, plusMinusRandomPercentage);
        //return Mathf.CeilToInt(total * (1+(random/100)));
        return Mathf.CeilToInt(total);
    }
}

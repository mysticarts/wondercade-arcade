﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Rewired;

public class WorldDataSave : MonoBehaviour {

    public string mainSceneName = "James B New";

    private static WorldDataSave worldInstance;

    //data to save and load:
    public View previousView = View.THIRD;
    public Vector3 playerPosition = Vector3.zero;
    public Quaternion playerRotation = Quaternion.Euler(Vector3.zero);

    public bool readyToLoad = false;

    void Awake() {
        DontDestroyOnLoad(this.gameObject);
        if(worldInstance == null)
            worldInstance = this;
        else Destroy(gameObject);
    }

    void Update() {
        if(readyToLoad) {
            readyToLoad = false;

            GameObject player = GameObject.Find("PlayerManager");
            ViewManager viewManager = Camera.main.GetComponent<ViewManager>();
            viewManager.currentView = View.GAME;
            Camera.main.transform.position = viewManager.viewPositions[View.GAME].transform.position;
            Camera.main.transform.rotation = viewManager.viewPositions[View.GAME].transform.rotation;

            // LOAD DATA HERE:
            player.transform.position = playerPosition;
            player.transform.rotation = playerRotation;
            player.SetActive(true);

            PlayerManager playerManager = player.GetComponent<PlayerManager>();
            viewManager.Switch(previousView);
        }    
    }

    public void Load() {
        DontDestroyAndDuplicate.worldInstance.gameObject.SetActive(true);
        SceneManager.LoadScene(sceneName:mainSceneName);
        readyToLoad = true;
    }

    public void Save(string gameName) {
        GameObject player = GameObject.Find("PlayerManager");
        ViewManager viewManager = Camera.main.GetComponent<ViewManager>();

        // SAVE DATA HERE:
        previousView = viewManager.previousView;
        playerPosition = player.transform.position;
        playerRotation = player.transform.rotation;
        player.SetActive(false);

        GameObject.FindGameObjectWithTag("MainInput").SetActive(false);
        SceneManager.LoadScene(sceneName:gameName.ToString());
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;
using Rewired;

public class Office : MonoBehaviour {

    protected DayNightCycle cycle;
    public ViewManager viewManager;
    protected Player input;

    public GameObject manageButton;

    public Vector3 poppedUpSize = new Vector3(1, 0.03f, 0.5f);
    private bool playerIsInRadius = false;
    public MenuUI menu;
    private BuildingManager buildingManager;

    public void Start() {
        cycle = GameObject.Find("DayNightCycle").GetComponent<DayNightCycle>();
        input = ReInput.players.GetPlayer(0);
        PlayerManager.menuUsing = menu;
        menu.PopDown(true);
        buildingManager = FindObjectOfType<BuildingManager>();
    }

    public void Update() {

        //using menu?

        if (input.GetButtonDown("Manage"))
        {
            Debug.Log("Open");
            EnterComputer();
        }
        if (input.GetButtonDown("Back") && buildingManager.canCloseComputer)
        {
            Debug.Log("Closed");
            LeaveComputer();
        }
    }

    public void EnterComputer()
    {     
           if (playerIsInRadius)
            {
            if (viewManager.currentView != View.EDIT)
            {
                if (PlayerManager.menuUsing == null)
                    menu.PopUp(true);
            }
        }
    }

    public void LeaveComputer()
    {
        if (PlayerManager.menuUsing != null && (PlayerManager.menuUsing == menu
               || (cycle.currentDayStats != null && PlayerManager.menuUsing == cycle.currentDayStats.menu)))
        {
            PlayerManager.menuUsing.PopDown(true); //should call this to pop down EOD\
        }
    }



    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player") && this.CompareTag("OfficeComputer")) {
            playerIsInRadius = true;

            PopUpButton();
        }
    }
    public void PopUpButton() {
        //pop up manage button:
        Tween.LocalScale(manageButton.transform, poppedUpSize, 0.75f, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
    }

    private void OnTriggerExit(Collider other) {
        if(other.CompareTag("Player") && this.CompareTag("OfficeComputer")) {
            playerIsInRadius = false;

            PopDownButton();
        }
    }
    public void PopDownButton() {
        //pop down manage button:
        Tween.LocalScale(manageButton.transform, Vector3.zero, 0.2f, 0, Tween.EaseOutStrong, Tween.LoopType.None, null, null, false);
    }

}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using Pixelplacement;
using UnityEngine.UI;
using TMPro;

public enum VendorType {
    DRINK,
    FOOD
}

public class Vendor : Machines {
    public MenuUI_Vending menu_vending;

    public VendorType vendorType;
    [HideInInspector]
    public bool occupied = false;

    public int costPerStock = 1;
    public float maxStock = 10;
    public int maxStockPerUpgrade = 0;
    public int stock = 0;

    public GameObject manageButton; //diegetic ui
    public GameObject stockButton; //diegetic ui
    public Vector3 poppedUpSizeManage = new Vector3(1, 0.03f, 0.5f);
    public Vector3 poppedUpSizeStock = new Vector3(1, 0.03f, 0.5f);

    public int transactions = 0;
    public int totalSales = 0;
    public int totalCostOfStock = 0;

    //the position the AI stand at when buying stock.
    public Transform AIStandingPos;

    private NotificationSystem notifications;

    private void Start()
    {
        base.Start();
        notifications = GameObject.Find("Managers").GetComponent<NotificationSystem>();

        UpdateBar(menu_vending.stockBar, stock / maxStock);
        menu_vending.stock.text = "" + (int)stock;
        UpdateBar(menu_vending.levelBar, (float)currentLevel / (float)levelPrefabs.Count);
        menu_vending.level.text = "" + currentLevel;
        UpdateBar(menu_vending.stockBar, (float)stock / (float)maxStock);
    }

    public void Update() {
        base.Update();
        if(playerIsInRadius) {
            if(input.GetButtonDown("PlayArcade")) {
                BuyStock();
            }
        }
    }

    public override void Upgrade()
    {
        if (currentLevel < levelPrefabs.Count)
        {
            int costToUpgrade = initialCostToUpgrade + ((currentLevel - 1) * costIncreasePerLevel);
            if (economy.money >= costToUpgrade)
            {
                maxStock += maxStockPerUpgrade;
                UpdateBar(menu_vending.stockBar, (float)stock / (float)maxStock);
            }
        }
        base.Upgrade();      
    }


    public override void ShowDiegeticUI(bool show) {
        //pop up manage button:

        if(show) {
            Tween.LocalScale(manageButton.transform, poppedUpSizeManage, 0.75f, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
            if(stock != maxStock)
                //pop up stock button
                Tween.LocalScale(stockButton.transform, poppedUpSizeStock, 0.75f, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
        } else {
            //pop down all buttons:
            Tween.LocalScale(manageButton.transform, Vector3.zero, 0.2f, 0, Tween.EaseOutStrong, Tween.LoopType.None, null, null, false);
            Tween.LocalScale(stockButton.transform, Vector3.zero, 0.2f, 0, Tween.EaseOutStrong, Tween.LoopType.None, null, null, false);
        }

    }

    public override void SetMenu() {
        base.menu = menu_vending;

    }

    public void BuyStock() {
        if(stock < (maxStock)) {
            if(economy.money >= costPerStock) {
                economy.Deduct(costPerStock);
                stock++;

                //UI:
                menu_vending.stock.text = "" + (int)stock;
                UpdateBar(menu_vending.stockBar, (float)(stock/maxStock));

                totalCostOfStock += costPerStock;
            } else notifications.Add(new Notification("Financial Advisor", "Not enough funds to buy more stock!"));
        } else notifications.Add(new Notification("Vending Management", "This machine is already fully stocked!"));
    }

    public void TakeStock()
    {
        if (stock > 0)
        {
            stock--;
            menu_vending.stock.text = "" + (int)stock;
            UpdateBar(menu_vending.stockBar, (float)(stock / maxStock));
        }
    }

    public override void SaveData(GameData data)
    {
        iData.stockLevel = stock;
        iData.itemLevel = currentLevel;
        iData.maxStock = (int)maxStock;
        if (occupant !=  null)
        {
            iData.AIOccupant = occupant;
        }
        base.SaveData(data);
    }

    public override void LoadData(GameData data)
    {       
        base.LoadData(data);
        stock = iData.stockLevel;
        currentLevel = iData.itemLevel;
        costSlider.value = ussageCost;
        maxStock = iData.maxStock;
        if (iData.AIOccupant != null)
        {
            occupant = iData.AIOccupant;
        }


    }


}

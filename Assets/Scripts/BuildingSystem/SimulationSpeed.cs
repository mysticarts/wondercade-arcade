﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SimulationSpeed : MonoBehaviour
{
    public GameObject pauseButton;
    public GameObject playButton;


    public void SetSpeed(float speed)
    {
        if (speed != 1)
        {
            pauseButton.SetActive(false);
            playButton.SetActive(true);
        } 
        if (speed == 1)
        {
            pauseButton.SetActive(true);
            playButton.SetActive(false);
        }

        Time.timeScale = speed;

        EventSystem.current.SetSelectedGameObject(null);
    }
  
}

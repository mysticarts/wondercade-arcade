﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum GridType
{
    FLOOR = 0,
    WALL = 1,
}


public class GenPointProperties : MonoBehaviour
{
    public GridType gridType;
    public bool unlockedOnStart = false;
    public List<GameObject> gridsToUnlock = new List<GameObject>();
    public GameObject displayWall;
    public float sizeX = 0;
    public float sizeY = 0;
    public float sizeZ = 0;
    BuildingManager buildingManager;
    public GameObject destructionUI;
    public int destructionCost = 0;
    [Tooltip("The cleaning button to unlock when the this grid gets destroyed.")]
    public GameObject cleaningButton;
    public GameObject roomLight;

    private void Start()
    {
        buildingManager = FindObjectOfType<BuildingManager>();
    }


    private void SetDestructionUI(bool status)
    {
        if (buildingManager == null)
        {
            buildingManager = FindObjectOfType<BuildingManager>();
        }
        
        if (buildingManager != null && destructionUI != null)
        {            
            if (status)
            {
                if (buildingManager.destroyWallMode)
                {
                    if (!buildingManager.destructionButtons.Contains(destructionUI.transform.GetChild(0).gameObject))
                    {
                        buildingManager.destructionButtons.Add(destructionUI.transform.GetChild(0).gameObject);
                    }
                    destructionUI.SetActive(true);
                }
            }
            else
            {
                destructionUI.SetActive(false);
            }
        }    
    }

    private void OnEnable()
    {       
        SetDestructionUI(true);
        if (buildingManager.unlockedGrids.Contains(gameObject))
        {
            if (roomLight != null)
            {
                if (!roomLight.activeInHierarchy)
                {
                    roomLight.SetActive(true);
                }
            }
        }
        
    }

    private void OnDisable()
    {
        SetDestructionUI(false);
    }

    private void OnDestroy()
    {      
        if (destructionUI != null)
        {
            Destroy(destructionUI);
        }

       

        if (buildingManager != null)
        {
            buildingManager.destoryedGrids.Add(gameObject.name);
            buildingManager.unlockedGrids.Remove(gameObject);
            
            if (displayWall != null)
            {
                Destroy(displayWall);
            }

            foreach (var grid in gridsToUnlock)
            {
                if (grid != null)
                {
                    if (!buildingManager.unlockedGrids.Contains(grid))
                    {
                        buildingManager.unlockedGrids.Add(grid);
                        if (grid.GetComponent<GenPointProperties>().gridType == GridType.FLOOR) {
                            buildingManager.unlockedRooms++;
                        }
                    }
                }
            }

            
            if (buildingManager.destroyWallMode)
            {
                buildingManager.EnableGrids(true);
            }

        }     
    }
}




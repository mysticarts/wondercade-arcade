﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ItemType
{
    VENDOR = 0,
    ARCADE = 1,
    DECORATION = 2
}

[System.Serializable]
public struct ItemData
{
    public string itemName;
    public Vector3 itemPosition;
    public Vector3 itemRotation;
    public ArcadeState itemState;
    public int timesUsed;
    public int itemCost;
    public int stockLevel;
    public int itemLevel;
    public int maxStock;
    public bool startItem;
    public float breakageAmount;
    public string ID;
    public bool occupied;
    public bool advanceQueue;
    public List<string> AIInQueue;
    public string AIOccupant;
}


public class Item : MonoBehaviour
{
    public string iname;
    public CellType placeType;
    public ItemType itemType;
    public bool ignoreSerialization = false;
    //shop stuff:
    public Sprite shopImage; //the image used in the shop menu
    public int cost = 0;
    public int ussageCost = 0;
    [HideInInspector]
    public ItemData iData = new ItemData();
    [HideInInspector]
    public string InstanceID;

    virtual public void SaveData(GameData data)
    {
        string instance = "(Clone)";
        
        iData.itemName = transform.name;

        iData.ID = InstanceID;
        if (iData.itemName.Contains("Clone"))
        {
            iData.itemName = iData.itemName.TrimEnd(instance.ToCharArray());
        }
        iData.itemPosition = transform.position;
        iData.itemRotation = transform.rotation.eulerAngles;
        iData.itemCost = ussageCost;
        data.itemsData.Add(iData);
    }

    virtual public void LoadData(GameData data)
    {
        ussageCost = iData.itemCost;
        InstanceID = iData.ID;
    }

  
}

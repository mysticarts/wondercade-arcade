﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Rewired;

public class GridNav : MonoBehaviour {
    public BuildingManager buildingManager;
    public Transform currentCell;

    // Update is called once per frame
    void Update() {
        if (buildingManager.placement.input != null) {
            if (buildingManager.placement.input.controllers.joystickCount == 0) {
                buildingManager.placement.cursorPosition = Input.mousePosition;
            } else {
            //    float xDelta = buildingManager.placement.input.GetAxis("CursorX");
              //  float yDelta = buildingManager.placement.input.GetAxis("CursorY");
           //     buildingManager.placement.cursorPosition += new Vector2(xDelta, yDelta) * buildingManager.placement.cursorSensitivity;
            }
        }

        if (Camera.main != null) {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(buildingManager.placement.cursorPosition);
            if (Physics.Raycast(ray, out hit, 10000))
            {
                if (hit.transform.gameObject.tag == "GridSquare" && buildingManager.selectedItem != null)
                {
                    if (currentCell != hit.transform)
                    {
                        CellProperties cell = hit.transform.GetComponent<CellProperties>();
                        Item item = buildingManager.selectedItem;
                        if (item.placeType == cell.cellType) {
                            buildingManager.selectedItem.gameObject.SetActive(true);
                            currentCell = hit.transform;
                            buildingManager.placement.cellInfo = currentCell.GetComponent<CellProperties>();
                            Reposition(hit.transform, hit.normal * -1, item.placeType);
                            buildingManager.placement.blockChange = false;
                        }
                        else
                        {
                            buildingManager.selectedItem.gameObject.SetActive(false);
                        }
                    }
                }
            }
        }
    }


    public void Reposition(Transform desiredPosition, Vector3 direction, CellType type) {
        if (type == CellType.FLOOR) {
            buildingManager.placement.transform.position = desiredPosition.position + new Vector3(0, 1, 0);
        }
        if (type == CellType.WALL) {
            if (direction != new Vector3(0,-1,0))
            {
                buildingManager.placement.transform.position = desiredPosition.position;
                buildingManager.placement.transform.forward = direction;
                buildingManager.placement.transform.position += buildingManager.placement.transform.forward * -0.6f;
            }

        }

    }

}

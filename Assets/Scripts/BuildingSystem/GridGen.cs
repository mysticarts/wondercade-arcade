﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GridGen : MonoBehaviour
{
    private Transform generationPoint;
    public GameObject floorCell;
    public GameObject wallCell;
    private BuildingManager buildingManager;
    public string SectionName = "";
    [HideInInspector]
    public GameObject section;
    public List<GenPointProperties> genPoints = new List<GenPointProperties>();

    // Start is called before the first frame update
    void Start()
    {
        buildingManager = FindObjectOfType<BuildingManager>();
        generationPoint = transform;
        GenerateRoom();
    }

    public void GenerateRoom()
    {
        if (section == null)
        {
            section = new GameObject(SectionName);
        }
        buildingManager.grids.Add(section);
        
        foreach(var point in genPoints)
        {
            GenerateSection(point);
            if (point.unlockedOnStart)
            {
                buildingManager.unlockedGrids.Add(point.gameObject);
            }
            point.gameObject.SetActive(false);
        }
        buildingManager.generatedGrids.Add(section);
    }

    void GenerateSection(GenPointProperties genPoint)
    {
        genPoint.transform.parent = section.transform;
        GameObject cell = null;
        if (genPoint.gridType == GridType.FLOOR)
        {
            cell = floorCell;
        }
        if (genPoint.gridType == GridType.WALL)
        {
            cell = wallCell;
        }
        for (int x = 0; x <= genPoint.sizeX; x++)
        {
            for (int y = 0; y <= genPoint.sizeY; y++)
            {
                for (int z = 0; z <= genPoint.sizeZ; z++)
                {
                    Transform point = genPoint.transform;
                    Vector3 offset = new Vector3(point.position.x + x, point.position.y + y, point.position.z + z);
                    GameObject gridCube = Instantiate(cell, offset, point.rotation, genPoint.transform);
                    CellProperties cellProps = gridCube.GetComponent<CellProperties>();
                    cellProps.genPointProps = genPoint;
                    cellProps.grid = genPoint.gameObject;
                    gridCube.name = SectionName + " X:" + x + " Y:" + y + " Z:" + z;
                }
            }
        }
    }
}

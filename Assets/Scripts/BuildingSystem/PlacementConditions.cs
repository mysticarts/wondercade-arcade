﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlacementConditions : MonoBehaviour
{
    public bool canPlace = true;
    private Economy economy;
    public TextMeshProUGUI moneyDisplay;
    public BuildingManager buildingManager;
    public LayerMask ignoreMask;
    // public List<MeshRenderer> ren;

    private void Start()
    {
        economy = FindObjectOfType<Economy>();
    }

    // Update is called once per frame
    void Update()
    {
        if (buildingManager.selectedItem != null)
        {
            int predictedBalance = economy.money - buildingManager.selectedItem.cost;
            if (predictedBalance < 0)
            {
                canPlace = false;
            }
        }


        if (canPlace)
        {
            SetColor(Color.green);
        }
        if (!canPlace)
        {
            SetColor(Color.red);

        }
    }

    void SetColor(Color color)
    {
        if (transform.childCount > 0)
        {
            List<Renderer> ren = new List<Renderer>(GetComponentsInChildren<Renderer>());
            foreach (var r in ren)
            {
                if (r != null)
                {
                    r.material.color = color;
                }
            }
        }
        
    }


    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer != 8)
        {
            canPlace = false;

        }
    }


    private void OnTriggerExit(Collider other)
    {
            canPlace = true;
        //if (money > placement.cost)
        //{
        //}
    }





}

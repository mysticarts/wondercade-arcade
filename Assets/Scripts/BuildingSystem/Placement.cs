﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Rewired;
using Pixelplacement;
using Rewired.ControllerExtensions;



public class Placement : MonoBehaviour
{
    [HideInInspector]
    public Player input;
    public int blockIter = 0;
    public bool blockChange = false;
    public float cost = 0;
    public PlayerController_Default player;
    public CellProperties cellInfo;
    public Vector2 cursorPosition;
    public float cursorSensitivity = 0;
    private bool rotated = false;
    public BuildingManager buildingManager;
    //public float clipRadius = 0;
    public List<GameObject> closetRendererCells = new List<GameObject>();
    public float radius = 0;
    public Material mat;
    public Material itemMat;
    public GameObject placementParticle;

    public int getClosetCellDelay = 0;
    public int currentDelay;

    private float delayTimer = 0.5f;
    private float currentDelayTimer = 0;
    private string prefabName;

    private NotificationSystem notifications;

    // Start is called before the first frame update
    void Start()
    {
        input = ReInput.players.GetPlayer(0);
        gameObject.SetActive(false);
        //mat.SetVector("Centre", transform.position);

        notifications = GameObject.Find("Managers").GetComponent<NotificationSystem>();
    }

    void SetClosetCells(float radius)
    {
        if (closetRendererCells.Count > 0)
        {
            foreach (var col in closetRendererCells)
            {
                col.SetActive(false);
            }
        }
        closetRendererCells.Clear();

        Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius);
        foreach(var hit in hitColliders)
        {
            if (hit.gameObject.layer == 8)
            {
                if (hit.transform.childCount > 0)
                {
                    closetRendererCells.Add(hit.transform.GetChild(0).gameObject);
                    if (hit.transform.childCount > 1)
                    {
                        closetRendererCells.Add(hit.transform.GetChild(1).gameObject);
                    }

                }
            }
        }
        foreach (var col in closetRendererCells)
        {
            col.SetActive(true);
        }
    }


    // Update is called once per frame
    void Update()
    {
        if (buildingManager.selectedItem != null)
        {
            if (currentDelayTimer < delayTimer)
            {
                currentDelayTimer += Time.unscaledDeltaTime;
            }
        }
    
        if (buildingManager.selectedItem != null)
        {
            if (buildingManager.selectedItem.placeType == CellType.FLOOR)
            {
                if (input.GetButtonDown("RotateObj") && !rotated)
                {
                    Tween.Rotate(transform, new Vector3(0, 90, 0), Space.World, 0.2f, 0,null,Tween.LoopType.None,null,OnRotateComplete);
 
                    rotated = true;
                }
                if (input.GetNegativeButton("RotateObj") && !rotated)
                {
                    Tween.Rotate(transform, new Vector3(0, -90, 0), Space.World, 0.2f, 0, null, Tween.LoopType.None, null, OnRotateComplete);

                    rotated = true;
                }
            }

            mat.SetVector("_Centre", transform.position);

            if (currentDelay >= getClosetCellDelay)
            {
                SetClosetCells(radius);
                currentDelay = 0;
            }
            else
            {
                currentDelay++;
            }
        }

        if (input.GetButtonDown("Place"))
        {
            if (buildingManager.purchaseMode)
            {
                if (buildingManager.conditions.canPlace)
                {
                    Place();
                }
            }
            if (buildingManager.editMode)
            {
                if (buildingManager.selectedItem == null)
                {
                    RaycastHit hit;
                    Ray ray = Camera.main.ScreenPointToRay(cursorPosition);
                    if (Physics.Raycast(ray, out hit))
                    {
                        buildingManager.selectedItem = hit.transform.GetComponent<Item>();
                        if (buildingManager.selectedItem != null)
                        {
                            buildingManager.selectedItem.transform.parent = transform;
                            buildingManager.EnableGrids(true);
                            buildingManager.selectedItem.transform.position = transform.position;
                            buildingManager.selectedItem.transform.rotation = transform.rotation;
                            buildingManager.IgnoreRayCasts(true);
                        }
                    }
                }
                else
                {
                    if (buildingManager.conditions.canPlace)
                    {
                        buildingManager.selectedItem.transform.parent = null;
                        buildingManager.selectedItem = null;
                        buildingManager.EnableGrids(false);
                        buildingManager.IgnoreRayCasts(false);
                    }   
                }

            }        
        }

        if (input.GetButtonDown("SellItem"))
        {
            if (buildingManager.editMode)
            {
                if (buildingManager.selectedItem != null)
                {
                    float returnAmount = 0.7f * buildingManager.selectedItem.cost;
                    buildingManager.economy.Give((int)returnAmount);
                    Destroy(buildingManager.selectedItem.gameObject);
                    buildingManager.EnableGrids(false);
                }
            }
        }
    }

    private void OnRotateComplete()
    {
        rotated = false;
    }

    public void setCost(float newCost)
    {
         cost = newCost;
    }

    GameObject FetchRoom(string name)
    {
        foreach (var room in buildingManager.generatedGrids)
        {
            if (room.name == name)
            {
                return room;
            }
        }
        return null;
    }

    public void SetSelectedObject(Item select)
    {
        Debug.Log("Set");
        prefabName = select.name;

        buildingManager.selectedItem = Instantiate(select, transform.position, transform.rotation, transform);
        buildingManager.selectedItem.ignoreSerialization = true;
        buildingManager.selectedItem.gameObject.layer = 2;

        BoxCollider[] itemColliders = buildingManager.selectedItem.GetComponents<BoxCollider>();
        foreach (var col in itemColliders)
        {
            col.isTrigger = true;
        }

        if (input.controllers.joystickCount > 0)
        {
            cursorPosition = new Vector2(Screen.width / 2, Screen.height / 2);
        }

        if (buildingManager.grid.currentCell != null)
        {
            buildingManager.grid.Reposition(buildingManager.grid.currentCell, Vector3.zero, buildingManager.selectedItem.placeType);
        }

        blockChange = true;
        buildingManager.selectedItem.gameObject.SetActive(true);
        buildingManager.conditions.canPlace = true;
    }

    void Place()
    {
        if (buildingManager.selectedItem != null && currentDelayTimer >= delayTimer)
        {
            if(MachineManager.GetNumberOfMachines(buildingManager.selectedItem.iname) < 3) {
                GameObject prefab = Resources.Load<GameObject>("PlaceableObjects/" + prefabName);

                GameObject placedObj = Instantiate(prefab, transform.position, transform.rotation);

                Destroy(Instantiate(placementParticle, transform.position, transform.rotation), 2);

                //track references
                if(prefab.GetComponent<Arcade>() != null) {
                    MachineManager.arcades.Add(prefab.GetComponent<Arcade>());
                }
                if(prefab.GetComponent<Vendor>() != null) {
                    MachineManager.vendors.Add(prefab.GetComponent<Vendor>());
                }

                buildingManager.economy.Deduct(buildingManager.selectedItem.cost);
            } else notifications.Add(new Notification("Arcade Management", "We have the maximum amount of " + buildingManager.selectedItem.iname + " machines!"));
        }

    }

   



}

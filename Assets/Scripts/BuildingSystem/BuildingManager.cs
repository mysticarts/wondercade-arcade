﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Rewired;
using TMPro;
using Pixelplacement;

public class BuildingManager : MonoBehaviour
{
    [Header("----Core System----")]
    [Tooltip("The script that handles positioning and placement of objects")]
    public Placement placement;
    [Tooltip("The script that handles if the conditions are met for an object to be placed")]
    public PlacementConditions conditions;
    [Tooltip("Script that handles current position in the grid")]
    public GridNav grid;
    [HideInInspector]
    public List<GameObject> grids = new List<GameObject>();
    [HideInInspector]
    public Item highlightedItem;
    //[HideInInspector]
    public Item selectedItem;
    public GameObject cursor;
    [HideInInspector]
    public List<GameObject> generatedGrids = new List<GameObject>();
    [HideInInspector]
    public List<GameObject> unlockedGrids = new List<GameObject>();
 //   [HideInInspector]
    public List<GameObject> destructionButtons = new List<GameObject>();
    public int unlockedRooms = 1;
    [HideInInspector]
    public List<string> destoryedGrids = new List<string>();
    public GameObject highlightedWall;
    public GenPointProperties highlightedSectionProps;
    public Economy economy;
    public LayerMask wallMask;
    private Office office;
    [Header("----Player----")]
    public PlayerController_Default player;
    public GameObject editorUI;
    public Image _cursorDisplay;
    public Texture2D _CursorText;
    public EventSystem eventSystem;
    public GameObject firstSelected;
    private int wallCost = 0;
    [HideInInspector]
    public bool purchaseMode = false;
    [HideInInspector]
    public bool editMode = false;
    [HideInInspector]
    public bool destroyWallMode = false;
    [HideInInspector]
    public bool quitting = false;
    [HideInInspector]
    public bool cleaningMode = false;
    public List<GameObject> cleaningButtons = new List<GameObject>();
    private MachineManager machineManager;
    [Header("----UI----")]
    private bool panelOpen = false;
    public GameObject confirmationPanel;
    public GameObject errorPanel;
    public TextMeshProUGUI panelHeaderText;
    public Button deleteButton;
    public MenuUI_Office officeUI;
    public GameObject virtualCursor;
    [HideInInspector]
    public bool canCloseComputer = true;
    private float leaveDelayTimer = 0;
    private void Start()
    {
        office = FindObjectOfType<Office>();
        machineManager = FindObjectOfType<MachineManager>();
     //   mouse = placement.input.controllers.;
     
    }

    private void Update()
    {
       
        if (placement.input.GetButtonDown("Back"))
        {
            virtualCursor.SetActive(false);
            _cursorDisplay.gameObject.SetActive(false);
            machineManager.editMode_ui.PopDown();
            if (editMode)
            {
                SetEditMode(false);
            }
            if (destroyWallMode)
            {
                SetDestroyWallMode(false);
            }
            if (cleaningMode)
            {
                SetCleaningMode(false);
            }
            if (purchaseMode)
            {
                SetPurchaseMode(false);
            }
        }

        if (placement.input.controllers.joystickCount == 0)
        {
            virtualCursor.SetActive(true);
        }
        else
        {
            if (!purchaseMode && !editMode && !destroyWallMode && !cleaningMode)
            {
                virtualCursor.SetActive(false);
            }
        }
        if (purchaseMode || editMode || destroyWallMode || cleaningMode)
        {
            leaveDelayTimer = 0;
            canCloseComputer = false;
            if (office.manageButton.activeInHierarchy)
            {
                office.manageButton.SetActive(false);
            }
            if (!panelOpen)
            {
                EnableCursor();
            }
        }
        else
        {
            if (!office.manageButton.activeInHierarchy)
            {
                office.manageButton.SetActive(true);
            }
        }
        if (leaveDelayTimer < 0.5f)
        {
            leaveDelayTimer += Time.deltaTime;
        }
        else
        {
            canCloseComputer = true;
        }

    }

    void EnableCursor()
    {
        if (placement.input.controllers.joystickCount == 0)
        {
            if (_cursorDisplay != null)
            {
                _cursorDisplay.gameObject.SetActive(false);
            }
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            placement.cursorPosition = _cursorDisplay.transform.position;
            virtualCursor.SetActive(true);
            //_cursorDisplay.transform.position = placement.input.controllers.Mouse.screenPosition;
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            if (!_cursorDisplay.gameObject.activeInHierarchy)
            {             
                _cursorDisplay.gameObject.SetActive(true);

                eventSystem.SetSelectedGameObject(null);

            }
        }
    }


    public void DestroyWallPrompt(int cost)
    {
        wallCost = cost;
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(placement.cursorPosition);
        if (Physics.Raycast(ray, out hit))
        {
            Debug.Log(hit.transform.name);
            if (hit.transform.gameObject.layer == 8 && hit.transform.position.y > 0)
            {
                CellProperties cell = hit.transform.GetComponent<CellProperties>();
                if (cell != null)
                {
                    panelOpen = true;
                    if (economy.money >= cell.genPointProps.destructionCost)
                    {
                        if (highlightedWall == null)
                        {
                            Tween.LocalScale(confirmationPanel.transform, new Vector3(1, 1, 1), 0.2f, 0);
                        }

                        if (highlightedWall != null)
                        {
                            Tween.LocalScale(confirmationPanel.transform, new Vector3(0, 0, 0), 0.2f, 0);
                            Tween.LocalScale(confirmationPanel.transform, new Vector3(1, 1, 1), 0.2f, 0.3f);
                        }
                        virtualCursor.gameObject.SetActive(false);
                        _cursorDisplay.gameObject.SetActive(false);
                        eventSystem.SetSelectedGameObject(confirmationPanel.transform.GetChild(1).gameObject);
                        highlightedWall = hit.transform.parent.gameObject;

                    }
                    else
                    {
                        //NOT ENOUGH FUNDS
                        virtualCursor.gameObject.SetActive(false);
                        _cursorDisplay.gameObject.SetActive(false);
                        eventSystem.SetSelectedGameObject(confirmationPanel.transform.GetChild(1).gameObject);
                        Tween.LocalScale(errorPanel.transform, new Vector3(1, 1, 1), 0.2f, 0);
                    }
                }
            }
        }
    }

    public void DestroyWall()
    {
        Destroy(highlightedWall);
        Tween.LocalScale(confirmationPanel.transform, new Vector3(0, 0, 0), 0.2f, 0);
        economy.Deduct(wallCost);
       // destructionButtons.RemoveAt(destructionIter);
        panelOpen = false;
    }

    public void DismissError()
    {
        panelOpen = false;
        Tween.LocalScale(errorPanel.transform, new Vector3(0, 0, 0), 0.2f, 0);
    }

    public void CancelWallDestruction()
    {
        Tween.LocalScale(confirmationPanel.transform, new Vector3(0, 0, 0), 0.2f, 0);
        highlightedWall = null;
        panelOpen = false;
    }

    public void IgnoreRayCasts(bool status)
    {
        Item[] placedItems = FindObjectsOfType<Item>();

        if (status)
        {
            foreach (var item in placedItems)
            {
                item.gameObject.layer = 2;
            }
        }
        else
        {
            foreach (var item in placedItems)
            {
                item.gameObject.layer = 0;
            }
        }      
    }  

    public void EnableGrids(bool status)
    {
        if (!unlockedGrids.Contains(null))
        {
            foreach (var grid in unlockedGrids)
            {
                grid.SetActive(status);
            }
        }
       
    }

    void DisableCellRenderers()
    {
        foreach (var cell in placement.closetRendererCells)
        {
            cell.SetActive(false);
        }
        placement.closetRendererCells.Clear();
    }
 
    public void SetEditMode(bool status)
    {
        editMode = status;

        if (status)
        {
            officeUI.PopDown(true);
            player.viewManager.Switch(View.EDIT);
            placement.input.controllers.maps.SetMapsEnabled(false, "Default");
            machineManager.editMode_ui.PopUp();
        }
        else
        {
            player.viewManager.Switch(player.viewManager.previousView);
            office.EnterComputer();
            placement.input.controllers.maps.SetMapsEnabled(true, "Default");
        }
        placement.input.controllers.maps.SetMapsEnabled(status, "PlayerControllerEditor");
        placement.input.controllers.maps.SetMapsEnabled(status, "Building");

        cursor.SetActive(status);
    }

    public void SetDestroyWallMode(bool status)
    {
        if (status)
        {
            officeUI.PopDown(true);
            player.viewManager.Switch(View.EDIT);
            placement.input.controllers.maps.SetMapsEnabled(false, "Default");
            machineManager.editMode_ui.PopUp();
        }
        else
        {
            player.viewManager.Switch(player.viewManager.previousView);
            office.EnterComputer();
            placement.input.controllers.maps.SetMapsEnabled(true, "Default");
        }
        placement.input.controllers.maps.SetMapsEnabled(status, "PlayerControllerEditor");
        destroyWallMode = status;
        placement.input.controllers.maps.SetMapsEnabled(status, "Building");
        EnableGrids(status);
    }

    public void SetCleaningMode(bool status)
    {
        if (status)
        {
            officeUI.PopDown(true);
            player.viewManager.Switch(View.EDIT);
            placement.input.controllers.maps.SetMapsEnabled(false, "Default");
            machineManager.editMode_ui.PopUp();
        }
        else
        {
            player.viewManager.Switch(player.viewManager.previousView);
            office.EnterComputer();
            placement.input.controllers.maps.SetMapsEnabled(true, "Default");
        }
        placement.input.controllers.maps.SetMapsEnabled(status, "PlayerControllerEditor");
        //placement.input.controllers.maps.SetMapsEnabled(status, "UINav");
        placement.input.controllers.maps.SetMapsEnabled(status, "Building");

        foreach(var button in cleaningButtons)
        {
            CleanRoom cleaning = button.GetComponentInChildren<CleanRoom>();
            if (cleaning != null)
            {
                if (!cleaning.cleaned)
                {
                    if (unlockedGrids.Contains(cleaning.floorGrid.gameObject))
                    {
                        button.SetActive(status);
                    }
                }
            }          
        }
        cleaningMode = status;
    }

    public void SetPurchaseMode(bool status)
    {
        EnableGrids(status);
        cursor.SetActive(status);
        placement.input.controllers.maps.SetMapsEnabled(status, "Building");
        placement.input.controllers.maps.SetMapsEnabled(status, "PlayerControllerEditor");
        purchaseMode = status;

        DisableCellRenderers();
        
        if (!status && placement != null)
        {
            player.viewManager.Switch(player.viewManager.previousView);
            office.EnterComputer();
            
            placement.input.controllers.maps.SetMapsEnabled(true, "Default");
            if (selectedItem != null)
            {
                selectedItem = null;
            }
            List<Transform> displayObjects = new List<Transform>();

            for (int i = 0; i < cursor.transform.childCount; i++)
            {
                displayObjects.Add(cursor.transform.GetChild(i));
            }

            foreach (var obj in displayObjects)
            {
                Destroy(obj.gameObject);
            }

            //foreach (var obj in displayObjects)
            //{
            //    Destroy(obj.gameObject);
            //}
        }
        if (status)
        {
          //  officeUI.PopDown(true);
            player.viewManager.Switch(View.EDIT);
            placement.input.controllers.maps.SetMapsEnabled(false, "Default");
        }
    }
}

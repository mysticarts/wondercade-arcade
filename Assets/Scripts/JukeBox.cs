﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Audio;
using System.IO;

public class JukeBox : Item
{

    public AudioSource audioSource;

    private List<AudioClip> songs = new List<AudioClip>();
    private int currentTrack = 0;
    public string musicFolderName = "Music";
    public string fileName = "";
    public string extension = "";
    float position = 0;
   // public int frequency = 0;

    // Start is called before the first frame update
    void Start()
    {
        LoadMusic();

        if (songs.Count > 0)
        {
            audioSource.clip = songs[currentTrack];
            audioSource.Play();
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (songs.Count > 0)
        {
            position += Time.deltaTime;
            if (!audioSource.isPlaying)
            {
                currentTrack++;
                position = 0;
                if (currentTrack == songs.Count)
                {
                    currentTrack = 0;
                }
                Debug.Log("Song Change");
                audioSource.clip = songs[currentTrack];
                audioSource.Play();


            }
        }




    }

    void LoadMusic()
    {
        if (!Directory.Exists(Application.persistentDataPath + musicFolderName))
        {
            Directory.CreateDirectory(Application.persistentDataPath + "/" + musicFolderName);
        }

        var info = Directory.GetFiles(Application.persistentDataPath + "/" + musicFolderName + "/");

        foreach (var file in info)
        {
            songs.Add(LoadClip(file, Path.GetFileName(file)));
        }
    }



    AudioClip LoadClip(string path, string songName)
    {
        string file = path;

        byte[] wav = File.ReadAllBytes(file);

        float[] data = BytesToFloats(wav);




        int channels = wav[22];

        int pos = 12;

        while (!(wav[pos] == 100 && wav[pos + 1] == 97) && wav[pos + 2] == 116 && wav[pos + 3] == 97)
        {
            pos += 4;
            int chuckSize = wav[pos] + wav[pos + 1] * 256 + wav[pos + 2] * 65536 + wav[pos + 3] * 16777216;
            pos += 4 + chuckSize;
        }

        pos += 8;
 

        int samples = (wav.Length - pos) / 8;


        //int bytesPerFrame = samples * channels;

        //int framesInFile = data.Length / bytesPerFrame;
        //samples = framesInFile;

        //if (channels == 2) samples /= 2;

        byte[] audioData = new byte[samples];

        Array.Copy(wav, pos, audioData, 0, samples);

        float[] audioSamples = BytesToFloats(audioData);
        int frequency = BitConverter.ToInt32(wav, 24);
        AudioClip clip = AudioClip.Create(songName, samples, channels, frequency, true, false);
        clip.SetData(audioSamples, 0);
        return clip;
    }

    private static float[] Int16ToFloats(Int16[] array)
    {
        float[] floatArray = new float[array.Length];
        for (int i = 0; i < floatArray.Length; i++)
        {
            floatArray[i] = ((float)array[i] / short.MaxValue);
        }
        return floatArray;
    }

    private static float[] BytesToFloats(byte[] array)
    {
        //1 Short = 2 bytes, so divide by 2
        float[] floatArray = new float[array.Length / 2];
        //Iterate through and populate array
        for (int i = 0; i < floatArray.Length; i++)
        {
            //Convert each set of 2 bytes into a short and scale that to be in the range of -1, 1
            floatArray[i] = (BitConverter.ToInt16(array, i * 2) / (float)(short.MaxValue));
        }
        return floatArray;
    }

}

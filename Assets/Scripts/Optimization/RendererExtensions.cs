﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RendererExtensions
{
    public static bool IsVisibleFrom(this Renderer renderer, Camera camera)
    {
        if (camera != null) {
            Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera); //calculate the planes from the cameras frustum
            return GeometryUtility.TestPlanesAABB(planes, renderer.bounds); //if the renderer collides with the camera frustum, return true
        }
        return false;
    }
}


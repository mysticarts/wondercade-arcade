﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class CurrentSave : MonoBehaviour
{
    public string selectedSave;
    public Serialization serialization;
    public bool newGame = false;
    public string playersGender;
    public string arcadesName;
    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(gameObject);


    }

    public void NewGame()
    {
        if (!Directory.Exists(Application.persistentDataPath + "/GameSaves"))
        {
            Directory.CreateDirectory(Application.persistentDataPath + "/GameSaves");
        }
        newGame = true;
        var info = new DirectoryInfo(Application.persistentDataPath + "/GameSaves");
        //  var fileInfo = info.GetDirectories();
        selectedSave = Application.persistentDataPath + "/GameSaves/" + "GameSave" + info.GetDirectories().Length;
        Directory.CreateDirectory(selectedSave);
    }

    public void LoadGame(string savePath)
    {
        selectedSave = savePath;
        selectedSave = selectedSave.Replace("/GameData.Arcade","");
    }

    public void Update()
    {
        Debug.Log(Time.timeScale);
    }

}

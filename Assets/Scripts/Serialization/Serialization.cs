﻿using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[Serializable]
public struct GameData
{
    public PlayerData playerData;
    public List<AIData> AIData;
    public CycleData cycleData;
    public List<ItemData> itemsData;
    public int AIDayCount;
    public EconomyData economyData;
    public List<string> DestroyedGrids;
    public string gender;
    public string arcadeName;
    public List<RoomData> roomData;
    public List<bool> lightActive;
    public string saveTime;
}





public class Serialization : MonoBehaviour
{
    public float autoSaveFrequency = 0;
    public float saveIconDisplayTime = 0;
    private float autoSaveTime = 0;
    private float saveIconTime = 0;
    public GameObject saveIcon;
    [HideInInspector]
    public GameData gameData;
    public string itemAssetsLocation = "";
    private string folderName = "GameSaves";
    public string fileName = "";
    private string fileExtension = ".Arcade";
    private PlayerController_Default player;
    private DayNightCycle cycle;
    [HideInInspector]
    public AISpawning AISpawner;
    private Economy economy;
    private BuildingManager buildingManager;
    public TextMesh arcadeNameDisplay;
    public bool debug = false;
    public List<CleanRoom> rooms = new List<CleanRoom>();
    public List<GameObject> lights = new List<GameObject>();
    CurrentSave currentSave;
    //public List<>


    private void Start()
    {
        //fileName = Directory.()
        AISpawner = FindObjectOfType<AISpawning>();
        autoSaveTime = autoSaveFrequency;
        saveIconTime = saveIconDisplayTime;
        player = FindObjectOfType<PlayerController_Default>();
        cycle = FindObjectOfType<DayNightCycle>();
        economy = FindObjectOfType<Economy>();
        buildingManager = FindObjectOfType<BuildingManager>();
        currentSave = FindObjectOfType<CurrentSave>();
        currentSave.serialization = this;
        if (currentSave.newGame)
        {
            SaveGame();
            player.SetGender(currentSave.playersGender);
            arcadeNameDisplay.text = currentSave.arcadesName;
            currentSave.newGame = false;
        }
        else
        {
            LoadGame();
        }
       // LoadGame();
    }

    // Update is called once per frame
    void Update()
    {
        if (saveIcon != null)
        {
            if (autoSaveTime > 0)
            {
                autoSaveTime -= Time.unscaledDeltaTime % 60;
            }
            if (autoSaveTime <= 0)
            {
                if (saveIconTime > 0)
                {
                    if (!saveIcon.activeInHierarchy)
                    {
                        saveIcon.SetActive(true);
                        SaveGame();
                    }
                    saveIconTime -= Time.unscaledDeltaTime % 60;
                }
                else
                {
                    autoSaveTime = autoSaveFrequency;
                    saveIconTime = saveIconDisplayTime;
                    saveIcon.SetActive(false);
                }
            }
        }
        
    }

    public void LoadGame()
    {
        //Path.Combine(Application.persistentDataPath, folderName);
        string folderPath = currentSave.selectedSave;

        string dataPath = "";

        dataPath = Path.Combine(folderPath, "GameData" + fileExtension);


        //if (debug)
        //{
        //    dataPath = Path.Combine(folderPath, "Game" + fileExtension);
        //}
        //else
        //{
        //}


        ReadData(gameData, dataPath);

    }

    public void ReadData(GameData data, string path)
    {
        if (!File.Exists(path))
        {
            return;
        }

        using (StreamReader streamReader = File.OpenText(path))
        {
            string jsonString = streamReader.ReadToEnd();
            data = JsonUtility.FromJson<GameData>(jsonString);
        }
        if (arcadeNameDisplay != null)
        {
            arcadeNameDisplay.text = data.arcadeName;
        }

        if (player != null)
        {
            player.LoadData(data);
            cycle.LoadData(data);

            Item[] items = FindObjectsOfType<Item>();

            foreach (var item in items)
            {
                if (!item.ignoreSerialization)
                {
                    Destroy(item.gameObject);
                }
            }

            for (int i = 0; i < data.itemsData.Count; i++)
            {
                Item prefab = Resources.Load<Item>("PlaceableObjects/" + data.itemsData[i].itemName);
                Item loadedItem = Instantiate(prefab, data.itemsData[i].itemPosition, Quaternion.Euler(data.itemsData[i].itemRotation));
                loadedItem.iData = data.itemsData[i];
                loadedItem.LoadData(data);
            }

            var grids = Resources.FindObjectsOfTypeAll<GenPointProperties>();

            foreach (var destroyedGrid in data.DestroyedGrids)
            {
                foreach (var grid in grids)
                {
                    if (grid != null)
                    {
                        if (grid.gameObject.scene != null)
                        {
                            if (grid.name == destroyedGrid)
                            {
                                DestroyImmediate(grid.gameObject, false);
                                break;
                            }
                        }
                    }              
                }
            }

            for (int i = 0; i < data.roomData.Count; i++)
            {
                rooms[i].LoadData(data.roomData[i]);
            }

            int lightIter = 0;

            foreach (var activeLight in data.lightActive)
            {
                lights[lightIter].SetActive(activeLight);
                lightIter++;
            }


            if (AISpawner != null)
            {
                AISpawner.maxAIForDay = data.AIDayCount;

                if (AISpawner.AIPool.Count == 0 && AISpawner.AITypes.Count > 0)
                {
                    AISpawner.GeneratePool();
                }

                List<AIController> AIInScene = FindAllAI();

                

                for (int i = 0; i < data.AIData.Count; i++)
                {
                    AIStats loadedAI = null;
                  //  GameObject.Find(data.AIData[i].objID).tag != "Car"
                    if (FindAIByName(AIInScene,data.AIData[i].objID).tag != "Car")
                    {
                        for (int j = 0; j < AISpawner.AIPool.Count; j++)
                        {
                            if (AISpawner.AIPool[j].name.Contains(data.AIData[i].AIType) && !AISpawner.AIPool[j].gameObject.activeInHierarchy)
                            {
                                loadedAI = AISpawner.AIPool[j].GetComponent<AIStats>();
                                loadedAI.gameObject.SetActive(true);
                                loadedAI.aData = data.AIData[i];
                                loadedAI.LoadData(data);
                                AISpawner.activeAI.Add(loadedAI.controller);
                                AISpawner.activeAICounter++;
                                loadedAI.controller.navigationalMesh.enabled = true;
                                break;
                            }
                        }
                    }
                    else
                    {
                        loadedAI = GameObject.Find(data.AIData[i].objID).GetComponent<AIStats>();
                        loadedAI.aData = data.AIData[i];
                        loadedAI.LoadData(data); 
                    }

                }
            }           
            economy.LoadData(data);
        }
    }

    public void SaveGame()
    {
        // string folderPath = Path.Combine(Application.persistentDataPath, folderName);

        string folderPath = currentSave.selectedSave;

        if (!Directory.Exists(folderPath))
        {
            Directory.CreateDirectory(folderPath);
        }

        string dataPath = "";

        dataPath = Path.Combine(folderPath, "GameData" + fileExtension);
        ScreenCapture.CaptureScreenshot(Path.Combine(folderPath, "SaveScreenShot.png"));

        gameData.roomData = new List<RoomData>();
        foreach (var room in rooms)
        {
            room.SaveData(ref gameData);
        }

        gameData.lightActive = new List<bool>();

        foreach (var light in lights)
        {
            gameData.lightActive.Add(light.activeInHierarchy);
        }

        gameData.arcadeName = arcadeNameDisplay.text;

        if (player != null)
        {
            player.SaveData(ref gameData);

            cycle.SaveData(ref gameData);

            Item[] placedItems = FindObjectsOfType<Item>();
          
            gameData.itemsData = new List<ItemData>();
            foreach (var item in placedItems)
            {
                if (!item.ignoreSerialization)
                {
                    item.SaveData(gameData);
                }
            }
            gameData.DestroyedGrids = buildingManager.destoryedGrids;

            if (AISpawner != null)
            {
                gameData.AIDayCount = AISpawner.maxAIForDay;

                AIStats[] AI = FindObjectsOfType<AIStats>();

                gameData.AIData = new List<AIData>();

                foreach (var ai in AI)
                {
                    ai.SaveData(gameData);
                }
            }
        
            economy.SaveData(ref gameData);
        }

        int hour = DateTime.Now.Hour - 12;
        int minutes = DateTime.Now.Minute;
        string merdies = "AM";
        if (DateTime.Now.Hour >= 12)
        {
            merdies = "PM";
        }

        gameData.saveTime = hour + ":" + minutes + merdies;

        WriteData(gameData, dataPath);
    }


    public void WriteData(GameData data, string path)
    {
        string jsonString = JsonUtility.ToJson(data);


        using (StreamWriter streamWriter = File.CreateText(path))
        {
            streamWriter.Write(jsonString);
        }
    }


    public GameObject FindAIByName(List<AIController> AI,string name)
    {
        foreach (var agent in AI)
        {
            if (name == agent.gameObject.name)
            {
                return agent.gameObject;
            }
        }

        return null;
    }


    public List<AIController> FindAllAI()
    {
        List<AIController> AI = new List<AIController>(Resources.FindObjectsOfTypeAll<AIController>());

        for (int i = 0; i < AI.Count; i++)
        {
            if (!AI[i].gameObject.scene.IsValid())
            {
                AI.Remove(AI[i]);
                Debug.Log("Removed Asset");
            }
        }

        return AI;

    }



}

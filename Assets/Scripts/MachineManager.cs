﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

public class MachineManager : MonoBehaviour {

    public Item itemSelected = null;
    public Office office;
    public BuildingManager buildingManager;
    public MenuUI_Office menu;
    public ViewManager viewManager;

    public static List<Arcade> arcades = new List<Arcade>();
    public static List<Vendor> vendors = new List<Vendor>();

    private NotificationSystem notifications;
    public void Start() {
        notifications = GameObject.Find("Managers").GetComponent<NotificationSystem>();
    }

    public static int GetNumberOfMachines(string itemName) {
        int amount = 0;
        foreach(Arcade arcade in arcades) {
            if(arcade.iname == itemName)
                amount++;
        }
        foreach(Vendor vendor in vendors) {
            if(vendor.iname == itemName)
                amount++;
        }
        return amount;
    }

    public EditModeUI editMode_ui;

    public void EnterPurchaseMode(GameObject shopItem) {
        ShopItem shop = shopItem.GetComponent<ShopItem>();
        editMode_ui.PopUp();
        if(MachineManager.GetNumberOfMachines(shop.item.iname) < 3) {
            itemSelected = shopItem.GetComponent<ShopItem>().item;
            office.PopDownButton();
            menu.PopDown(true);
            viewManager.Switch(View.EDIT);
            if(buildingManager != null) {
                buildingManager.SetPurchaseMode(true);
                buildingManager.placement.SetSelectedObject(itemSelected);
            }
        } else notifications.Add(new Notification("Arcade Management", "We have the maximum amount of " + shop.item.iname + " machines!"));
    }

    public PopulateGrid content;
    public void ExitPurchaseMode() {
        itemSelected = null;
        office.PopUpButton();
        menu.PopUp(true);
        
        editMode_ui.PopDown();

        //update populate grid
        content.Populate(content.currentDisplayed);
    }

}

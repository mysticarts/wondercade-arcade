﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditorUtilities
{
    public static void SetFloat(UnityEngine.Object obj, string varName, float value)
    {
#if UNITY_EDITOR
            UnityEditor.SerializedObject so = new UnityEditor.SerializedObject(obj);
            UnityEditor.SerializedProperty prop = so.FindProperty(varName);
            if (prop != null)
                prop.floatValue = value;
            so.ApplyModifiedProperties();
#endif
    }

    public static void SetInt(UnityEngine.Object obj, string varName, int value)
    {
#if UNITY_EDITOR
            UnityEditor.SerializedObject so = new UnityEditor.SerializedObject(obj);
            UnityEditor.SerializedProperty prop = so.FindProperty(varName);
            if (prop != null)
                prop.intValue = value;
            so.ApplyModifiedProperties();
#endif
    }

    public static void SetBool(UnityEngine.Object obj, string varName, bool value)
    {
#if UNITY_EDITOR
            UnityEditor.SerializedObject so = new UnityEditor.SerializedObject(obj);
            UnityEditor.SerializedProperty prop = so.FindProperty(varName);
            if (prop != null)
                prop.boolValue = value;
            so.ApplyModifiedProperties();
#endif
    }

    public static void SetString(UnityEngine.Object obj, string varName, string value)
    {
#if UNITY_EDITOR
            UnityEditor.SerializedObject so = new UnityEditor.SerializedObject(obj);
            UnityEditor.SerializedProperty prop = so.FindProperty(varName);
            if (prop != null)
                prop.stringValue = value;
            so.ApplyModifiedProperties();
#endif
    }

    public static void SetObjectRef(UnityEngine.Object obj, string varName, UnityEngine.Object value)
    {
#if UNITY_EDITOR
            UnityEditor.SerializedObject so = new UnityEditor.SerializedObject(obj);
            UnityEditor.SerializedProperty prop = so.FindProperty(varName);
            if (prop != null)
                prop.objectReferenceValue = value;
            so.ApplyModifiedProperties();
#endif
    }
}
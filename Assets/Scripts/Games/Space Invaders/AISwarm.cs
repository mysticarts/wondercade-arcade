﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISwarm : MonoBehaviour
{

    [HideInInspector]
    public List<AIController> AI = new List<AIController>();
    [HideInInspector]
    public List<Invaders_Movement> AIMovement = new List<Invaders_Movement>();
    [HideInInspector]
    public bool AIKilled = false;
    public GameObject bullet;
    public float fireRate = 0;
    private float currentFireDelay = 0;
    private InvadersManager gameManager;

    //private Transform


    // Start is called before the first frame update
    void Start()
    {
        gameManager = FindObjectOfType<InvadersManager>();
    }

   // Update is called once per frame
    void Update()
    {

        if (AI.Count == 0)
        {
            EndGame();
        }

        for (int i = 0; i < AI.Count; i++)
        {
            if (AI[i] != null)
            {
                if (AIKilled)
                {
                    AIMovement[i].ResetSpeed(AIMovement[i].movementSpeed/2);
                }               
                
                if (AI[i].transform.position.y >= 3.4f)
                {
                    gameManager.EndGame();
                    break;
                }
            }
        }
        if (AIKilled)
        {
            AIKilled = false;
        }

        currentFireDelay += Time.deltaTime * 2;
        if (currentFireDelay >= fireRate)
        {
            Shoot();
        }

    }

    public void EndGame()
    {
        foreach (var ai in AI)
        {
            ai.gameObject.SetActive(false);
        }
        AI.Clear();
        AIMovement.Clear();
        enabled = false;
    }

    void Shoot()
    {
        int random = Random.Range(0, AIMovement.Count);

        AIController chosenAI = AI[random];

        RaycastHit hit;

        if (chosenAI != null)
        {
            if (Physics.Raycast(chosenAI.transform.position, chosenAI.transform.up, out hit, 100))
            {
                if (hit.transform.tag != "Enemy")
                {
                    Destroy(Instantiate(bullet, chosenAI.transform.position + chosenAI.transform.up, chosenAI.transform.rotation), 5);
                    currentFireDelay = 0;
                }
            }
        }

        
    }

    public void OnDeath(AIController ai)
    {
        int iter = AI.IndexOf(ai);

        AI.RemoveAt(iter);
        AIMovement.RemoveAt(iter);
        AIKilled = true;


    }




}

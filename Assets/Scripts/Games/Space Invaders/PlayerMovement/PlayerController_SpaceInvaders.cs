﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class PlayerController_SpaceInvaders : MonoBehaviour
{

    public Player input;
    public float speed = 0;
    public GameObject bullet;
    public Transform bulletSpawnPos;
    



    // Start is called before the first frame update
    void Start()
    {
        input = ReInput.players.GetPlayer(0);
      //  input.controllers.maps.SetMapsEnabled(true, "Invaders");
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;


        if (input.GetButton("MovementHorizontal"))
        {
            Ray ray = new Ray(transform.position + new Vector3(transform.localScale.x * 0.5f,0, 0), transform.right);
            if (!Physics.Raycast(ray, out hit, 0.1f))
            {
                Movement(transform.right);
            }
        }
        if (input.GetNegativeButton("MovementHorizontal"))
        {
            Ray ray = new Ray(transform.position - new Vector3(transform.localScale.x * 0.5f, 0, 0), -transform.right);
            if (!Physics.Raycast(ray, out hit, 0.1f))
            {
                Movement(-transform.right);
            }
        }
        if (input.GetButtonDown("Shoot"))
        {
            Shoot();
        }



    }


    void Shoot()
    {
        Destroy(Instantiate(bullet, bulletSpawnPos.position + transform.up * 0.2f, bulletSpawnPos.rotation),10);
    }

    void Movement(Vector3 direction)
    {
        transform.position += direction * speed;
    }


}

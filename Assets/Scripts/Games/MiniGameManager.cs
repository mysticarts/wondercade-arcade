﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Rewired;

public class MiniGameManager : MonoBehaviour
{

    protected Player input;
    //public string gameName = "";

    public void Start() {
        input = ReInput.players.GetPlayer(0);
        Intro();
    }

    public enum GameState 
    {
        SPLASHSCREEN = 0,
        MAINMENU = 1,
        GAME = 2,
        ENDGAME = 3,
    }

    public GameState gameState;

    virtual public void Intro() { }

    virtual public void ExitGame() {
        SceneManager.LoadScene("James B New");

    }

}

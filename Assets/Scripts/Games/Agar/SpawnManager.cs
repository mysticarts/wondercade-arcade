﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour {

    public PlayerController player;
    public GameObject foodPrefab;
    public GameObject enemyPrefab;

    public List<GameObject> foods = new List<GameObject>();

    public float minEnemyRadius = 2;
    public float maxEnemyRadius = 10;
    public int maxEnemies = 10;
    public int maxFood = 10;

    public float safetyRadius = 5;
    public float spawnRadius = 15;

    void Start() {
        //initial spawning
        for(int count = 0; count < maxEnemies; count++)
            SpawnEnemy();
        for(int count = 0; count < maxFood; count++)
            SpawnFood();
    }

    void Update() {
        if(Random.Range(0, 100) < 2) //2% chance of spawn food
            SpawnFood();
        if(Random.Range(0, 100) < 1) //1% chance of spawn food
            SpawnEnemy();
    }

    public void SpawnFood() {
        GameObject food = (GameObject) Instantiate(foodPrefab, GetRandomPosition(), Quaternion.identity);
        food.transform.Rotate(new Vector3(90, 0, 0), Space.World);
        foods.Add(food);
    }

    public void SpawnEnemy() {
        float randomRadius = Random.Range(minEnemyRadius, maxEnemyRadius);
        GameObject enemy = (GameObject) Instantiate(enemyPrefab, GetRandomPosition(), Quaternion.identity);
        
        enemy.GetComponent<Enemy>().radius = randomRadius;

        enemy.transform.localScale = new Vector3(randomRadius, 0.1f, randomRadius);
        enemy.transform.Rotate(new Vector3(90, 0, 0), Space.World);
    }

    public Vector3 GetRandomPosition() {
        float randomPosX;
        float randomPosY;

        if(Random.Range(0, 2) == 0)
            randomPosX = Random.Range(safetyRadius, spawnRadius);
        else randomPosX = Random.Range(-safetyRadius, -spawnRadius);

        if(Random.Range(0, 2) == 0)
            randomPosY = Random.Range(safetyRadius, spawnRadius);
        else randomPosY = Random.Range(-safetyRadius, -spawnRadius);

        return new Vector3(player.transform.position.x + randomPosX, player.transform.position.y + randomPosY, 0);
    }

}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public SpawnManager manager;
    public PlayerController_Agar player;

    public float speed = 1;
    public float radius = 1;

    public void Update() {

        if(Vector3.Distance(player.transform.position, this.gameObject.transform.position) < (player.fleeAttackRadius+(radius/2))) {
            if(player.radius <= radius) 
                Attack();
            else Flee();
        } else Wander();

    }

    public GameObject GetNearestFood() {
        GameObject nearestFood = null;

        float nearestFoodDistance = float.MaxValue;
        foreach(GameObject food in manager.foods) {
            float distance = Vector3.Distance(food.transform.position, this.gameObject.transform.position);
            if(distance < nearestFoodDistance) {
                nearestFoodDistance = distance;
                nearestFood = food;
            }
        }

        return nearestFood;
    }

    public void Attack() {
        speed = 2;
        transform.position = Vector3.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);
    }

    public void Flee() {
        speed = 2;
        transform.position = Vector3.MoveTowards(transform.position, player.transform.position, -speed * Time.deltaTime);
    }

    public void Wander() {
        speed = 1;
        GameObject nearestFood = GetNearestFood();
        if(nearestFood != null) {
            transform.position = Vector3.MoveTowards(transform.position, nearestFood.transform.position, speed * Time.deltaTime);
            float distance = Vector3.Distance(transform.position, nearestFood.transform.position);
            if(distance < ((radius/2)-0.3)) {
                //update radius
                radius = radius + 0.1f;
                transform.localScale = new Vector3(radius, 0.1f, radius);

                manager.foods.Remove(nearestFood);
                Destroy(nearestFood);
            }
        }
    }
}

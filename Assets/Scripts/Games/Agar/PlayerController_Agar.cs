﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class PlayerController_Agar : PlayerController {

    public float fleeAttackRadius = 5;
    public float radius = 0.5f;
    public float speed = 5;

    override public void Initialise() {
        input.controllers.maps.SetMapsEnabled(true, "Default");
    }

    public void Update() {
        Vector3 position = this.gameObject.transform.position;

        if(input.GetButton("Vertical")) {
            transform.position = Vector3.MoveTowards(transform.position, transform.position + Vector3.up, speed * Time.deltaTime);
            Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, Camera.main.transform.position + Vector3.up, speed * Time.deltaTime);
        } if(input.GetNegativeButton("Vertical")) {
            transform.position = Vector3.MoveTowards(transform.position, transform.position + Vector3.down, speed * Time.deltaTime);
            Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, Camera.main.transform.position + Vector3.down, speed * Time.deltaTime);
        } if(input.GetNegativeButton("Horizontal")) {
            transform.position = Vector3.MoveTowards(transform.position, transform.position + Vector3.left, speed * Time.deltaTime);
            Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, Camera.main.transform.position + Vector3.left, speed * Time.deltaTime);
        } if(input.GetButton("Horizontal")) {
            transform.position = Vector3.MoveTowards(transform.position, transform.position + Vector3.right, speed * Time.deltaTime);
            Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, Camera.main.transform.position + Vector3.right, speed * Time.deltaTime);
        }

    }

}

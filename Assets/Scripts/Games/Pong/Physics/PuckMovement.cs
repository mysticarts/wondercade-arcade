﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using Rewired.ControllerExtensions;

public class PuckMovement : MonoBehaviour
{

    public Vector3 velocity;
    public float horizontalSpeed;
    public float verticalSpeed;
    private float spawnSpeed = 0;
    public Rigidbody puckRigidBody;
    private Vector3 previousPos;
    private Vector3 currentPos;
    private Vector3 spawnPos;
    public GameObject middleLine;
    private PlayController_Pong player;
    public float direction = 0;
    public bool respawnDelay = false;
    public float respawnTime = 0;

    // Start is called before the first frame update
    void Start()
    {
        spawnPos = transform.position;
        Debug.Log(Vector3.Distance(spawnPos, GameObject.Find("TopEdge").transform.position));
        puckRigidBody = GetComponent<Rigidbody>();
        puckRigidBody.velocity = new Vector3(-horizontalSpeed, -verticalSpeed, 0);
        if (direction > 0)
        {
            player = GameObject.Find("Player 1").GetComponent<PlayController_Pong>();
        }
        else;
        {
            player = GameObject.Find("Player 2").GetComponent<PlayController_Pong>();
        }
    }

    // Update is called once per frame
    void Update()
    {        
        puckRigidBody.velocity = new Vector3(horizontalSpeed, verticalSpeed, 0);

        direction = Vector3.Dot(puckRigidBody.velocity, middleLine.transform.right);

        if (respawnDelay)
        {
            respawnTime += Time.deltaTime * 2;

            if (respawnTime > 2)
            {
                Respawn();
            }
        }
    }
    

   
    private void Respawn()
    {
        
        while (verticalSpeed == 0 && horizontalSpeed == 0)
        {
            int[] directions = new int[2] { -1, 1 };
            int direction = Random.Range(directions[0], directions[1]);
            verticalSpeed = spawnSpeed * direction;
            horizontalSpeed = spawnSpeed * direction;
            respawnTime = 0;
            respawnDelay = false;
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name != "Middle Line")
        {
            player.score++;
            float randOffSet = Random.Range(-3.5f, 3.5f);
            transform.position = spawnPos + new Vector3(0, randOffSet, 0);
            respawnDelay = true;
            spawnSpeed = verticalSpeed;
            verticalSpeed = 0;
            horizontalSpeed = 0;
        }
       
    }


    private void OnCollisionEnter(Collision collision)
    {     
        if (collision.gameObject.tag == "Player")
        {
            horizontalSpeed *= -1;
            player = collision.gameObject.GetComponent<PlayController_Pong>();
#if UNITY_EDITOR
            foreach (Joystick joystick in player.input.controllers.Joysticks)
            {

                var ps4ControllerExtension = joystick.GetExtension<DualShock4Extension>();
                ps4ControllerExtension.SetVibration(DualShock4MotorType.StrongMotor, 1, 0.25f, false);


            }
#endif

        }
        else
        {
            verticalSpeed *= -1;
        }
    }

}

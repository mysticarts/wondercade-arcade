﻿Shader "Hidden/CRT"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
	}
		SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}

			sampler2D _MainTex;

			uniform float u_time;
			uniform float u_bend;
			uniform float u_scanline_size_1;
			uniform float u_scanline_speed_1;
			uniform float u_scanline_size_2;
			uniform float u_scanline_speed_2;
			uniform float u_scanline_amount;
			uniform float u_vignette_size;
			uniform float u_vignette_smoothness;
			uniform float u_vignette_edge_round;
			uniform float u_noise_size;
			uniform float u_noise_amount;
			uniform half2 u_red_offset;
			uniform half2 u_green_offset;
			uniform half2 u_blue_offset;

			//bending the image
			half2 crt_coords(half2 uv, float bend)
			{
				uv -= 0.5;//taking the uv coords from the 0 to 1 space to the -1 to 1 space for easy manipulation
				uv *= 2.;
				uv.x *= 1. + pow(abs(uv.y) / bend, 2.); 
				uv.y *= 1. + pow(abs(uv.x) / bend, 2.); //bends the coordinates/changes the curvature based off the bend scalar we pass in

				//return the value back to the 0 to 1 space.
				uv /= 2.5;
				return uv + 0.5;
			}

			//for creating the black rim around the screen.
			float vignette(half2 uv, float size, float smoothness, float edgeRounding)
			{
				//take the uv coords from the -0.5f to 0.5f space
				uv -= .5;
				uv *= size; //how big we want the rim to be.
				float amount = sqrt(pow(abs(uv.x), edgeRounding) + pow(abs(uv.y), edgeRounding)); //distance formula (Sqrt((x2^2 - x1^2) + (y2^2 - y1^2)));
				amount = 1. - amount; //inverse it so it isnt in the center of the screen.
				return smoothstep(0, smoothness, amount); //smoothly interpolate for how much blackness we want on the rim
			}

			float scanline(half2 uv, float lines, float speed)
			{
				return sin(uv.y * lines + u_time * speed); //use sin function to move uv.y coordinates to create that CRT scan line effect. Increase the density and frequency of scan lines based of lines and speed values.
			}

			float random(half2 uv)
			{
				return frac(sin(dot(uv, half2(15.1511, 42.5225))) * 12341.51611 * sin(u_time * 0.03));//randomly select a value
			}

			//the noise function from the Book Of Shaders
			float noise(half2 uv)
			{
				half2 i = floor(uv);
				half2 f = frac(uv);

				float a = random(i);
				float b = random(i + half2(1., 0.));
				float c = random(i + half2(0, 1.));
				float d = random(i + half2(1., 1.));

				half2 u = smoothstep(0., 1., f);

				return lerp(a, b, u.x) + (c - a) * u.y * (1. - u.x) + (d - b) * u.x * u.y;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				half2 crt_uv = crt_coords(i.uv, u_bend); //get the bent uv coords
				fixed4 col;
				//set the crt rgba values to that of bent crt_uv coords
				col.r = tex2D(_MainTex, crt_uv + u_red_offset).r;
				col.g = tex2D(_MainTex, crt_uv + u_green_offset).g;
				col.b = tex2D(_MainTex, crt_uv + u_blue_offset).b;
				col.a = tex2D(_MainTex, crt_uv).a;

				float s1 = scanline(i.uv, u_scanline_size_1, u_scanline_speed_1); //get first set of scanlines based of users size and speed input
				float s2 = scanline(i.uv, u_scanline_size_2, u_scanline_speed_2);//get secpmd set of scanlines based of users size and speed input

				col = lerp(col, fixed(s1 + s2), u_scanline_amount); //smoothly interpolate between current color and scanlines

				return lerp(col, fixed(noise(i.uv * u_noise_size)), u_noise_amount) * vignette(i.uv, u_vignette_size, u_vignette_smoothness, u_vignette_edge_round); //smoothly interpolate and add the noise and black edge to the texture. CRT effect.
			}
			ENDCG
		}
	}
}
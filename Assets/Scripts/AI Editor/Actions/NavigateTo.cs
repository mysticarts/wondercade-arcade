﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tree.Editor;

public class NavigateTo : Action
{
    public List<Transform> points = new List<Transform>();
    public int iter = 0;
    public bool ResetPath = false;
    private AIStats stats;

    public override void Start()
    {
        stats = agentObj.GetComponent<AIStats>();
    }
    public override void OnEnter()
    {

    }
    public override BehaviourResult execute()//execute behaviour
    {
       
        if (points.Count > 0)
        {
            
            if (iter < points.Count)
            {
                if (navigation.path.status != UnityEngine.AI.NavMeshPathStatus.PathInvalid) {
                navigation.SetDestination(points[iter].position);
                }

                if (Vector3.Distance(agentObj.transform.position, points[iter].position) < 1)
                {
                    tree.SetBool(false, "Navigate");

                    if (stats.wallet == 0)
                    {
                        tree.SetBool(false, "Entered Arcade");
                        tree.SetBool(false, "Navigate Arcade");
                        return result = BehaviourResult.SUCCESS;
                    }


                    if (stats.cycle.isDay && tree.Test("Moral"))
                    {
                        stats.cycle.customers.Add(stats);
                        tree.SetBool(true, "Entered Arcade");
                    }
                    else
                    {
                        tree.SetBool(false, "Entered Arcade");
                        tree.SetBool(false, "Navigate Arcade");
                        stats.leftStoreUnsatisfied = true;
                    }
                    iter++;
                    if (iter >= points.Count && ResetPath)
                    {
                        iter = 0;
                    }

                    return result = BehaviourResult.SUCCESS;
                }
                if (Vector3.Distance(agentObj.transform.position, points[iter].position) > 1)
                {
                    return result = BehaviourResult.RUNNING;
                }
            }
         
        }


        return result = BehaviourResult.FAILURE;
    }
    public override float CalculateWeight()//calc weight score
    {
        return 0;
    }

    public override void SaveData(ref AIData data)
    {
        bData.fieldData.Add(nameof(iter) + iter.ToString());
        base.SaveData(ref data);

    }

    public override void LoadData(AIData data)
    {
        AssignLoadedFieldData(nameof(iter), out iter);
        base.LoadData(data);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tree.Editor;


public class Flocking : Action
{
    public float detectionRadius = 0;
    public LayerMask detectionMask;


    public override void Start()
    {
        
    }

    public override void OnEnter()
    {
        
    }

    public override BehaviourResult execute()
    {
        Collider[] detection = Physics.OverlapSphere(agentObj.transform.position + agentObj.transform.forward, detectionRadius);


        if (detection.Length > 0)
        {
            foreach (var ai in detection)
            {
                if (ai.gameObject != agentObj && ai.name != "Model" && ai.tag == "AI")
                {
                    Avoid(ai.transform);
                    //navigation.velocity += agentObj.transform.right * 1;
                    //Debug.Log("AI Detected");
                }
            }
            return result = BehaviourResult.SUCCESS;
        }

        




        return result = BehaviourResult.FAILURE;
    }

    void Avoid(Transform target)
    {
        Vector3 forward = agentObj.transform.right;
        Vector3 toOther = target.transform.position - agentObj.transform.position;



        float avoidanceVelocity = Vector3.Dot(forward,toOther);
       // Debug.Log(avoidanceVelocity);
        //navigation.velocity += avoidanceVelocity;
        if (avoidanceVelocity < 0)
        {
            Debug.Log(target.name + " right");
        }
        if (avoidanceVelocity > 0)
        {
            Debug.Log(target.name + " left");
        }
    }



    public override float CalculateWeight()
    {
        return weightValue;
    }



   
}

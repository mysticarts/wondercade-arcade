﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tree.Editor;

public class Pong_Movement : Action
{
    private PlayController_Pong controller;
    private PuckMovement puckMovement;

    public override void Start()
    {
    }

    public override void OnEnter()
    {
    }
    public override BehaviourResult execute()//execute behaviour
    {
        if (controller == null)
        {
            controller = agentObj.GetComponent<PlayController_Pong>();
            puckMovement = FindObjectOfType<PuckMovement>();
        }
        
        if (puckMovement.direction > 0)
        {
            if (!Physics.Raycast(agentObj.transform.position + new Vector3(0, agentObj.transform.localScale.y * 0.5f, 0), agentObj.transform.up, 0.1f))
            {
                if (puckMovement.verticalSpeed > 0)
                {
                    controller.Movement(controller.movementSpeed, 1);
                    return result = BehaviourResult.SUCCESS;
                }
            }
            if (!Physics.Raycast(agentObj.transform.position - new Vector3(0, agentObj.transform.localScale.y * 0.5f, 0), -agentObj.transform.up, 0.1f))
            {
                if (puckMovement.verticalSpeed < 0)
                {
                    controller.Movement(controller.movementSpeed, -1);
                    return result = BehaviourResult.SUCCESS;
                }
            }
        }

        
        
        return result = BehaviourResult.SUCCESS;
    }


    public override float CalculateWeight()
    {
        return weightValue;
    }

    public override void SaveData(ref AIData data)
    {
        base.SaveData(ref data);

    }

    public override void LoadData(AIData data)
    {
        base.LoadData(data);

    }
}

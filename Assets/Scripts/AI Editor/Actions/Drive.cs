﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tree.Editor;

public class Drive : Action
{
    public float acceleration = 0;
    public float deacceleration = 0;
    private float speed = 0;
    public float maxSpeed = 0;

    public override void Start()
    {
    }

    public override void OnEnter()
    {
    }

    public override BehaviourResult execute()
    {
        RaycastHit hit;
        if (Physics.BoxCast(agentObj.transform.position, new Vector3(1, 1, 3), -agentObj.transform.forward, out hit, agentObj.transform.rotation, 4))
        {
            if (hit.transform.tag == "CarTrigger")
            {
                agentObj.transform.rotation *= Quaternion.Euler(0, 180, 0);
            }
            if (hit.transform.tag == "AI")
            {
                speed -= deacceleration * Time.deltaTime;
            }
        }
        else
        {
            speed += acceleration * Time.deltaTime;
        }

        speed = Mathf.Clamp(speed, 0, maxSpeed);

        agentObj.transform.position -= agentObj.transform.forward * speed * Time.deltaTime;

        return result = BehaviourResult.SUCCESS;
    }

    public override float CalculateWeight()
    {
        return 0;
    }

    public override void SaveData(ref AIData data)
    {
        bData.fieldData.Add(nameof(speed) + speed.ToString());
        base.SaveData(ref data);
    }

    public override void LoadData(AIData data)
    {
        AssignLoadedFieldData(nameof(speed), out speed);
        base.LoadData(data);

    }
}

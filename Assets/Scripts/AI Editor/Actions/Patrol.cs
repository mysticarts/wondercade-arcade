﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tree.Editor;

public class Patrol : Action
{
    private List<Transform> points;
    private int iter;
    private AISpawning AISpawner;
    private int patrolDuration;

    public override void Start()
    {
        points = new List<Transform>(FindPatrolPoints());
        AISpawner = FindObjectOfType<AISpawning>();
        if (points != null && !gameSaveLoaded)
        {
            iter = Random.Range(0, points.Count);
            patrolDuration = Random.Range(1, points.Count);
        }

    }

    public override void OnEnter()
    {
        //navigation.SetDestination(points[iter].position);
    }


    public override BehaviourResult execute()//execute behaviour
    {        
        if (points.Count > 0)
        {         

            if (navigation.path.status != UnityEngine.AI.NavMeshPathStatus.PathInvalid)
            {
                navigation.SetDestination(points[iter].position);
            }


            if (Vector3.Distance(points[iter].position,agentObj.transform.position) < 1)
            {
                if (patrolDuration > 0)
                {
                    iter = Random.Range(0, points.Count);
                    patrolDuration--;
                }
                else
                {
                    AISpawner.Despawn(controller);
                }



                return result = BehaviourResult.SUCCESS;
            }
            else
            {
                return result = BehaviourResult.RUNNING;
            }
          
           

        }
        return result = BehaviourResult.FAILURE;
    }

    public Transform[] FindPatrolPoints()
    {
        List<GameObject> pointsObjs = new List<GameObject>(GameObject.FindGameObjectsWithTag("NavPoint"));
        List<Transform> points = new List<Transform>();
        for (int i = 0; i < pointsObjs.Count; i++)
        {
            points.Add(pointsObjs[i].transform);
        }



        return points.ToArray();
    }
    public override float CalculateWeight()//calc weight score
    {
        return weightValue;
    }
    
    public override void SaveData(ref AIData data)
    {
        //Debug.Log(pData.ToString());
        bData.fieldData.Add(nameof(iter) + iter.ToString());
        bData.fieldData.Add(nameof(patrolDuration) + patrolDuration.ToString());

        base.SaveData(ref data);
    }

    public override void LoadData(AIData data)
    {
        AssignLoadedFieldData(nameof(iter), out iter);
        AssignLoadedFieldData(nameof(patrolDuration), out patrolDuration);

        base.LoadData(data);
    }
}

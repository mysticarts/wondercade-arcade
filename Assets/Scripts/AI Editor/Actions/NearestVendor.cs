﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tree.Editor;
using Pixelplacement;

public class NearestVendor : Action
{
    public Transform chosenVendorPos;
    public Vendor chosenVendor;
    public string chosenVendorID;
    private Vendor vendorToIgnore;
    public AIStats stats;
    private AIAnimationHandler anim;
    public VendorType desiredVendor;
    private bool lookAt = false;

    public override void Start()
    {
        stats = agentObj.GetComponent<AIStats>();
        anim = agentObj.GetComponentInChildren<AIAnimationHandler>();
    }
    public override void OnEnter()
    {

    }
    public bool FindNearestVendor(Transform origin, out Transform target)
    {
        Vendor[] vendors = FindObjectsOfType<Vendor>();

        float minDist = Mathf.Infinity;
        target = null;
        foreach (var vendor in vendors)
        {
            if (vendor.itemType == ItemType.VENDOR && desiredVendor == vendor.vendorType && vendor.stock > 0 && !vendor.occupied)
            {
                if (!CostToHigh(vendor) && stats.wallet >= vendor.ussageCost)
                {
                    float dist = Vector3.Distance(origin.position, vendor.transform.position);
                    if (dist < minDist)
                    {
                        target = vendor.AIStandingPos;
                        chosenVendor = vendor;
                        minDist = dist;
                    }
                }
               
            }
        }

        if (target != null)
        {
            tree.SetBool(true, "Vending");
            //tree.SetBool(true, "Vendor Found");
            return true;
        }

        return false;
    }

    public override BehaviourResult execute()//execute behaviour
    {      
        if (chosenVendorPos == null)
        {
            if (FindNearestVendor(agentObj.transform, out chosenVendorPos))
            {
                navigation.SetDestination(chosenVendorPos.position);
                return result = BehaviourResult.RUNNING;
            }
        }

        if (chosenVendorPos != null)
        {
            chosenVendorID = chosenVendor.InstanceID;
            navigation.SetDestination(chosenVendorPos.position);
            tree.SetBool(true, "Chosen Action");

            if (chosenVendor.occupant.Length > 0 && chosenVendor.occupant != stats.ID)
            {
                chosenVendorPos = null;
                return BehaviourResult.RUNNING;
            }


            if (Vector3.Distance(agentObj.transform.position, chosenVendorPos.position) < 2)
            {
                agentObj.transform.LookAt(new Vector3(chosenVendor.transform.position.x, agentObj.transform.position.y, chosenVendor.transform.position.z));
                chosenVendor.occupant = stats.ID;
                if (!lookAt)
                {
                    stats.economySystem.Give(chosenVendor.ussageCost);
                    //Tween.LookAt(agentObj.transform, chosenVendor.transform, agentObj.transform.up, 0.5f, 0);
                    lookAt = true;
                }
                anim.animator.SetBool("Wandering", false);

                if (desiredVendor == VendorType.DRINK)
                {
                    anim.animator.SetBool("Using Soda Machine Round", true);
                }
                if (desiredVendor == VendorType.FOOD)
                {
                    anim.animator.SetBool("Using Soda Machine Square", true);
                }

                chosenVendor.occupied = true;

                if (anim.replenish)
                {
                    vendorToIgnore = null;
                    chosenVendorPos = null;
                    tree.SetBool(false, "Vending");
                    if (desiredVendor == VendorType.DRINK)
                    {
                        stats.cycle.currentDayStats.drinkSales += chosenVendor.ussageCost;
                        stats.thirst = 0;
                        anim.animator.SetBool("Using Soda Machine Round", false);
                    }
                    if (desiredVendor == VendorType.FOOD)
                    {
                        stats.cycle.currentDayStats.foodSales += chosenVendor.ussageCost;
                        anim.animator.SetBool("Using Soda Machine Square", false);
                        stats.hunger = 0;
                    }
                    stats.wallet -= chosenVendor.ussageCost;
                    chosenVendor.TakeStock();
                    anim.animator.SetBool("Wandering", true);
                    tree.SetBool(true, "Wandering");
                    anim.replenish = false;
                    lookAt = false;
                    chosenVendor.occupied = false;
                    chosenVendor.occupant = "";
                    chosenVendor = null;
                    return result = BehaviourResult.SUCCESS;
                }
            }
            return result = BehaviourResult.RUNNING;
        }

        return result = BehaviourResult.FAILURE;
    }

   


    public override float CalculateWeight()//calc weight score
    {    
        if (desiredVendor == VendorType.DRINK)
        {
            weightValue = stats.thirst;
        }
        if (desiredVendor == VendorType.FOOD)
        {
            weightValue = stats.hunger;
        }

        return weightValue;
    }

    public bool CostToHigh(Vendor vendor)
    {
        if (vendor.ussageCost == 0)
        {
            return false;
        }

        if (vendorToIgnore != null)
        {
            if (vendor == vendorToIgnore)
            {
                Debug.Log("Already looked at this, Price is to high");
                return true;
            }
        }
        float sum = stats.wallet;
        float prob = (sum / vendor.ussageCost) * 2;

        float dice = Random.Range(0, sum);

        if (prob > dice)
        {
            Debug.Log(vendor.name + " is a good value" + " , " + "prob = " + prob + " , " + "dice = " + dice);
            return false;
        }

        Debug.Log(vendor.name + " costs too much" + " , " + "prob = " + prob + " , " + "dice = " + dice);

        vendorToIgnore = vendor;
        return true;
    }
    public override void SaveData(ref AIData data)
    {
        bData.fieldData.Add(nameof(desiredVendor) + (int)desiredVendor);

        if (chosenVendorPos != null)
        {
            bData.fieldData.Add(nameof(chosenVendorID) + chosenVendorID);

            bData.fieldData.Add(nameof(chosenVendorPos) + "x" + chosenVendorPos.position.x.ToString());
            bData.fieldData.Add(nameof(chosenVendorPos) + "y" + chosenVendorPos.position.y.ToString());
            bData.fieldData.Add(nameof(chosenVendorPos) + "z" + chosenVendorPos.position.z.ToString());
        }

        base.SaveData(ref data);
    }

    public override void LoadData(AIData data)
    {
        int venType;
        AssignLoadedFieldData(nameof(desiredVendor), out venType);
        desiredVendor = (VendorType)venType;

        Vector3 vendorPos;

        AssignLoadedFieldData(nameof(chosenVendorPos) + "x", out vendorPos.x);
        AssignLoadedFieldData(nameof(chosenVendorPos) + "y", out vendorPos.y);
        AssignLoadedFieldData(nameof(chosenVendorPos) + "z", out vendorPos.z);

        AssignLoadedFieldData(nameof(chosenVendorID), out chosenVendorID);

        Vendor[] vendors = FindObjectsOfType<Vendor>();

        foreach (var vendor in vendors)
        {
            if (vendor.InstanceID == chosenVendorID)
            {
                chosenVendor = vendor;
                chosenVendorPos = vendor.AIStandingPos;
                vendor.occupied = true;  
            }
        }


        base.LoadData(data);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tree.Editor;

public class Invaders_Movement : Action
{
    public enum MovementDirection
    {
        RIGHT = 1,
        LEFT = -1,
    }

    [HideInInspector]
    public MovementDirection direction = MovementDirection.RIGHT;

    public float movementSpeedIncreaseRate = 0;
    public float movementSpeed = 0;
    private float changingDirectionDelay = 0;
    private float currentDelayTimer = 0;
    private float currentMovementSpeed = 0;
    private AISwarm swarm;
    private bool changingDirection = false;

    public override void Start()
    {
        swarm = FindObjectOfType<AISwarm>();
        swarm.AI.Add(controller);
        swarm.AIMovement.Add(this);
    }

    public override void OnEnter()
    {
       
    }

    public override BehaviourResult execute()
    {

        if (changingDirection)
        {
            currentDelayTimer += Time.deltaTime * 2;
            if (currentDelayTimer >= changingDirectionDelay)
            {
                currentMovementSpeed = 0;
                agentObj.transform.position += agentObj.transform.up * 1;
                currentDelayTimer = 0;
                changingDirection = false;
            }
            return result = BehaviourResult.SUCCESS;
        }


        RaycastHit hit;

        if (direction == MovementDirection.RIGHT)
        {
            if (Physics.Raycast(agentObj.transform.position, agentObj.transform.right, out hit, 1))
            {
                if (hit.transform.tag == "Right")
                {
                    foreach (var ai in swarm.AIMovement)
                    {
                        ai.ChangeDirection(MovementDirection.LEFT);
                    }
                    return result = BehaviourResult.SUCCESS;
                }
            }
        }
        if (direction == MovementDirection.LEFT)
        {
            if (Physics.Raycast(agentObj.transform.position, -agentObj.transform.right, out hit, 1))
            {
                if (hit.transform.tag == "Left")
                {
                    foreach (var ai in swarm.AIMovement)
                    {
                        ai.ChangeDirection(MovementDirection.RIGHT);
                    }
                    return result = BehaviourResult.SUCCESS;
                }
            }
        }

        currentMovementSpeed += Time.deltaTime * 2;
        if (currentMovementSpeed >= movementSpeed && !changingDirection)
        {
            currentMovementSpeed = 0;
            agentObj.transform.position += agentObj.transform.right * (int)direction;
        }

        return result = BehaviourResult.SUCCESS;
    }

    public void ChangeDirection(MovementDirection dir)
    {
        if (agentObj != null)
        {
            changingDirection = true;
            movementSpeed -= movementSpeedIncreaseRate;
            changingDirectionDelay = movementSpeed;
            currentMovementSpeed = 0;
            direction = dir;
        }

    }
  
    public void ResetSpeed(float speed)
    {
        currentMovementSpeed = speed;
    }


    public override float CalculateWeight()
    {
        return 0;
    }
}

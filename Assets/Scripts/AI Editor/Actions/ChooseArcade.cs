﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Tree.Editor;
using Pixelplacement;

public class ChooseArcade : Action
{
    private AIStats stats;
    private AIAnimationHandler anim;
    public Transform chosenArcadePos;
    private Arcade arcadeToIgnore;
    [HideInInspector]
    public Vector3 targetPos;
    public Arcade chosenArcade;
    private string chosenArcadesID;
    private GameName previouslyDesired = GameName.None;
    public GameName desiredArcade;
    public float playTime = 0;
    private float currentPlayTime = 0;
    private bool playing = false;
    private Image progressDisplay;
 
    public override void Start()
    {
        stats = agentObj.GetComponent<AIStats>();
        progressDisplay = agentObj.GetComponentInChildren<Image>();
        anim = agentObj.GetComponentInChildren<AIAnimationHandler>();
        if (progressDisplay != null)
        {
            if (gameSaveLoaded)
            {
                progressDisplay.gameObject.SetActive(true);
            }
            else
            {
                progressDisplay.gameObject.SetActive(false);
            }
            progressDisplay.fillAmount = 0;
        }

    }
    public override void OnEnter()
    {
     
    }

    public override BehaviourResult execute()
    {
        if (chosenArcadePos == null)
        {
            ChooseGame(agentObj.transform, out chosenArcadePos);
            navigation.SetDestination(targetPos);
        }
        if (chosenArcadePos != null)
        {
            tree.SetBool(true, "Chosen Action");
            chosenArcadesID = chosenArcade.InstanceID;
            //targetPos.y = 0;

            if (navigation.pathStatus == UnityEngine.AI.NavMeshPathStatus.PathInvalid)
            {
                Debug.Log("Greatt");
            }

            if (navigation.enabled)
            {
                navigation.SetDestination(targetPos);
            }

            if (chosenArcade.customer != null)
            {
                if (chosenArcade.customer != controller)
                {
                    Debug.Log("Why");
                    if (!chosenArcade.queue.Contains(controller))
                    {
                        if (GoToQueue() && chosenArcade.queue.Count < chosenArcade.queueSize)
                        {
                            Debug.Log("Imma Line up");
                            Queue();
                            return BehaviourResult.RUNNING;
                        }
                        else
                        {
                            Debug.Log("Nah ill find another");
                            chosenArcade = null;
                            chosenArcadePos = null;
                            return BehaviourResult.RUNNING;
                        }
                    }                   
                }               
            }
            if (chosenArcade.customer == null)
            {
                if (chosenArcade.queue.Count == 0)
                {
                    if (targetPos != chosenArcadePos.position)
                    {
                        targetPos = chosenArcadePos.position;
                        navigation.SetDestination(targetPos);
                    }
                }            
            }

            if (playing)
            {
                Debug.Log("PlayTime");
                progressDisplay.fillAmount = currentPlayTime / playTime;
                if (currentPlayTime < playTime)
                {
                    if (chosenArcade.cabinetType == ArcadeType.CLASSIC && stats.aiType == AIType.KID)
                    {
                        agentObj.transform.position = chosenArcade.stoolStandingPos.position;
                    }

                    agentObj.transform.LookAt(new Vector3(chosenArcade.transform.position.x, agentObj.transform.position.y, chosenArcade.transform.position.z), agentObj.transform.up);
                    currentPlayTime += Time.deltaTime;
                    return result = BehaviourResult.RUNNING;
                }
                else
                {
                    if (chosenArcade.cabinetType == ArcadeType.CLASSIC)
                    {
                        if (stats.aiType == AIType.KID)
                        {
                            ActivateStandingStool(chosenArcade, false);
                            navigation.enabled = true;
                        }
                        anim.animator.SetBool("Playing Classic Arcade", false);
                    }
                    if (chosenArcade.cabinetType == ArcadeType.COCKTAIL)
                    {
                        anim.animator.SetBool("Playing Cocktail Arcade", false);
                    }
                    anim.animator.SetBool("Wandering", true);
                    arcadeToIgnore = null;
                    chosenArcade.Deteriorate();
                    progressDisplay.gameObject.SetActive(false);
                   // chosenArcade.occupied = false;
                    currentPlayTime = 0;
                    playing = false;
                    chosenArcadePos = null;
                    stats.wanderTimer = 0;
                    stats.moral += stats.ReconfigureMoral(stats.moralGainRange);
                    tree.SetBool(true, "Wandering");
                    tree.SetBool(false, "Find Arcade");
                    stats.boredom = 0;
                    chosenArcade.customer = controller;
                    chosenArcade = null;
                    previouslyDesired = GameName.None;
                    return result = BehaviourResult.SUCCESS;
                }
            }

            if (Vector3.Distance(agentObj.transform.position, chosenArcadePos.position) < 2)
            {
                if (stats.aiType == AIType.KID)
                {
                    if (chosenArcade.standingStool != null && chosenArcade.cabinetType == ArcadeType.CLASSIC && chosenArcade.customer == null)
                    {
                        ActivateStandingStool(chosenArcade, true);
                    }
                }
            }

            if (Vector3.Distance(agentObj.transform.position, chosenArcade.transform.position) < 1.5f)
            {
                if (!playing)
                {
                    if (anim != null)
                    {
                        anim.animator.SetBool("Wandering", false);
                        if (chosenArcade.cabinetType == ArcadeType.CLASSIC)
                        {
                            if (stats.aiType == AIType.KID)
                            {
                                navigation.enabled = false;

                            }
                            anim.animator.SetBool("Playing Classic Arcade", true);
                        }
                        if (chosenArcade.cabinetType == ArcadeType.COCKTAIL)
                        {
                            anim.animator.SetBool("Playing Cocktail Arcade", true);
                        }
                    }
                    progressDisplay.gameObject.SetActive(true);
                    playing = true;
                    chosenArcade.advanceQueue = true;
                    chosenArcade.customer = controller;
                   // chosenArcade.occupied = true;
                    stats.economySystem.Give(chosenArcade.ussageCost);
                    stats.wallet -= chosenArcade.ussageCost;
                    stats.cycle.currentDayStats.arcadeSales += chosenArcade.ussageCost;
                    chosenArcade.occupant = stats.ID;
                }
                return result = BehaviourResult.RUNNING;
            }
            else
            {
                return result = BehaviourResult.RUNNING;
            }
        }
        else
        {
            stats.moral -= stats.ReconfigureMoral(stats.moralLossRange);
            previouslyDesired = desiredArcade;
        }
        return result = BehaviourResult.FAILURE;
    }
    public override float CalculateWeight()
    {
        return stats.boredom;
    }

    public bool ChooseGame(Transform origin, out Transform target)
    {
        List<Arcade> machines = new List<Arcade>(FindObjectsOfType<Arcade>());
        List<GameName> availableGames = new List<GameName>();

        foreach (var machine in machines)
        {
            if (!availableGames.Contains(machine.gameName))
            {
                availableGames.Add(machine.gameName);
            }
        }

        desiredArcade = GameName.None;

        if (previouslyDesired != GameName.None)
        {
            availableGames.Remove(previouslyDesired);
        }
        int random = Random.Range(0, availableGames.Count);

        if (random >= 0 && random < availableGames.Count)
        {
            desiredArcade = availableGames[random];//Randomly Choose Game
        }

        float minDist = Mathf.Infinity;
        target = null;
        foreach (var arcade in machines)
        {
            if (desiredArcade == arcade.gameName  && arcade.state != ArcadeState.BROKEN && stats.wallet >= arcade.ussageCost)
            {
                float dist = Vector3.Distance(origin.position, arcade.transform.position);
                if (dist < minDist)
                {
                    if(!CostToHigh(arcade))
                    {
                        target = arcade.playerStandingPos.transform;
                        chosenArcade = arcade;
                        minDist = dist;
                    }
                }
         
            }
        }

        if (target != null)
        {       
            if (!chosenArcade.occupied && chosenArcade.queue.Count == 0)
            {
               // previousPlayed = desiredArcade;
                targetPos = target.position;
                //chosenArcade.occupied = true;
                tree.SetBool(false, "Wandering");
                navigation.SetDestination(targetPos);
                return true;
            }
            else
            {
                if (GoToQueue() && chosenArcade.queue.Count < chosenArcade.queueSize)
                {
                    //previousPlayed = desiredArcade;
                    Queue();
                    return true;
                }
                else
                {
                    tree.SetFloat(0, "Wander Time");
                    target = null;
                    return false;
                }
            }
        }


        return false;
    }

    private void Queue()
    {
        if (!chosenArcade.queue.Contains(controller))
        {
            targetPos = chosenArcade.transform.position;
            targetPos -= chosenArcade.transform.forward * 2 + new Vector3(0, 0, chosenArcade.queue.Count);
            chosenArcade.queue.Add(controller);
            tree.SetBool(false, "Wandering");
            navigation.SetDestination(targetPos);
        }
    }



    private void ActivateStandingStool(Arcade arcade, bool status)
    {
        if (arcade.standingStool != null)
        {
            arcade.standingStool.SetActive(status);
        }
    }


    bool GoToQueue()
    {
        return (Random.value < 0.5f);
    }

    public bool CostToHigh(Arcade arcade)
    {
        if (arcade.ussageCost  == 0)
        {
            return false;
        }

        if (arcadeToIgnore != null)
        {
            if (arcade == arcadeToIgnore)
            {
                //Debug.Log("Already looked at this, Price is to high");
                return true;
            }
        }
        float sum = stats.wallet;
        float prob = (sum / arcade.ussageCost) * (arcade.currentLevel * 2);

        float dice = Random.Range(0, sum);

        if (prob > dice)
        {
            //Debug.Log(arcade.name + " is a good value" + " , " + "prob = " + prob + " , " + "dice = " + dice);
            return false;
        }

     //   Debug.Log(arcade.name + " costs too much" + " , " + "prob = " + prob + " , " + "dice = " + dice);

        arcadeToIgnore = arcade;

        return true;
    }

    public override void SaveData(ref AIData data)
    {
        bData.fieldData.Add(nameof(playing) + playing.ToString());
        bData.fieldData.Add(nameof(currentPlayTime) + currentPlayTime.ToString());

        bData.fieldData.Add(nameof(desiredArcade) + (int)desiredArcade);
        bData.fieldData.Add(nameof(previouslyDesired) + (int)previouslyDesired);
        if (chosenArcade != null)
        {
            if (chosenArcade.occupied)
            {
                if (chosenArcade.queue.Contains(controller))
                {
                    bData.fieldData.Add("QueueIndex" + chosenArcade.queue.IndexOf(controller));
                }
            }
            bData.fieldData.Add(nameof(chosenArcadesID) + chosenArcadesID);
        }
      
        if (chosenArcadePos != null)
        {
            bData.fieldData.Add(nameof(chosenArcadePos) + "x" + chosenArcade.transform.position.x.ToString());
            bData.fieldData.Add(nameof(chosenArcadePos) + "y" + chosenArcade.transform.position.y.ToString());
            bData.fieldData.Add(nameof(chosenArcadePos) + "z" + chosenArcade.transform.position.z.ToString());

            bData.fieldData.Add(nameof(targetPos) + "x" + targetPos.x.ToString());
            bData.fieldData.Add(nameof(targetPos) + "y" + targetPos.y.ToString());
            bData.fieldData.Add(nameof(targetPos) + "z" + targetPos.z.ToString());
        }

        base.SaveData(ref data);

    }

    public override void LoadData(AIData data)
    {
        AssignLoadedFieldData(nameof(playing), out playing);
        AssignLoadedFieldData(nameof(currentPlayTime), out currentPlayTime);

        int prevDesired, desArcade;

        AssignLoadedFieldData(nameof(desiredArcade), out desArcade);
        desiredArcade = (GameName)desArcade;
        AssignLoadedFieldData(nameof(previouslyDesired), out prevDesired);
        previouslyDesired = (GameName)prevDesired;

        AssignLoadedFieldData(nameof(targetPos) + "x", out targetPos.x);
        AssignLoadedFieldData(nameof(targetPos) + "y", out targetPos.y);
        AssignLoadedFieldData(nameof(targetPos) + "z", out targetPos.z);

        Vector3 chosenArcadePosition;

        AssignLoadedFieldData(nameof(chosenArcadePos) + "x", out chosenArcadePosition.x);
        AssignLoadedFieldData(nameof(chosenArcadePos) + "y", out chosenArcadePosition.y);
        AssignLoadedFieldData(nameof(chosenArcadePos) + "z", out chosenArcadePosition.z);

        AssignLoadedFieldData(nameof(chosenArcadesID), out chosenArcadesID);

        Arcade[] arcadeCabinets = FindObjectsOfType<Arcade>();

        foreach (var arcade in arcadeCabinets)
        {
            if (arcade.InstanceID == chosenArcadesID)
            {
                chosenArcadePos = arcade.transform;
                chosenArcade = arcade;
            }
        }

        if (chosenArcade != null)
        {
            if (chosenArcade.occupied)
            {
                int QueueIndex;
                AssignLoadedFieldData(nameof(QueueIndex), out QueueIndex);
                if (chosenArcade.queueIDs.Contains(stats.ID))
                {
                    chosenArcade.queue[QueueIndex] = controller;
                }
                else
                {
                    if (chosenArcade.cabinetType == ArcadeType.CLASSIC && stats.aiType == AIType.KID)
                    {
                        chosenArcade.standingStool.SetActive(true);
                        navigation.enabled = false;
                    }                 
                }
            }
        }




        base.LoadData(data);
    }
}

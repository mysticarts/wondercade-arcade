﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tree.Editor;

[System.Serializable]
public class AITree : ScriptableObject
{
    public List<NodeData> nodes = new List<NodeData>();

    [SerializeField]
    public List<Parameters> globalParameters = new List<Parameters>(); //the parameters that are created and binded to the tree.

    
    public _Behaviour GetBehaviour(string nodeName)
    {
        foreach (var node in nodes)
        {
            if (nodeName == node.name)
            {
                return node.behaviour;
            }
        }
        return null;
    }



    public void UpdateParameters()
    {
        foreach (var gParam in globalParameters)
        {
            foreach (var node in nodes)
            {
                foreach (var lParam in node.localParams)
                {
                    if (lParam.paramName == gParam.paramName)
                    {
                        lParam.type = gParam.type;
                        if (lParam.type == ParameterType.INT)
                        {
                            lParam.iConditionvalue = gParam.iConditionvalue;
                            lParam.nConditionTypes = gParam.nConditionTypes;
                        }
                        if (lParam.type == ParameterType.FLOAT)
                        {
                            lParam.fConditionvalue = gParam.fConditionvalue;
                            lParam.nConditionTypes = gParam.nConditionTypes;
                        }
                        if (lParam.type == ParameterType.BOOL)
                        {
                            lParam.bConditionvalue = gParam.bConditionvalue;
                            lParam.bConditionTypes = gParam.bConditionTypes;
                        }
                        lParam.paramName = gParam.paramName;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Setting the continous integer value on the desired node.
    /// </summary>
    public void SetInt(int value, string name)
    {
        foreach (var node in nodes)
        {
            foreach (var param in node.localParams)
            {
                if (param.paramName == name)
                {
                    param.iContinuousValue = value;
                }
            }
          
        }
    }

    /// <summary>
    /// Setting the continous floating point value on the desired node.
    /// </summary>
    public void SetFloat(float value, string name)
    {
        foreach (var node in nodes)
        {
            foreach (var param in node.localParams)
            {
                if (param.paramName == name)
                {
                    param.fContinuousValue = value;
                }
            }

        }
    }

    /// <summary>
    /// Setting the continous boolean value on the desired node.
    /// </summary>
    public void SetBool(bool value, string name)
    {
        foreach (var node in nodes)
        {
            foreach (var param in node.localParams)
            {
                if (param.paramName == name)
                {
                    param.bContinuousValue = value;
                }
            }

        }
    }

    public bool GetContinousBool(string name)
    {
        bool result = false;
        foreach (var node in nodes)
        {
            foreach (var param in node.localParams)
            {
                if (param.paramName == name)
                {
                    result = param.bContinuousValue;
                }
            }
        }

        return result;

    }

    public float GetContinousFloat(string name)
    {
        float result = 0;
        foreach (var node in nodes)
        {
            foreach (var param in node.localParams)
            {
                if (param.paramName == name)
                {
                    result = param.fContinuousValue;
                }
            }
        }

        return result;
    }

    public float GetContinousInt(string name)
    {
        int result = 0;
        foreach (var node in nodes)
        {
            foreach (var param in node.localParams)
            {
                if (param.paramName == name)
                {
                    result = param.iContinuousValue;
                }
            }
        }

        return result;
    }
    public bool GetConditionBool(string name)
    {
        bool result = false;
        foreach (var param in globalParameters)
        {
            if (param.paramName == name)
            {
                result = param.bConditionvalue;
            }
        }

        return result;

    }

    public float GetConditionFloat(string name)
    {
        float result = 0;
        foreach (var param in globalParameters)
        {
            if (param.paramName == name)
            {
                result = param.fConditionvalue;
            }
        }

        return result;
    }

    public float GetConditionInt(string name)
    {
        int result = 0;
        foreach (var param in globalParameters)
        {
            if (param.paramName == name)
            {
                result = param.iConditionvalue;
            }
        }

        return result;
    }

    /// <summary>
    /// Tests if the current parameter meets the condition.
    /// </summary>
    public bool Test(string name)
    {
        foreach (var node in nodes)
        {
            foreach (var param in node.localParams)
            {
                if (param.paramName == name)
                {
                    return param.Test();
                }
            }
        }
        return false;
    }


    public void ClearParameters()
    {
        globalParameters.Clear();
    }

    public void CreateParameter(string type)
    {

        Parameters param = new Parameters();

        param.OnCreate(type);

        globalParameters.Add(param);

    }
  
}




﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct ParameterData
{
    public string paramName;
    public int iParamValue;
    public float fParamValue;
    public bool bParamValue;
}




namespace Tree.Editor
{
    [Serializable]
    public enum ParameterType
    {
        BOOL = 0,
        INT = 1,
        FLOAT = 2,
    }
    [Serializable]
    public enum NumericalConditionType
    {
        GREATER = 0,
        LESS = 1,
        EQUAL = 2,
    }
    [Serializable]
    public enum BooleanConditionType
    {
        TRUE = 0,
        FALSE = 1,
    }

    [Serializable]
    public class Parameters 
    {
        public ParameterType type;
        public NumericalConditionType nConditionTypes;
        public BooleanConditionType bConditionTypes;
        
        //The values set in the parameters window (i.e. if the value is set at 35 and requires the continous value to be greater for the condition to be met).
        public float fConditionvalue = 0;
        public int iConditionvalue = 0;
        public bool bConditionvalue = false;

        //the values that user changes on the node.
        public float fContinuousValue = 0;
        public int iContinuousValue = 0;
        public bool bContinuousValue = false;

        public string paramName;

        public bool selected = false;

        [HideInInspector]
        public ParameterData pData;

        public void OnCreate(string createdType)
        {
            type = (ParameterType)Enum.Parse(typeof(ParameterType), createdType);
            paramName = "Untitled";
        }

        /// <summary>
        /// Testing if the parameter conditions are met.
        /// </summary>
        public bool Test()
        {
            if (type == ParameterType.INT)
            {
                if (nConditionTypes == NumericalConditionType.GREATER)
                {
                    if (iContinuousValue > iConditionvalue)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                if (nConditionTypes == NumericalConditionType.LESS)
                {
                    if (iContinuousValue < iConditionvalue)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                if (nConditionTypes == NumericalConditionType.EQUAL)
                {
                    if (iContinuousValue == iConditionvalue)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            if (type == ParameterType.FLOAT)
            {
                if (nConditionTypes == NumericalConditionType.GREATER)
                {
                    if (fContinuousValue > fConditionvalue)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                if (nConditionTypes == NumericalConditionType.LESS)
                {
                    if (fContinuousValue < fConditionvalue)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                if (nConditionTypes == NumericalConditionType.EQUAL)
                {
                    if (fContinuousValue == fConditionvalue)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            if (type == ParameterType.BOOL)
            {
                if (bConditionTypes == BooleanConditionType.TRUE)
                {
                    if (bContinuousValue == true)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                if (bConditionTypes == BooleanConditionType.FALSE)
                {
                    if (bContinuousValue == false)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public void SaveData(ref AIData data)
        {
            pData = new ParameterData();
            pData.paramName = paramName;
            pData.iParamValue = iContinuousValue;
            pData.fParamValue = fContinuousValue;
            pData.bParamValue = bContinuousValue;
            data.parameterData.Add(pData);
        }

        public void LoadData(AIData data)
        {
            for (int i = 0; i < data.parameterData.Count; i++)
            {
                if (paramName == data.parameterData[i].paramName)
                {
                    iContinuousValue = data.parameterData[i].iParamValue;
                    fContinuousValue = data.parameterData[i].fParamValue;
                    bContinuousValue =  data.parameterData[i].bParamValue;
                    break;
                }
            }   
        }

    }
}





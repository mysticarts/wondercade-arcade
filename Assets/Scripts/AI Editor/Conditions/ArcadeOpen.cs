﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tree.Editor;

public class ArcadeOpen : Condition
{
    public bool arcadeOpen = false;

    public override void Start()
    {

    }

    public override BehaviourResult execute()//execute behaviour
    {
        if (arcadeOpen)
        {   
            return result = BehaviourResult.SUCCESS;
        }
        return result = BehaviourResult.FAILURE;
    }
    public override float CalculateWeight()//calc weight score
    {    
        return 0;
    }

    public override void OnEnter()
    {
    }

    public override void SaveData(ref AIData data)
    {
        base.SaveData(ref data);

    }

    public override void LoadData(AIData data)
    {
        base.LoadData(data);

    }
}

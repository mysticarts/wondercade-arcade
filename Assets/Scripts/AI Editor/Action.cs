﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tree.Editor
{
    public abstract class Action : _Behaviour
    {

        public Selector selector;

        public float weightValue = 0;

        public float probability = 0;

        public Condition condition; //a reference to a condition script (if the previous node is a condition, this will be assigned to it. else it will never be assigned)

        public abstract override void Start();

        public abstract override BehaviourResult execute();//execute behaviour

        public abstract override float CalculateWeight();//calc weight score

        public abstract override void OnEnter();


    }
}



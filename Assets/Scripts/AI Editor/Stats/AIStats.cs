﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Tree.Editor;
using Pixelplacement;

[System.Serializable]
public struct AIData
{
    public string objID;
    public Vector3 position;
    public Vector3 rotation;
    public Vector4 color;
    public int wallet;
    public float morale;
    public float hunger;
    public float thirst;
    public float boredom;
    public float wanderTime;
    public string AIType;
    public bool inStore;
    public string instanceID;
    public Vector3 velocity;
    public List<BehaviourData> behavioursData;
    public List<ParameterData> parameterData;
    public AIAnimationData animationData;
    public AIOutFitData outfitData;
}

public enum AIType
{ 
    KID = 0,
    ADULT = 1
}


public class AIStats : MonoBehaviour
{
    public bool car = false;
    public AIAppearance appearanceGenerator;
    public AIAnimationHandler animation;
    [HideInInspector]
    public string ID;
    public AIType aiType;
    [HideInInspector]
    public AIController controller;
    public DayNightCycle cycle;
    [HideInInspector]
    public AIData aData = new AIData();
    [Range(0,100)]
    [Tooltip("The current value of the thirst meter")]
    public float thirst = 0;
    [Range(0, 100)]
    [Tooltip("The current value of the hunger meter")]
    public float hunger = 0;
    [Range(0, 100)]
    [Tooltip("The current value of the boredom meter")]
    public float boredom = 0;
    [Tooltip("The current value of the cleanliness satisfaction meter")]
    public float cleanliness = 100;
    [Tooltip("The amount of money this AI has")]
    public int wallet = 0;
    [Tooltip("The maximum amount of money this AI can hold")]
    public int maxWalletCapacity = 0;
    [HideInInspector]
    public Economy economySystem;
    [Header("----Moral Values----")]
    [Tooltip("The current value of the moral meter")]
    [Range(0,100)]
    public float moral = 0;
    private float maxMoral = 0;
    [Tooltip("Moral Lost Rate")]
    public float mlr = 0;
    [Range(0, 100)]
    public float moralGainRange = 0;
    [Range(0, 100)]
    public float moralLossRange = 0;
    private float mlrTimer = 0;

    [Header("----Utility OverTime----")]
    [Tooltip("The rate at which the AI gets hungry over time")]
    [Range(0,100)]
    public float hungerOverTime = 0;
    [Tooltip("The rate at which the AI gets thirsty over time")]
    [Range(0, 100)]
    public float thirstOverTime = 0;

    [Tooltip("The minimum value required for AI to get food")]
    public float hungerThreshold = 0;
    [Tooltip("The minimum value required for AI to get drinks")]
    public float thirstThreshold = 0;
    [Tooltip("The minimum value required for AI to get bored")]
    public float boredomThreshold = 0;
    [Tooltip("The minimum value required for AI to be happy")]
    public float happyThreshold = 0;
    public float angryThreshold = 0;

    [Tooltip("The morale lost in dirty rooms")]
    public float moraleLossMultiplier = 0;
    [Tooltip("the morale lost when doing an activity")]
    public float moraleLossScalar = 0;
    public LayerMask mask;

    public bool goToStore = false;
    public bool leftStoreUnsatisfied = false;

    [HideInInspector]
    public float wanderTimer = 0;
    
    public MenuUI_Customer customerMenu;

    private NotificationSystem notifications;
    // Start is called before the first frame update
    void Start()
    {
        ID = "AI" + GetInstanceID();
        if (controller == null)
        {
            controller = GetComponent<AIController>();
        }
        if (!car)
        {
            economySystem = FindObjectOfType<Economy>();
            cycle = FindObjectOfType<DayNightCycle>();
          
            controller.tree.SetFloat(moral, "Moral");
            maxMoral = moral;
        }

        notifications = GameObject.Find("Managers").GetComponent<NotificationSystem>();
        UpdateBars();
    }

    // Update is called once per frame
    void Update()
    {
        if (!car)
        {
            //morale notifications
            if (cycle.customers.Count != 0 && (cycle.customers.Count % cycle.howOftenToSendMoraleNotification) == 0)
            { //every X customers, send notification

                if (cycle.GetCurrentAverageMorale() > 66)
                    notifications.Add(new Notification("Customer Morale", "Your customers are happy!"));

                else if (cycle.GetCurrentAverageMorale() > 33)
                    notifications.Add(new Notification("Customer Morale", "Your customers are indifferent."));

                else notifications.Add(new Notification("Customer Morale", "Your customers are angry!"));

            }

            if (controller != null)
            {
                if (cycle.isDay && !leftStoreUnsatisfied && wallet > 0)
                {
                    if (!goToStore)
                    {
                        goToStore = true;
                        controller.tree.SetBool(true, "Navigate");
                    }
                    controller.tree.SetBool(true, "Navigate Arcade");

                }
                if (!cycle.isDay && !leftStoreUnsatisfied || wallet <= 0 && controller.tree.Test("Chosen Action"))
                {
                    if (goToStore)
                    {
                        controller.tree.SetBool(true, "Navigate");
                        controller.tree.SetBool(true, "Leaving");
                        goToStore = false;
                    }
                }

                if (controller.tree.GetContinousBool("Entered Arcade"))
                {
                    controller.tree.SetFloat(wanderTimer, "Wander Time");

                    if (controller.tree.Test("Entered Arcade") && controller.tree.Test("Chosen Action"))
                    {
                        controller.tree.SetBool(true, "Wandering");
                    }
                    else
                    {
                        controller.tree.SetBool(false, "Wandering");
                    }
                    if (hunger >= hungerThreshold || thirst >= thirstThreshold || boredom >= boredomThreshold)
                    {
                        if (InDirtyRoom())
                        {
                            moral -= ReconfigureMoral(moralLossRange) * moraleLossMultiplier;
                        }
                        else
                        {
                            if (!controller.tree.Test("Chosen Action"))
                            {
                                moral -= (ReconfigureMoral(moralLossRange) / (moraleLossScalar - (cleanliness/100)));
                            }
                            else
                            {
                                moral -= ReconfigureMoral(moralLossRange);
                            }

                        }
                    }

                    if (controller.tree.Test("Wandering") && controller.tree.Test("Chosen Action"))
                    {
                        

                        if (hunger <= hungerThreshold || thirst <= thirstThreshold || boredom <= boredomThreshold)
                        {
                            moral += ReconfigureMoral(moralGainRange);
                        }
                        wanderTimer += Time.deltaTime % 60;
                        boredom += Time.deltaTime * 2;
                        thirst += thirstOverTime * Time.deltaTime / 2;
                        hunger += hungerOverTime * Time.deltaTime / 2;
                    }

                    if (controller.tree.Test("Wander Time"))
                    {
                        controller.tree.SetBool(true, "Find Arcade");
                    }

                    thirst = Mathf.Clamp(thirst, 0, 100);
                    hunger = Mathf.Clamp(hunger, 0, 100);

                    if (hunger >= hungerThreshold)
                    {
                        controller.tree.SetBool(true, "Need Food");
                    }
                    else
                    {     
                        controller.tree.SetBool(false, "Need Food");
                    }

                    if (thirst >= thirstThreshold)
                    {

                        controller.tree.SetBool(true, "Need Drink");
                    }
                    else
                    {
                        controller.tree.SetBool(false, "Need Drink");
                    }

                    if (!controller.tree.Test("Moral"))
                    {
                        if (!leftStoreUnsatisfied)
                        {
                            controller.tree.SetBool(true, "Navigate");
                        }
                    }
                }
                else
                {
                    controller.tree.SetBool(false, "Find Arcade");                   
                }

                controller.tree.SetFloat(moral, "Moral");


                moral = Mathf.Clamp(moral, 0, maxMoral);

                UpdateBars();
            }
        }
        
    }

    public void UpdateBars() {
        if(customerMenu != null) {
            customerMenu.excitement.text = "" + (int)(100 - boredom);
            customerMenu.cleanliness.text = "" + (int)cleanliness;
            customerMenu.hunger.text = "" + (int)hunger;
            customerMenu.thirst.text = "" + (int)thirst;
            customerMenu.wallet.text = "" + (int)wallet;
            UpdateBar(customerMenu.excitementBar, (100 - boredom) / 100);
            UpdateBar(customerMenu.cleanlinessBar, cleanliness / 100);
            UpdateBar(customerMenu.hungerBar, hunger / 100);
            UpdateBar(customerMenu.thirstBar, thirst / 100);
            UpdateBar(customerMenu.walletBar, (float)wallet / (float)maxWalletCapacity);
        }
    }
    public void UpdateBar(UIobject bar, float percentageFilled) {
        if(PlayerManager.menuUsing == customerMenu && !customerMenu.isOpening) {
            Vector2 previousSize = bar.currentSize;
            bar.currentSize = new Vector2(bar.poppedUpSize.x * percentageFilled, bar.poppedUpSize.y);
            Tween.Size(bar.gameObject.GetComponent<RectTransform>(), previousSize, bar.currentSize, 0.5f, 0f, Tween.EaseInOutStrong, Tween.LoopType.None, null, null, false);
        }
    }

    private void OnDisable()
    {
        if (controller != null)
        {
            controller.ResetTree();
        }
    }


    public bool InDirtyRoom()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, -transform.up,out hit,4, mask))
        {
            if (hit.transform.tag == "Dirty")
            {
                return true;
            }
        }
        return false;
    }


    public float ReconfigureMoral(float range)
    {
        float moralValue = 0;

        if (mlrTimer >= mlr)
        {
            mlrTimer = 0;
            moralValue = Random.Range(0, range + 1);
        }
        mlrTimer += Time.deltaTime * 2;

        return moralValue;
    }

    public void SaveData(GameData data)
    {
        aData.objID = name;
        aData.position = transform.position;
        aData.rotation = transform.rotation.eulerAngles;
        aData.instanceID = ID;
        if (!car)
        {
            aData.color = GetComponent<Renderer>().material.color;
            aData.morale = moral;
            aData.hunger = hunger;
            aData.thirst = thirst;
            aData.boredom = boredom;
            aData.wallet = wallet;
            aData.wanderTime = wanderTimer;
            aData.velocity = controller.navigationalMesh.velocity;
            aData.inStore = goToStore;
            animation.SaveData(ref aData);
            appearanceGenerator.SaveData(ref aData);
        }
       
        aData.behavioursData = new List<BehaviourData>();
        aData.parameterData = new List<ParameterData>();

        if (controller == null)
        {
            controller = GetComponent<AIController>();
        }

        foreach (var node in controller.tree.nodes)
        {
            if (node.nodeType != NodeType.Root)
            {
                node.behaviour.bData.fieldData = new List<string>();
                node.behaviour.SaveData(ref aData);
                foreach (var param in node.localParams)
                {
                    param.SaveData(ref aData);
                }
            }
           
        }
        if (!car)
        {
            string instance = "(Clone)";
            aData.AIType = name.TrimEnd(instance.ToCharArray());
        }

        data.AIData.Add(aData);
    }

    public void LoadData(GameData data)
    {
        if (controller == null)
        {
            controller = GetComponent<AIController>();
        }

        if (!car)
        {
            controller.navigationalMesh.enabled = false;
        }

        transform.position = aData.position;
        transform.rotation = Quaternion.Euler(aData.rotation);
        ID = aData.instanceID;
        if (!car)
        {
            GetComponent<Renderer>().material.color = aData.color;
            moral = aData.morale;
            hunger = aData.hunger;
            thirst = aData.thirst;
            boredom = aData.boredom;
            wallet = aData.wallet;
            wanderTimer = aData.wanderTime;
            controller.navigationalMesh.velocity = aData.velocity;
            goToStore = aData.inStore;
            appearanceGenerator.LoadData(aData.outfitData);
            animation.LoadData(aData.animationData);
        }
       
        controller.InitializeTree();
        controller.gameSaveLoaded = true;
        for (int i = 0; i < controller.tree.nodes.Count; i++)
        {
            if (controller.tree.nodes[i].nodeType != NodeType.Root)
            {
                controller.tree.nodes[i].behaviour.bData = aData.behavioursData[i - 1];
                controller.tree.nodes[i].behaviour.LoadData(aData);
                for (int j = 0; j < controller.tree.nodes[i].localParams.Count; j++)
                {
                    controller.tree.nodes[i].localParams[j].LoadData(aData);
                }
            }
           
        }      
    }
}

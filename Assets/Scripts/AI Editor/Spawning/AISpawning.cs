﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;




public class AISpawning : MonoBehaviour
{
    public AdvertisingManager advertisingManager;
    public int maxAIForDay = 0;

    public List<AIController> AITypes = new List<AIController>();
    
    public List<AIController> AIPool = new List<AIController>();
    public List<AIController> activeAI = new List<AIController>();
    public List<Transform> spawningPoints = new List<Transform>();
    public int poolSize = 0;
    public float spawnFrequency = 0;
    [HideInInspector]
    public int activeAICounter = 0;
    private float currentSpawnTimer = 0;

    [Tooltip("The minimum amount of customers spawned with absolute worst shop stats")]
    public int minOffStreetCustomers = 1;
    [Tooltip("The maximum amount of customers spawned with absolute worst shop stats")]
    public int maxOffStreetCustomers = 4;

    public float moraleMultiplier = 2;

    private DayNightCycle cycle;

    public List<System.String> firstNames;
    public System.String firstName() {
        return firstNames[UnityEngine.Random.Range(0, firstNames.Count)];
    }

    public List<System.String> lastNames;
    public System.String lastName() {
        return lastNames[UnityEngine.Random.Range(0, lastNames.Count)];
    }

    void Start()
    {
        cycle = FindObjectOfType<DayNightCycle>();
        if (AIPool.Count == 0 && AITypes.Count > 0)
        {
            GeneratePool();
        }
    }

    //on new day calculate new max
    //update pool max size
    public void DetermineMaxAIforDay()
    {
        int bareMinimumAmount = UnityEngine.Random.Range(minOffStreetCustomers, maxOffStreetCustomers);
        int advertisingAddition = advertisingManager.GetAdvertisingAddition();
        float satisfaction = (WeekStats.GetSatisfaction() / 100) * moraleMultiplier;

        // your morale multplier will vary between this and (2-this), so make it between 0 and 1
        float baseMultiplier = 0.3f;

        maxAIForDay = Mathf.CeilToInt((bareMinimumAmount + advertisingAddition) * (baseMultiplier + (1 - baseMultiplier) * satisfaction));

        if (maxAIForDay < 4)
        {
            maxAIForDay = 4;
        }

    }



    // Update is called once per frame
    void Update()
    {
        if (cycle.storeOpen)
        {
            currentSpawnTimer += Time.deltaTime * 2;

            if (currentSpawnTimer >= spawnFrequency)
            {
                if (activeAI.Count < maxAIForDay)
                {
                    Spawn();
                }
            }
        }
      
    }

    public void Despawn(AIController controller)
    {
        controller.gameObject.SetActive(false);
        activeAI.Remove(controller);
        activeAICounter--;
    }

    void Spawn()
    {
        bool AIFound = false;

        if (activeAI.Count < AIPool.Count)
        {
            while (!AIFound)
            {
                int AISelector = Random.Range(0, AIPool.Count);

                if (!AIPool[AISelector].gameObject.activeInHierarchy)
                {
                    AIPool[AISelector].gameObject.SetActive(true);
                    AIStats stats = AIPool[AISelector].GetComponent<AIStats>();
                    stats.wallet = Random.Range(0, stats.maxWalletCapacity);
                    activeAI.Add(AIPool[AISelector]);
                    AIFound = true;
                    currentSpawnTimer = 0;
                }

            }
        }
    }


    public void ShufflePool()
    {
        int size = AIPool.Count;
        int last = size - 1;
        for (int i = 0; i < last; i++)
        {
            int r = Random.Range(i, size);
            AIController temp = AIPool[i];
            AIPool[i] = AIPool[r];
            AIPool[r] = temp;
        }


    }



    public void GeneratePool()
    {
        int aiTypeIter = 0;
        for (int i = 0; i < poolSize; i++)
        {
           // int ranAIType = Random.Range(0, AITypes.Count);
            if (i == (poolSize/2))
            {
                aiTypeIter++;
            }

            int ranSpawnPoint = Random.Range(0, spawningPoints.Count);

            NavMeshHit hit;

            if (NavMesh.SamplePosition(spawningPoints[ranSpawnPoint].position, out hit,1000, 1 << NavMesh.GetAreaFromName("Walkable")))
            {
                AIController generatedAI = Instantiate(AITypes[aiTypeIter], hit.position, spawningPoints[ranSpawnPoint].rotation);
                generatedAI.gameObject.SetActive(false);
                Debug.Log("added");
                AIPool.Add(generatedAI);

            }




        }


        ShufflePool();

    }

 

}

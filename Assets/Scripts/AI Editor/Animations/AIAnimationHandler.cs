﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Animations;

[System.Serializable]
public struct AIAnimationData
{
    public List<string> animParamNames;
    public List<string> animParamValue;
    public List<string> animParamType;
    public string currentAnimation;
    public float currentAnimationTime;
}



public class AIAnimationHandler : MonoBehaviour
{
    public AIController controller;
    public AIAnimationData animData;
    public Animator animator;
    public NavMeshAgent agent;
    public List<string> states = new List<string>();
    private float velocityX = 0;
    private float velocityY = 0;
    [Tooltip("Used to scale the velocity threshold for changing movement animation.")]
    public float scalar = 0;
    private Vector3 velocity;
    private Vector3 lastPosition;
    private Vector3 rotationLast;
    private Vector3 angularVelocity;
    public float walkAnimThreshold = 0;
    public AIStats stats;
    [HideInInspector]
    public bool replenish = false;
    private bool gameSaveLoaded = false;

    // Start is called before the first frame update
    void Start()
    {
        if (!gameSaveLoaded)
        {
            animator.SetBool("Wandering", true);
        }
        rotationLast = agent.transform.rotation.eulerAngles;
        lastPosition = agent.transform.position;
    }

    void Update()
    {
        if (!controller.tree.Test("Moral"))
        {
            if (!animator.GetCurrentAnimatorStateInfo(0).IsName("Movement"))
            {
                animator.Play("Movement", 0);
            }
        }


        velocity = agent.transform.position - lastPosition;

        lastPosition = agent.transform.position;
        if (velocity.z * scalar > walkAnimThreshold)
        {
            velocityY += Time.deltaTime * 60;
        }

        if (velocity.z * scalar < -walkAnimThreshold)
        {
            velocityY -= Time.deltaTime * 60;
        }

        if (velocity.z * scalar > -walkAnimThreshold && velocity.z * scalar < walkAnimThreshold)
        {
            velocityY = 0;
        }

        angularVelocity = agent.transform.rotation.eulerAngles - rotationLast;
        rotationLast = agent.transform.rotation.eulerAngles;

        velocityX = angularVelocity.y;

        angularVelocity.Normalize();


        velocityX = Mathf.Clamp(velocityX, -1, 1);
        velocityY = Mathf.Clamp(velocityY, -1, 1);

        animator.SetFloat("BlendY", velocityX);

        animator.SetFloat("BlendX", velocityY);
    }

    //Animation Event.
    public void Replenish()
    {
        replenish = true;
    }

    public string GetParameterValue(AnimatorControllerParameter param)
    {
        if (param.type == AnimatorControllerParameterType.Int)
        {
            animData.animParamType.Add("Int");
            return animator.GetInteger(param.name).ToString();
        }
        if (param.type == AnimatorControllerParameterType.Float)
        {
            animData.animParamType.Add("Float");
            return animator.GetFloat(param.name).ToString();
        }
        if (param.type == AnimatorControllerParameterType.Bool)
        {
            animData.animParamType.Add("Bool");
            return animator.GetBool(param.name).ToString();
        }
        return "";
    }

    public void SaveData(ref AIData data)
    {
        animData.animParamNames = new List<string>();
        animData.animParamValue = new List<string>();
        animData.animParamType = new List<string>();

        AnimatorControllerParameter[] param_s = animator.parameters;

        foreach (var param in param_s)
        {
            animData.animParamNames.Add(param.name);
            animData.animParamValue.Add(GetParameterValue(param));
        }

        AnimatorStateInfo currentState = animator.GetCurrentAnimatorStateInfo(0);
        //AnimatorClipInfo[] currentState = animator.GetCurrentAnimatorClipInfo(0);

        foreach (var state in states)
        {
            if (currentState.IsName(state))
            {
                animData.currentAnimation = state;
                animData.currentAnimationTime = currentState.normalizedTime;
            }
        }
        data.animationData = animData;
    }

    public void LoadData(AIAnimationData data)
    {
        gameSaveLoaded = true;
        for (int i = 0; i < data.animParamNames.Count; i++)
        {
            if (data.animParamType[i] == "Int")
            {
                int value = 0;
                if (int.TryParse(data.animParamValue[i], out value))
                {
                    animator.SetInteger(data.animParamNames[i], value);
                }
            }
            if (data.animParamType[i] == "Float")
            {
                float value = 0;
                if (float.TryParse(data.animParamValue[i], out value))
                {
                    animator.SetFloat(data.animParamNames[i], value);
                }
            }
            if (data.animParamType[i] == "Bool")
            {
                bool value = false;
                if (bool.TryParse(data.animParamValue[i], out value))
                {
                    animator.SetBool(data.animParamNames[i], value);
                }
            }
        }

        if (data.currentAnimationTime != 0)
        {
            animator.Play(data.currentAnimation, 0, data.currentAnimationTime);
        }


        // animator.state


    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;


public enum EmotionalStatus
{
    HAPPY = 0,
    ANGRY = 1,
    INDIFFERENT = 2,
    THIRSTY = 3,
    HUNGRY = 4,
}



public class EmotesHandler : MonoBehaviour
{
    public AIStats stats;
    public List<EmotionalStatus> moods = new List<EmotionalStatus>();
    private List<GameObject> moodsDisplay = new List<GameObject>();
    public float displayFrequency = 0;
    private float currentFrequency = 0;
    public float displayTimer = 0;
    private float currentDisplayTime = 0;
    private float emoteDisplayTime = 0;
    private float currentemoteDisplayTime = 0;
    private bool displayBubble = false;
    public float bubbleScale = 0;
    private int iter = 0;
    [Header("----Emotes----")]
    public GameObject happyEmote;
    public GameObject angryEmote;
    public GameObject hungryEmote;
    public GameObject thirstyEmote;
    public GameObject indifferentEmote;
    private GameObject currentEmote;
       
    // Start is called before the first frame update
    void Start()
    {
           
    }

    // Update is called once per frame
    void Update()
    {
        if (!displayBubble)
        {
            CheckMood(stats.happyThreshold,0, stats.moral, EmotionalStatus.HAPPY, happyEmote, false, false);
            CheckMood(stats.angryThreshold,0, stats.moral, EmotionalStatus.ANGRY, angryEmote, true, false);
            CheckMood(stats.happyThreshold, stats.angryThreshold, stats.moral, EmotionalStatus.INDIFFERENT, indifferentEmote, false, true);
            CheckMood(stats.hungerThreshold,0, stats.hunger, EmotionalStatus.HUNGRY, hungryEmote, false, false);
            CheckMood(stats.thirstThreshold,0, stats.thirst, EmotionalStatus.THIRSTY, thirstyEmote, false, false);
        }

        DisplayEmote();
    }


    void DisplayEmote()
    {
        if (currentFrequency <= displayFrequency)
        {
            currentFrequency += Time.deltaTime % 60;
        }
        else
        {
            if (!displayBubble)
            {
                Tween.LocalScale(transform, new Vector3(bubbleScale, bubbleScale, bubbleScale), 0.3f, 0);
                displayBubble = true;
                emoteDisplayTime = displayTimer / moods.Count;
                if (iter < moodsDisplay.Count)
                {
                    moodsDisplay[iter].SetActive(true);
                }
            }
            if (displayBubble)
            {
                currentDisplayTime += Time.deltaTime % 60;
                currentemoteDisplayTime += Time.deltaTime % 60;
                if (currentemoteDisplayTime >= emoteDisplayTime)
                {
                    moodsDisplay[iter].SetActive(false);
                    iter++;
                    if (iter < moodsDisplay.Count)
                    {
                        moodsDisplay[iter].SetActive(true);
                    }
                    currentemoteDisplayTime = 0;
                }



                if (currentDisplayTime >= displayTimer)
                {
                    displayBubble = false;
                    currentFrequency = 0;
                    currentDisplayTime = 0;
                    Tween.LocalScale(transform, new Vector3(0, 0, 0), 0.3f, 0);
                    iter = 0;
                }
            }


        }
    }




    void CheckMood(float threshold, float secondThreshold, float continuous, EmotionalStatus mood, GameObject emoteDisplay, bool belowThreshold, bool betweenThreshold)
    {
        if (!belowThreshold && !betweenThreshold)
        {
            if (continuous >= threshold)
            {
                if (!moods.Contains(mood))
                {
                    moods.Add(mood);
                    moodsDisplay.Add(emoteDisplay);
                }
            }
            else
            {
                if (moods.Contains(mood))
                {
                    moods.Remove(mood);
                    moodsDisplay.Remove(emoteDisplay);
                }
            }
        }
        if (betweenThreshold)
        {
            if (continuous <= threshold && continuous >= secondThreshold)
            {
                if (!moods.Contains(mood))
                {
                    moods.Add(mood);
                    moodsDisplay.Add(emoteDisplay);
                }
            }
            else
            {
                if (moods.Contains(mood))
                {
                    moods.Remove(mood);
                    moodsDisplay.Remove(emoteDisplay);
                }
            }
        }
        if (belowThreshold && !betweenThreshold)
        {
            if (continuous <=  threshold)
            {
                if (!moods.Contains(mood))
                {
                    moods.Add(mood);
                    moodsDisplay.Add(emoteDisplay);
                }
            }
            else
            {
                if (moods.Contains(mood))
                {
                    moods.Remove(mood);
                    moodsDisplay.Remove(emoteDisplay);
                }
            }
        }


    }



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Tree.Editor;

[System.Serializable]
public struct BehaviourData
{
   // public int result;    
    public List<string> fieldData;
   // public PatrolData pData;
}


[System.Serializable]
public abstract class _Behaviour : ScriptableObject {

    [HideInInspector]
    public GameObject agentObj;
    [HideInInspector]
    public NavMeshAgent navigation;
    public NodeData node;
    [HideInInspector]
    public AIController controller;

    public BehaviourData bData = new BehaviourData();
    public bool gameSaveLoaded = false;

    public enum BehaviourResult
    {
        SUCCESS = 0,
        FAILURE = 1,
        RUNNING = 2
    }
    // Use this for initialization
    public AITree tree;

    /// <summary>
    /// Called upon entering the behaviour.
    /// </summary>
    public abstract void OnEnter();


    public abstract void Start();

    public BehaviourResult result = BehaviourResult.FAILURE;
    
    public abstract BehaviourResult execute(); //execute behaviour

    public abstract float CalculateWeight(); //calc weight score

    public virtual void SaveData(ref AIData data)
    {
        // bData.result = (int)result;
        bData.fieldData.Add(nameof(result) + (int)result);
        data.behavioursData.Add(bData);
    }

    public virtual void LoadData(AIData data)
    {
        int r = 0;
        AssignLoadedFieldData(nameof(result), out r);
        result = (BehaviourResult)r;
        //result = (BehaviourResult)bData.result;
        gameSaveLoaded = true;
    }

    public void AssignLoadedFieldData<T>(string fieldName, out T input)
    {
        T value = default(T);
        foreach (var fieldData in bData.fieldData)
        {
            if (fieldData.Contains(fieldName))
            {
                string field = fieldData;
                field = field.Replace(fieldName, "");
                value = (T)System.Convert.ChangeType(field, typeof(T));
                break;
            }
        }

        input = value;
    }

}

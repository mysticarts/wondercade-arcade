﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tree.Editor;


[CreateAssetMenu(fileName = "Selector", menuName = "AI Behaviour/Selector", order = 0)]
public class Selector : _Behaviour
{
    public List<Action> actions = new List<Action>();
    //public Action chosenAction = null;

    public enum SelectionMethod
    {
        Roulette,
        BestChoice
    }

    public SelectionMethod selection;
    public Action chosenAction;
    private int chosenActionIter = 0;
    private bool actionSelected = false;

    public override void Start()
    {

    }

    public override void OnEnter()
    {

    }

    public override BehaviourResult execute()
    {
        if (actions.Count > 0) //if the selector is connected to actions.
        {
            if (chosenAction == null)
            {
                actionSelected = false;
                if (Evaluate(agentObj) == BehaviourResult.SUCCESS) //if selectors evaluation returns success
                {
                    return result = BehaviourResult.SUCCESS;
                }
                else
                {
                    return result = BehaviourResult.RUNNING;
                }
            }
            if (chosenAction != null)
            {
                actionSelected = true;
                if (chosenAction.result == BehaviourResult.SUCCESS)
                {
                    tree.SetBool(false, "Chosen Action");
                    chosenAction.result = BehaviourResult.FAILURE;
                    chosenAction = null;
                    return result = BehaviourResult.RUNNING;
                }
                chosenAction.execute(); //execute chosen action by the selector      
                return result = BehaviourResult.SUCCESS;
            }
        }
        return result = BehaviourResult.FAILURE;
    }


    public BehaviourResult Evaluate(GameObject agentObj)
    {
        if (selection == SelectionMethod.BestChoice)
        {
            return BestChoiceEvaluation(agentObj);
        }
        
        if (selection == SelectionMethod.Roulette)
        {
            return RouletteEvaluation(agentObj);
        }

        return result = BehaviourResult.FAILURE;
    }

    public BehaviourResult BestChoiceEvaluation(GameObject agentObj)
    {
        List<float> scores = GetScores(agentObj, actions);

        //sort score
        scores.Sort(); //sort lowest scores lowest to highest

        foreach (var action in actions)
        {
            if (action.weightValue == scores[0]) //if the actions equal the best value
            {
                if (TestChosenAction(action))
                {
                    return result = BehaviourResult.SUCCESS;
                }
                else
                {
                    return result = BehaviourResult.FAILURE;
                }
            }
        }
        return result = BehaviourResult.FAILURE;
    }

    public BehaviourResult RouletteEvaluation(GameObject agentObj)
    {
        float sum = CalculateSum(GetScores(agentObj, actions));
        float probabilitySum = CalculateProbability(sum);

        float dice = Random.Range(0, sum);
        sum = 0;
        for (int i = 0; i < actions.Count; i++)
        {
            sum += actions[i].probability;
            if (sum > dice)
            {
                if (TestChosenAction(actions[i]))
                {
                    return result = BehaviourResult.SUCCESS;
                }
                else
                {
                    return result = BehaviourResult.FAILURE;
                }
            }
        }
        return BehaviourResult.FAILURE;
    }


    bool TestChosenAction(Action action)
    {
        if (action.node.TestParameters())
        {
            chosenAction = action;

            return true;
        }
        action = null;
        return false;
    }


    List<float> GetScores(GameObject agentObj, List<Action> actions)
    {
        List<float> scores = new List<float>();
        foreach (var action in actions)
        {
            scores.Add(action.CalculateWeight()); //Calculate weighted values of each node           
        }
        return scores;
    }

    float CalculateProbability(float sum)
    {
        float probSum = 0;

        foreach (var action in actions)
        {
            action.probability = probSum + (action.weightValue / sum);
            probSum += action.probability;
        }

        return probSum;
    }

    float CalculateSum(List<float> scores)
    {
        float sum = 0;

        foreach (var action in actions)
        {
            sum += action.weightValue;
        }
        return sum;
    }

    public override float CalculateWeight()
    {
        return 0;
    }

    public override void SaveData(ref AIData data)
    {
        if (actionSelected)
        {
            chosenActionIter = actions.IndexOf(chosenAction);
            bData.fieldData.Add(nameof(chosenActionIter) + chosenActionIter);
        }

        bData.fieldData.Add(nameof(actionSelected) + actionSelected);




        base.SaveData(ref data);

    }

    public override void LoadData(AIData data)
    {
        AssignLoadedFieldData(nameof(actionSelected), out actionSelected);

        if (actionSelected)
        {
            AssignLoadedFieldData(nameof(chosenActionIter), out chosenActionIter);
            if (chosenActionIter >= 0  && chosenActionIter < actions.Count)
            {
                chosenAction = actions[chosenActionIter];
            }
        }


        base.LoadData(data);

    }
}

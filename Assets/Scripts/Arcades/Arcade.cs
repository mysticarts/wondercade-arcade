﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using Pixelplacement;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public enum ArcadeState {
    NORMAL,
    BROKEN,
    DIRTY
}

public enum ArcadeType
{
    COCKTAIL,
    CLASSIC,
}

//name these same as scene name
public enum GameName {
    None = -1,
    Pong = 0,
    Agar = 1,
    MortalKombat = 2,
    AirHockey = 3,
    SpaceInvaders = 4
}

public class Arcade : Machines {

    public MenuUI_Arcade menu_arcade;

    public GameName gameName;
    public ArcadeType cabinetType;

    public GameObject standingStool;
    public Transform stoolStandingPos;
    private AISpawning AISpawner;
    [HideInInspector]
    public AIController customer;

    protected ViewManager viewManager;

    public GameObject gameViewPos;
    public GameObject playerStandingPos;

    public GameObject manageButton;
    public GameObject cleanRepairButton;
    public GameObject playButton;
    public GameObject diegeticUI;
    public Vector3 poppedUpSize = new Vector3(1, 0.03f, 0.5f);
    
    public GameObject rotatingCabinet;

    [Header("----Stats-----")]
    public int timesPlayed = 0;
    public int totalSales = 0;

    public float breakage = 0;
    public float dirtiness = 0;
    public ArcadeState state = ArcadeState.NORMAL;

    public bool waitingToBegin = false;

    public bool occupied = false;

    public List<AIController> queue = new List<AIController>();
    [HideInInspector]
    public List<string> queueIDs = new List<string>();
    public int queueSize = 0;
    [HideInInspector]
    public bool advanceQueue = false;

    [Tooltip("When customer uses arcade, what is the chance of them DAMAGING it a little? If not damaging, it will dirty")] [Range(0, 100)] public int chanceOfBreakingOnDeteriorate = 30;
    [Tooltip("Multiply deterioration by arcade level (the higher the level, the less likely to break)")] public float arcadeLevelMultiplier = 0.1f;
    [Tooltip("Random deterioration: min amount")] public float minDeterioration = 0.01f;
    [Tooltip("Random deterioration: max amount")] public float maxDeterioration = 0.1f;

    private NotificationSystem notifications;
    
    public override void SetMenu() {
        base.menu = menu_arcade;
    }

    virtual public void BeginGame() {
        // zoom into screen:

        //put player in front of arcade
        viewManager.player.transform.position = playerStandingPos.transform.position;

        //face player to arcade
        Vector3 lookPos = gameViewPos.transform.position - viewManager.player.transform.position; lookPos.y = 0;
        viewManager.player.transform.rotation = Quaternion.LookRotation(lookPos);

        //update the game view to move to
        viewManager.gameViewPos.transform.SetPositionAndRotation(gameViewPos.transform.position, gameViewPos.transform.rotation);

        viewManager.Switch(View.GAME);
        waitingToBegin = true;
    }

    public void Update() {
        base.Update();


        if (customer != null)
        {
            if (Vector3.Distance(customer.transform.position, playerStandingPos.transform.position) > 3 && occupied)
            {
                customer = null;
                Debug.Log("GodDammit");
                if (queue.Count > 0)
                {
                    advanceQueue = true;
                }
            }
        }     

        if (customer != null)
        {
            occupied = true;
        }
        else
        {
            occupied = false;
        }

        


        if(playerIsInRadius) {
            if(input.GetButtonDown("PlayArcade")) {
                if(state == ArcadeState.NORMAL)
                {
                    if (Application.CanStreamedLevelBeLoaded(gameName.ToString()))
                    {
                        BeginGame();
                    }
                }
                else if(state == ArcadeState.DIRTY || state == ArcadeState.BROKEN)
                    RestoreMachine();
            }
        }

        if (waitingToBegin && viewManager.viewChangeComplete) {
            viewManager.viewChangeComplete = false;
            waitingToBegin = false;
            Serialization serialization = FindObjectOfType<Serialization>();
            serialization.SaveGame();
            Tween.StopAll();
            SceneManager.LoadScene(gameName.ToString());
        }

        if (!occupied)
        {
            if (advanceQueue)
            {
                if (queue.Count > 0)
                {
                    AdvanceQueue();
                    Debug.Log("Advance Queue");
                }
            }
        }

    }

    void AdvanceQueue()
    {
        for (int i = 0; i < queue.Count; i++)
        {
            Vector3 target = Vector3.zero;
            if (i == 0)
            {
                target = playerStandingPos.transform.position;
            }
            else
            {
                target = queue[i - 1].transform.position;
            }

            ChooseArcade choose = (ChooseArcade)queue[i].tree.GetBehaviour("Find&Play Game");

            if (choose != null)
            {
                choose.targetPos = target;
            }


           // queue[i].navigationalMesh.SetDestination(target);
        }
        queue.RemoveAt(0);
        advanceQueue = false;
    }

    public override void ShowDiegeticUI(bool show) {
        if(show) {
            PlayerManager.arcadeUsing = this;

            //pop up manage button:
            Tween.LocalScale(manageButton.transform, poppedUpSize, 0.75f, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
            if(state == ArcadeState.NORMAL)
            {
                //pop up play button
                Tween.LocalScale(playButton.transform, poppedUpSize, 0.75f, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
            }
                
            else Tween.LocalScale(cleanRepairButton.transform, poppedUpSize, 0.75f, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
            
        } else {
            PlayerManager.arcadeUsing = null;

            //pop down all buttons:
            Tween.LocalScale(manageButton.transform, Vector3.zero, 0.2f, 0, Tween.EaseOutStrong, Tween.LoopType.None, null, null, false);
            Tween.LocalScale(playButton.transform, Vector3.zero, 0.2f, 0, Tween.EaseOutStrong, Tween.LoopType.None, null, null, false);
            Tween.LocalScale(cleanRepairButton.transform, Vector3.zero, 0.2f, 0, Tween.EaseOutStrong, Tween.LoopType.None, null, null, false);
        }
    }

    public void Start() {
        base.Start();
        notifications = GameObject.Find("Managers").GetComponent<NotificationSystem>();

        AISpawner = FindObjectOfType<AISpawning>();

        viewManager = Camera.main.GetComponent<ViewManager>();

        currentStatusText = functioningText;
       
        //UpdateBar(menu_arcade.levelBar, currentLevel / levelPrefabs.Count);
        base.Start();
        menu_arcade.breakage.text = "" + (int)breakage;
        UpdateBar(menu_arcade.breakageBar, breakage / 100);
        UpdateBar(menu_arcade.levelBar, (float)currentLevel / (float)levelPrefabs.Count);
        menu_arcade.level.text = "" + currentLevel;
        if (smoke != null && sparks != null) {
            smoke.Stop();
            sparks.Stop();
        }

        if(state == ArcadeState.BROKEN || breakage >= 100) {
            BreakMachine();
        }
        else if(state == ArcadeState.DIRTY || dirtiness >= 100) {
            DirtyMachine();
        }
    }
    
    //use when customer plays machine
    public void Deteriorate() {
        //wears out over time
        //upgrading reduces the chance of breaking

        if(state == ArcadeState.NORMAL) {

            float randomAffect = Random.Range(minDeterioration, maxDeterioration);
            float levelAffect = arcadeLevelMultiplier*(levelPrefabs.Count-currentLevel);
            float deteriorateAmount = levelAffect * randomAffect;
            
            int random = Random.Range(0, 100);
            if(random < chanceOfBreakingOnDeteriorate)
                breakage += deteriorateAmount;
            else dirtiness += deteriorateAmount;

            if(breakage > 100) 
                BreakMachine();
            if(dirtiness > 100)
                DirtyMachine();
                
            //UI:
            menu_arcade.breakage.text = "" + (int)breakage;
            UpdateBar(menu_arcade.breakageBar, breakage/100);
            
            menu_arcade.dirtiness.text = "" + (int)dirtiness;
            UpdateBar(menu_arcade.dirtinessBar, dirtiness/100);

        }

    }
    
    public Texture normalTex;
    public List<Texture> brokenTex;
    public List<Texture> dirtyTex;

    public ParticleSystem smoke;
    public ParticleSystem sparks;

    public GameObject dirtyText;
    public GameObject brokenText;
    public GameObject functioningText;
    private GameObject currentStatusText;
    public void BreakMachine() {
        if(cycle.currentDayStats != null)
            cycle.currentDayStats.brokenMachines += 1;
        state = ArcadeState.BROKEN;
        breakage = 100;
        notifications.Add(new Notification("Arcade Management", "An arcade machine needs to be repaired!"));
        buttonRightText.text = "Repair";
        currentStatusText.SetActive(false);
        currentStatusText = brokenText;
        currentStatusText.SetActive(true);
        //change texture of machine

        smoke.Play();
        sparks.Play();
    }

    public void DirtyMachine() {
        if(cycle.currentDayStats != null)
            cycle.currentDayStats.dirtyMachines += 1;
        state = ArcadeState.DIRTY;
        dirtiness = 100;    
        notifications.Add(new Notification("Arcade Management", "An arcade machine needs to be cleaned!"));
        buttonRightText.text = "Clean";
        currentStatusText.SetActive(false);
        currentStatusText = dirtyText;
        currentStatusText.SetActive(true);
        //change texture of machine
    }

    public int costToRepair = 50;
    public int costToClean = 10;
    public void RestoreMachine() {
        if(state == ArcadeState.BROKEN) {
            if(economy.money >= costToRepair) {
                economy.Deduct(costToRepair);
                breakage = 0;
                state = ArcadeState.NORMAL;
                notifications.Add(new Notification("Arcade Management", "Repairs were made to the arcade machine for " + costToRepair + " coins."));
                breakage = 0;
                currentStatusText.SetActive(false);
                currentStatusText = functioningText;
                currentStatusText.SetActive(true);

                buttonRightText.text = "Play";
                smoke.Stop();
                sparks.Stop();
            } else notifications.Add(new Notification("Financial Advisor", "Not enough funds to repair the machine!"));
        }

        if(state == ArcadeState.DIRTY) {
            if(economy.money >= costToClean) {
                economy.Deduct(costToClean);
                dirtiness = 0;
                state = ArcadeState.NORMAL;
                notifications.Add(new Notification("Arcade Management", "The arcade machine was cleaned for " + costToRepair + " coins."));
            } else notifications.Add(new Notification("Financial Advisor", "Not enough funds to clean the machine!"));
        }

        //restore normal texture ?
    }

    public override void SaveData(GameData data)
    {
        iData.itemState = state;
        iData.timesUsed = timesPlayed;
        iData.itemLevel = currentLevel;
        iData.breakageAmount = breakage;
        iData.occupied = occupied;
        iData.advanceQueue = advanceQueue;
        if (queue.Count > 0)
        {
            iData.AIInQueue = new List<string>();
            foreach (var ai in queue)
            {
                iData.AIInQueue.Add(ai.GetComponent<AIStats>().ID);
            }
        }
        
        base.SaveData(data);
    }

    public override void LoadData(GameData data)
    {
        base.LoadData(data);
        state = iData.itemState;
        timesPlayed = iData.timesUsed;
        currentLevel = iData.itemLevel;
        breakage = iData.breakageAmount;
        costSlider.value = ussageCost;
        occupied = iData.occupied;
        advanceQueue = iData.advanceQueue;

        foreach (var ai in iData.AIInQueue)
        {
            queueIDs.Add(ai);
            queue.Add(null);
        }
        

    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using Pixelplacement;
using UnityEngine.UI;
using TMPro;

public class Machines : Item {

    public MenuUI menu;
    protected Economy economy;
    protected Player input;
    public TextMeshPro buttonLeftText;
    public TextMeshPro buttonRightText;
    [HideInInspector]
    public string occupant;
    public int dayPurchased = 0;
    public int totalCostOfMaintenance = 0;

    [HideInInspector]
    public DayNightCycle cycle;

    protected bool playerIsInRadius = false;

    public void Update() {
        if(playerIsInRadius) {
            if(input.GetButtonDown("Manage")) {
                if(PlayerManager.menuUsing == null) menu.PopUp(true);

            }
            if (input.GetButtonDown("Back"))
            {
                if (PlayerManager.menuUsing == menu) menu.PopDown(true);
            }
        }

        InstanceID = "Obj" + GetInstanceID();
    }

    private void OnTriggerEnter(Collider other) {
        if (transform.parent == null)
        {
            if (other.CompareTag("Player") && this.CompareTag("Machine"))
            {
                playerIsInRadius = true;

                ShowDiegeticUI(true);
            }
        }
       
    }

    private void OnTriggerExit(Collider other) {
        if (transform.parent == null)
        {
            if (other.CompareTag("Player") && this.CompareTag("Machine"))
            {
                playerIsInRadius = false;

                ShowDiegeticUI(false);
            }
        }

    }

    private NotificationSystem notifications;

    public void Start() {
        SetMenu();

        notifications = GameObject.Find("Managers").GetComponent<NotificationSystem>();

        Canvas[] uiCanvases = GetComponentsInChildren<Canvas>();

        foreach (var canvas in uiCanvases)
        {
            canvas.worldCamera = FindObjectOfType<ViewManager>().UICamera;
        }


        input = ReInput.players.GetPlayer(0);
        cycle = GameObject.Find("DayNightCycle").GetComponent<DayNightCycle>();
        economy = GameObject.Find("Managers").GetComponent<Economy>();

        dayPurchased = cycle.daysPlayed;

        UpdateCost();

        int nextCostToUpgrade = initialCostToUpgrade + ((currentLevel-1) * costIncreasePerLevel);
        if(upgradeButton != null) upgradeButton.transform.Find("Text").GetComponent<Text>().text = "Upgrade ($" + nextCostToUpgrade + ")";
        menu.PopDown(true);
    }


    public List<GameObject> levelPrefabs;
    public int currentLevel = 1;

    public int initialCostToUpgrade = 100;
    public int costIncreasePerLevel = 50;

    public GameObject upgradeButton;
    public virtual void Upgrade() {
        if(currentLevel < levelPrefabs.Count) {
            int costToUpgrade = initialCostToUpgrade + ((currentLevel-1) * costIncreasePerLevel);
            if(economy.money >= costToUpgrade) {
                economy.Deduct(costToUpgrade);
                totalCostOfMaintenance += costToUpgrade;
                if(cycle.currentDayStats != null) cycle.currentDayStats.upgradeExpenses += costToUpgrade;
                currentLevel++;
                menu.level.text = "" + currentLevel;
            
                float percentageFilled = (float) currentLevel / levelPrefabs.Count;

                Vector2 previousSize = menu.levelBar.currentSize;
                menu.levelBar.currentSize = new Vector2(menu.levelBar.poppedUpSize.x * percentageFilled, menu.levelBar.poppedUpSize.y);

                int nextCostToUpgrade = initialCostToUpgrade + ((currentLevel-1) * costIncreasePerLevel);
                if(currentLevel == levelPrefabs.Count) upgradeButton.transform.Find("Text").GetComponent<Text>().text = "Max Level";
                else upgradeButton.transform.Find("Text").GetComponent<Text>().text = "Upgrade ($" + nextCostToUpgrade + ")";

                Tween.Size(menu.levelBar.gameObject.GetComponent<RectTransform>(), previousSize, menu.levelBar.currentSize, 0.5f, 0f, Tween.EaseInOutStrong, Tween.LoopType.None, null, null, false);
            }  else notifications.Add(new Notification("Financial Advisor", "Not enough funds to upgrade machine!"));
        } else notifications.Add(new Notification("Financial Advisor", "Machine is at its maximum level!"));
    }

    public void UpdateBar(UIobject bar, float percentageFilled) {
        Vector2 previousSize = bar.currentSize;
        bar.currentSize = new Vector2(bar.poppedUpSize.x * percentageFilled, bar.poppedUpSize.y);
        Tween.Size(bar.gameObject.GetComponent<RectTransform>(), previousSize, bar.currentSize, 0.5f, 0f, Tween.EaseInOutStrong, Tween.LoopType.None, null, null, false);
    }

    public TextMeshProUGUI costText;
    public Slider costSlider;
    public void UpdateCost() {
        ussageCost = (int)costSlider.value;
        costText.text = "$" + ussageCost;
    }

    public virtual void ShowDiegeticUI(bool show) {}
    public virtual void SetMenu() {}

}

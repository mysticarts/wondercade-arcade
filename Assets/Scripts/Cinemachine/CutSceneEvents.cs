﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class CutSceneEvents : MonoBehaviour
{
    public TextMesh ArcadeName;
    public InputField NameIF;
    public GameObject NameOfArcadeInput;
    public GameObject MaleOrFemaleCanvas;
    public GameObject MalePlayer;
    public GameObject FemalePlayer;
    public CurrentSave currentSave;
    public GameObject StoryCanvas;
    //public ParticleSystem Dust;




    public void Start()
    {
        
    
        ArcadeName.text = "Wondercade";
        currentSave = FindObjectOfType<CurrentSave>();
       // Dust.Play();
        
    }

  
    public GameObject firstSelected;
    private void GenderUiAppears()
    {
        Time.timeScale = 0;
        StoryCanvas.SetActive(false);
        GameObject.Find("EventSystem").GetComponent<EventSystem>().SetSelectedGameObject(firstSelected);
        MaleOrFemaleCanvas.SetActive(true);
        //sets the canvas ui to be seen to the player by an animation event
    }

    public void MaleActive()
    {
        //serialization.gameData.gender = "Male";
        
        MalePlayer.SetActive(true);
        FemalePlayer.SetActive(false);
        //timescale normalised on character selection resuming cutscene

        currentSave.playersGender = "Male";

    }

  
    public void FemaleActive()
    {
        
        //serialization.gameData.gender = "Female";
       
        //timescale normalised on character selection resuming cutscene
        FemalePlayer.SetActive(true);
        //if female butotn is pressed female player is set active
        MalePlayer.SetActive(false);
        //Male Player is set inactive
        currentSave.playersGender = "Female";

    }

    public void ConfirmedGender()
    {
       // Dust.Play();
        Time.timeScale = 1;


        MaleOrFemaleCanvas.SetActive(false);
        


    }

    public GameObject secondSelected;
    public void ArcadeNameChangeAppears()
    {
        GameObject.Find("EventSystem").GetComponent<EventSystem>().SetSelectedGameObject(secondSelected);
        
        // Dust.Stop();
        Time.timeScale = 0;

        NameOfArcadeInput.SetActive(true);
        //sets the canvas to be seen on animation event

        //GameObject.Find("EventSystem").GetComponent<EventSystem>().SetSelectedGameObject(firstSelected);

        ArcadeName.text = NameIF.text;
        currentSave.arcadesName = ArcadeName.text;
        
        //whats written in the input field is transfered to the text mesh 
    }

    

    public void NameInputSelected()
    {
      //  Dust.Play();
        Time.timeScale = 1;
        //TimeScale resumes leading the animation to continue
        NameOfArcadeInput.SetActive(false);
        //The game object is removed as it is no longer useds in the playthrough
    }

    public void LoadScene()
    {
        Time.timeScale = 1;
       // serialization.SaveGame();
        SceneManager.LoadScene("James B New");
    }

    private void OnApplicationQuit()
    {
        Time.timeScale = 1;
    }


}




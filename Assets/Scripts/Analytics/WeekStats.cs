﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeekStats : MonoBehaviour {

    //always keep the last 7 days stats
    private static int maxDaysToTrack = 7;

    //when a day ends, it creates a 'DayStats' object which holds all the stats for that day
    //For example, to access yesterdays sales amount, use dayStats[0].sales;
    public static Queue<DayStats> dayStats = new Queue<DayStats>();

    public static void AddDay(DayStats stats) {
        dayStats.Enqueue(stats);
        Debug.Log(dayStats.Count);
        if(dayStats.Count > maxDaysToTrack)
            Destroy(dayStats.Dequeue().gameObject); //pop off last day
    }

    public static int GetTotalCustomers() {
        int customers = 0;
        foreach(DayStats stats in dayStats)
            customers += stats.numberOfCustomers;
        return customers;
    }
    public static float GetTotalSales() {
        float sales = 0;
        foreach(DayStats stats in dayStats)
            sales += stats.GetTotalSales();
        return sales;
    }
    public static float GetArcadeSales() {
        float sales = 0;
        foreach(DayStats stats in dayStats)
            sales += stats.arcadeSales;
        return sales;
    }
    public static float GetFoodSales() {
        float sales = 0;
        foreach(DayStats stats in dayStats)
            sales += stats.foodSales;
        return sales;
    }
    public static float GetDrinkSales() {
        float sales = 0;
        foreach(DayStats stats in dayStats)
            sales += stats.drinkSales;
        return sales;
    }
    public static float GetAverageSpending() {
        if(GetTotalCustomers() == 0) return 0;
        return Mathf.Round((GetTotalSales() / GetTotalCustomers()) * 100) / 100;
    }

    public static float GetTotalExpenses() {
        float expenses = 0;
        foreach(DayStats stats in dayStats)
            expenses += stats.GetTotalExpenses();
        return expenses;
    }
    public static float GetRepairExpenses() {
        float expenses = 0;
        foreach(DayStats stats in dayStats)
            expenses += stats.repairExpenses;
        return expenses;
    }
    public static float GetUpgradeExpenses() {
        float expenses = 0;
        foreach(DayStats stats in dayStats)
            expenses += stats.upgradeExpenses;
        return expenses;
    }
    public static float GetBillsExpenses() {
        float expenses = 0;
        foreach(DayStats stats in dayStats)
            expenses += stats.billsExpenses;
        return expenses;
    }
    public static float GetMarketingExpenses() {
        float expenses = 0;
        foreach(DayStats stats in dayStats)
            expenses += stats.marketingExpenses;
        return expenses;
    }

    public static float GetTotalProfit() {
        float profit = 0;
        foreach(DayStats stats in dayStats)
            profit += stats.GetProfit();
        return profit;
    }

    public static float GetSatisfaction() {
        if(dayStats.Count == 0) return 50;
        float satisfaction = 0;
        foreach (DayStats stats in dayStats)
            satisfaction += stats.GetAverageSatisfaction();
        return satisfaction/dayStats.Count;
    }

}

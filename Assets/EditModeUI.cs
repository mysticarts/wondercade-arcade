﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

public class EditModeUI : MonoBehaviour {
    public GameObject back;
    public Vector3 back_PoppedUpPos;
    public Vector3 back_PoppedDownPos;

    public void PopUp() {
         Tween.AnchoredPosition(back.GetComponent<RectTransform>(), back_PoppedUpPos, 1, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
    }

    public void PopDown() {
        Tween.AnchoredPosition(back.GetComponent<RectTransform>(), back_PoppedDownPos, 1, 0, Tween.EaseBounce, Tween.LoopType.None, null, null, false);
    }
}
